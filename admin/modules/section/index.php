 <?php
  $root = $_SERVER['DOCUMENT_ROOT'];
  $page_name = 'admin.section';

  require($root.'/inc/classes/db.php');
  include($root.'/inc/system/redis.php');
  include($root.'/inc/functions.php');
  include($root.'/inc/variables.php');
  require($root.'/inc/classes/users.php');
  include($root.'/inc/system/profile.php');
  include($root.'/inc/system/profile_redirect.php');
  require($root.'/inc/classes/sessions.php');
  include($root.'/inc/system/usession.php');
  require($root.'/inc/classes/support.php');

  if($ugroup != 4) {
   header('Location: /');
   exit;
  }
$newusers = $user->new_users_num();

if ($newusers > 0) {
  $num = '<div class="flat_admin_counter">'.$newusers.'</div>';
} else {

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>Панель управления</title>
    <? include($root.'/include/head.php') ?>
  </head>
  <body>
    <div id="header_load"></div>
    <div id="page">
      <? include($root.'/include/header.php') ?>
      <div id="content">
        <? include($root.'/include/left.php') ?>
        <div id="right_wrap">
          <div id="right_wrap_b">
            <div id="right">
              <div class="main nopad">
                  <ul class="admin_select">
                    <li>
                      <a class="block vk" href="/admin/modules/users/" onclick="nav.go(this); return false;">
                        <?php echo $num; ?>
                        <div class="icon socials vk"><i></i></div>
                        <div class="name">Сотрудники</div>
                        <div class="desc">Список сотрудников</div>
                      </a>
                    </li>
                    <li>
                      <a class="block vk" href="/exchange/requests" onclick="nav.go(this); return false;">
                        <? if($redis->get('admin_exchange_counter')) echo '<div class="flat_admin_counter">'.$redis->get('admin_exchange_counter').'</div>'; ?>
                        <div class="icon socials nik"><i></i></div>
                        <div class="name">Смена никнейма</div>
                        <div class="desc">Заявки на смену никнейма</div>
                      </a>
                    </li>
                    <li>
                      <a class="block vk" href="/urgent/requests" onclick="nav.go(this); return false;">
                        <? if($redis->get('admin_urgent_counter')) echo '<div class="flat_admin_counter">'.$redis->get('admin_urgent_counter').'</div>'; ?>
                        <div class="icon socials dnations"><i></i></div>
                        <div class="name">Срочные заказы</div>
                        <div class="desc">Отчеты о срочных заказах</div>
                      </a>
                    </li>
                    <li>
                      <a class="block vk" href="/penalty/requests" onclick="nav.go(this); return false;">
                        <? if($redis->get('admin_penalty_counter_left')) echo '<div class="flat_admin_counter">'.$redis->get('admin_penalty_counter_left').'</div>'; ?>
                        <div class="icon socials penalty"><i></i></div>
                        <div class="name">Погашения штрафов</div>
                        <div class="desc">Отчеты о погашения штрафов</div>
                      </a>
                    </li>
                    <li>
                      <a class="block vk" href="/admin/modules/pages/" onclick="nav.go(this); return false;">
                        <div class="icon socials pages"><i></i></div>
                        <div class="name">Страницы</div>
                        <div class="desc">Управление контентом сайта</div>
                      </a>
                    </li>
                    <li>
                      <a class="block vk" href="/admin/modules/stats/" onclick="nav.go(this); return false;">
                        <div class="icon socials stats"><i></i></div>
                        <div class="name">Статистика</div>
                        <div class="desc">Статистика активности сотрудников</div>
                      </a>
                    </li>
                    <li>
                      <a class="block vk" href="/penalty/appeal" onclick="nav.go(this); return false;">
                        <? if($redis->get('admin_penalty_appeal_counter_left')) echo '<div class="flat_admin_counter">'.$redis->get('admin_penalty_appeal_counter_left').'</div>'; ?>
                        <div class="icon socials vacation"><i></i></div>
                        <div class="name">Заявки на апелляцию</div>
                        <div class="desc">Апелляция штрафных баллов</div>
                      </a>
                    </li>
					           <li>
                      <a class="block vk" href="/admin/modules/balance/" onclick="nav.go(this); return false;">
                        <div class="icon socials convoys_point"><i></i></div>
                        <div class="name">Баллы за конвой</div>
                        <div class="desc">Начислить баллы за конвой</div>
                      </a>
                    </li>
					            <li>
                      <a class="block vk" href="/admin/modules/stats/pays.php?act=rules" onclick="nav.go(this); return false;">
                        <div class="icon socials rules"><i></i></div>
                        <div class="name">Правила приняли</div>
                        <div class="desc">Кто принял правила компании</div>
                      </a>
                    </li>
                    <li>
                    <a class="block vk" href="/convoys/add" onclick="nav.go(this); return false;">
                      <div class="icon socials convoys_add"><i></i></div>
                      <div class="name">Добавления конвоя</div>
                      <div class="desc">Опубликовать новый конвой</div>
                      </a>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
          <? include($root.'/include/footer.php') ?>
        </div>
      </div>
    </div>
    <? include($root.'/include/scripts.php') ?>
  </body>
</html>
