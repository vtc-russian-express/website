<?php
  $root = $_SERVER['DOCUMENT_ROOT'];
  $page_name = 'admin.section.auto';

  require($root.'/inc/classes/db.php');
  include($root.'/inc/system/redis.php');
  include($root.'/inc/functions.php');
  include($root.'/inc/variables.php');
  require($root.'/inc/classes/users.php');
  include($root.'/inc/system/profile.php');
  include($root.'/inc/system/profile_redirect.php');
  require($root.'/inc/classes/sessions.php');
  include($root.'/inc/system/usession.php');
  require($root.'/inc/classes/support.php');

  if($ugroup != 3 && $ugroup != 4) {
   header('Location: /');
   exit;
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>Автошкола</title>
    <? include($root.'/include/head.php') ?>
  </head>
  <div id="header_load"></div>
  <body>
    <div id="page">
      <? include($root.'/include/header.php') ?>
      <div id="content">
        <? include($root.'/include/left.php') ?>
        <div id="right_wrap">
          <div id="right_wrap_b">
            <div id="right">
              <div class="main nopad">
                  <ul class="admin_select">
                    <li>
                      <a class="block vk" href="#" onclick="nav.go(this); return false;">
                        <div class="icon socials stop"><i></i></div>
                        <div class="name">Заявки на экзамен</div>
                        <div class="desc">Список заявок на экзамены</div>
                      </a>
                    </li>
                    <li>
                      <a class="block vk" href="/admin/modules/school/category.php" onclick="nav.go(this); return false;">
                        <div class="icon socials passport"><i></i></div>
                        <div class="name">Выдать категорию</div>
                        <div class="desc">Выдать новую категорию сотруднику</div>
                      </a>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
          <? include($root.'/include/footer.php') ?>
        </div>
      </div>
    </div>
    <? include($root.'/include/scripts.php') ?>
  </body>
</html>
