<?php
$support_new = ($ugroup == 4 || $ugroup == 5) ? my_support_new(1) : my_support_new();
$balance = getBalancePoints($user_id);
?>
<div id="left">
   <? if($udel) { ?>
   <div style="margin-top: 10px" id="account_is_deleted_left">
      Аккаунт удален.
   </div>
   <? } elseif($user_logged) { ?>

    <div id="flat_profile_menu_left_wrap">
        <div id="flat_profile_menu_left">
            <a<? if($page_name == 'tasks') echo ' class="active"'; ?> href="/tasks" onclick="nav.go(this); return false;"><span>Срочные заказы</span></a>
            <a<? if($page_name == 'convoys') echo ' class="active"'; ?> href="/convoys" onclick="nav.go(this); return false;"><span>Конвои компании</span></a>

            <div class="flat_profile_menu_left_hr"></div>

            <a<? if($page_name == 'my_complaints') echo ' class="active"'; ?> href="/complaints" onclick="nav.go(this, '', function() {setTimeout(function() {$('#mleft_complaints').html('')}, 10)}); return false;"><span>Мои штрафы</span> <span id="mleft_complaints"><? if($unew_complaints) { ?><div class="flat_profile_menu_left_counter"><? echo $unew_complaints; ?></div><? } ?></span></a>
            <a <? if($page_name == 'award') echo ' class="active"'; ?> href="/award" onclick="nav.go(this); return false;"><span>Мои достижения</span></a>
            <a<? if($page_name == 'my_settings' && !$_GET['menu']) echo ' class="active"'; ?> href="/settings" onclick="nav.go(this); return false;"><span>Мои настройки</span></a>

            <div class="flat_profile_menu_left_hr"></div>

            <a<? if($page_name == 'donation') echo ' class="active"'; ?> href="/donation" onclick="nav.go(this); return false;"><span>Мои пожертвования</span></a>
            <div class="flat_profile_menu_left_hr"></div>
            <a<? if($page_name == 'my_settings' && $_GET['menu'] == 1) echo ' class="active"'; ?> href="/settings/ref?menu=1" onclick="nav.go(this); return false">
            <span>Партнерская программа</span>
            </a>

            <div class="flat_profile_menu_left_hr"></div>

            <a<? if($page_name == 'support') echo ' class="active"'; ?> href="<? if($ugroup == 4 || $ugroup == 5) { ?>/support/questions<? } elseif($support_new) { ?>/support<? } else { ?>/support/new<? } ?>" onclick="nav.go(this, '', function() {setTimeout(function() {$('#support_start_message_w').hide();}, 10);}); return false"><span>Помощь<span>
            <? if($support_new) echo '<div class="flat_profile_menu_left_counter">'.$support_new.'</div>'; ?></a>


        </div>
    </div>

   <? if($ugroup == 4 || $ugroup == 3) { ?>

       <div style="width: 1px; height: 10px;"></div>

        <div id="flat_profile_menu_left_wrap">
            <div id="flat_profile_menu_left">

                <? if($ugroup == 4) { ?>
                    <a<? if($page_name == 'admin.section') echo ' class="active"'; ?> href="/admin/modules/section/" onclick="nav.go(this); return false">
                        Панель управления
                    </a>
                <?  } ?>

                <a<? if($page_name == 'admin.section.auto') echo ' class="active"'; ?> href="/admin/modules/school/" onclick="nav.go(this); return false">
                    Автошкола
                </a>

            </div>

        </div>

   <? } ?>

       <? if(!$uvk_id && $user_logged) { ?>
   <div id="info_msg_header">Пожалуйста <a href="javascript://" onclick="users._add_vk();"><b>привяжите страницу ВКонтакте</b></a> к Вашему аккаунту.
     <br />

	 <div id="uLoginb4af8ba2" data-ulogin="display=buttons;fields=first_name,last_name;redirect_uri=;callback=addvk"><div class="blue_button_wrap small_blue_button" data-uloginbutton = "vkontakte"><div class="blue_button">Привязать страницу</div></div></div>

   </div><? } ?>

      <div id="balance_block">
       <div class="block br3px">
        <div class="label">На Вашем счёте</div>
        <div class="balance">
         <a href="/settings/balance" onclick="nav.go(this); return false"><span class="amount"><? echo $balance; ?></span> <? echo declOfNum(abs($balance), array('балл', 'балла', 'баллов')); ?></a>
        </div>
       </div>
      </div>

       <div id="vk_groups">
           <div id="vk_groups"></div>
       </div>

     <? if($redis->hget('blog_menu_left', 'id')) { ?><div id="flat_profile_new_menu_left">
      <div id="flat_profile_new_menu_left_title">Новости компании</div>

      <div id="flat_profile_new_menu_left_text">
       <? echo stripslashes(fxss($redis->hget('blog_menu_left', 'title'))); ?>
      </div>

      <div id="flat_profile_new_menu_left_next">
       <a href="/blog?id=<? echo $redis->hget('blog_menu_left', 'id'); ?>" onclick="nav.go(this); return false">подробнее →</a>
      </div>
     </div><? } ?>

     <div id="ads_menu_left_div"></div>

     <? } else { ?>
	 <div id="flat_header_user_login">
	 <div id="login_form_error"></div>
	 <div class="flat_field_title">Логин:</div>
     <input id="ulogin" type="text" title="Логин"/>
     <div class="flat_field_title">Пароль:</div>
     <div id="flat_header_restore"><a href="/restore" onclick="nav.go(this); return false">Забыли?</a></div>
     <input id="upassword" type="password" title="Пароль"/>
	 <div onclick="users._post_login()" id="flat_auth_submit_button" class="blue_button_wrap"><div class="blue_button">Войти</div></div>
     <div onclick="nav.go('', '/reg'); return false" id="flat_reg_button" class="blue_button_wrap"><div class="blue_button">Регистрация</div></div>
	 <div id="uLogin" data-ulogin="display=buttons;fields=first_name,last_name;redirect_uri=;callback=authvk">
	 <div id="flat_reg_button" class="new_button_auth" data-uloginbutton = "vkontakte"><div class="new_flat_button_auth">Войти через ВКонтакте</div></div>
	 </div>
 	 </div>
	 <div id="ads_menu_left_div"></div>
     <? } ?>
    </div>
