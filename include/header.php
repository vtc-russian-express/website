
<div id="black_bg"></div>
<div id="error_head"></div>

<div id="box_smilies_box"></div>

<div id="tooltip_users_info"></div>

   <div id="header">
    <div class="content_width">
     <div class="clearfix">
      <div class="fl_l logo_wrap user_select_none">
       <a href="/tasks" onclick="nav.go(this); return false">
        <span class="logo"></span>
       </a>
      </div>
      <? if($user_logged) { ?>
      <div class="fl_l">
        <div id="header_notif">
          <a href="javascript://" class="trns15s br60px">
           <span class="clearfix">
            <span class="fl_l icon trns15s">
              <? if($unew_archive) { ?><div id="notify_count"><? echo $unew_archive; ?></div><? } ?>
             <span class="header_menu_icons notification"></span>
            </span>
           </span>
          </a>
        </div>
        <div id="header_notif_wrap">
         <div id="notify_header">
           Уведомления
         </div>
         <div id="notify_content">
           <div class="notif_load" id="notif_loading" style="height: 79px;width: 470px;"></div>
           <div id="result_notif" style="display: none;"></div>
         </div>
        </div>
      </div>
     <? } ?>
      <div class="fl_l menu user_select_none">

       <? if($user_logged){ ?>
       <a href="/users" onclick="nav.go(this); return false" class="trns15s br60px <? if($page_name == 'employee') echo 'active'; ?>">
        <span class="clearfix">
         <span class="fl_l icon trns15s">
          <span class="header_menu_icons home"></span>
         </span>
         <span class="fl_l">Сотрудники</span>
        </span>
       </a>
      <? } ?>

       <a href="/convoys" onclick="nav.go(this); return false" class="trns15s br60px <? if($page_name == 'convoys') echo 'active'; ?>">
        <span class="clearfix">
         <span class="fl_l icon trns15s">
          <span class="header_menu_icons info"></span>
         </span>
         <span class="fl_l">
             <? if($user_logged){ ?>
                Конвои
             <? } else { ?>
                Открытые конвои
             <? } ?>
         </span>
        </span>
       </a>

      <? if($user_logged){ ?>
       <a href="/page/rules" onclick="nav.go(this); return false" class="trns15s br60px <? if($page_name == 'site_page') echo 'active'; ?>">
        <span class="clearfix">
         <span class="fl_l icon trns15s">
          <span class="header_menu_icons check"></span>
         </span>
         <span class="fl_l">Правила</span>
        </span>
       </a>
      <? } ?>

      <? if($user_logged){ ?>
       <a href="/support/new" onclick="nav.go(this); return false" class="trns15s br60px <? if($page_name == 'support') echo 'active'; ?>">
        <span class="clearfix">
         <span class="fl_l icon trns15s">
          <span class="header_menu_icons question"></span>
         </span>
         <span class="fl_l">Помощь</span>
        </span>
       </a>
      <? } ?>

      </div>
      <? if($user_logged) { ?>
      <div class="fl_r profile">
       <div class="clearfix">
        <div class="fl_l avatar user_select_none">
         <a href="/id<? echo $user_id; ?>" onclick="nav.go(this); return false">
          <div id="header_avatar" class="header_avatar br60px" style="background-image: url('<? echo $uavatar ? $uavatar : '/images/camera_b.png'; ?>');"></div>
         </a>
        </div>
        <div class="fl_l info">
         <div class="fullname">
          <a href="/id<? echo $user_id; ?>" onclick="nav.go(this); return false" class="trns15s bold"><? echo $ufirst_name; ?> <? echo $ulast_name; ?></a>
         </div>

         <div class="logout">
          <a href="/logout?hash=<? echo $user_uhash; ?>">Выйти</a>
         </div>
        </div>
       </div>
     </div>

    <? } ?>
     </div>
    </div>
   </div>
