<? $rand_script = 'v=1.10'; ?>
  <script type="text/javascript">
   var iPopular = {
    page_name: '<? echo $page_name; ?>',
    section: <? echo isset($_GET['section']) ? "'".fxss($_GET['section'])."'" : 0; ?>
  }
  </script>

  <meta name="description" content="Автоматизированная система управления Russian Express  Euro Truck Simulator 2. Вступайте в компанию, Это просто!"/>
  <meta name="keywords" content="Автоматизированная система управления, организации деятельности, контроля и учета сотрудников виртуальныx транпортных компаний мультиплеера грузовых симуляторов American Truck Simulator и Euro Truck Simulator 2, Вступайте в компанию или создавайте свою. Это просто, Euro Truck Simulator 2, ets 2, ats 2, ats, American Truck Simulator."/>
  <meta charset="UTF-8" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="Pragma" content="no-cache" />

  <meta property="og:url" content="https://vtc-express.ru/">
  <meta property="og:type" content="website">
  <meta property="og:title" content="<? if($page_name == 'convoys') { echo 'Конвои компании Russian Express.'; } else { echo 'Russian Express | Virtual Company'; } ?>">
  <meta property="og:image" content="https://vtc-express.ru/images/preview.png">
  <meta property="og:description" content="Автоматизированная система управления Russian Express">
  <meta property="og:site_name" content="vtc-express">

  <link rel="shortcut icon" href="/images/favicon.png?4" type="image/x-icon" />
  <link type="text/css" href="/css/main.css?<? echo $rand_script; ?>" rel="stylesheet" />
  <link type="text/css" href="/css/jquery.datetimepicker.css?<? echo $rand_script; ?>" rel="stylesheet" />

  <script type="text/javascript" src="/js/jquery.min.js?4"></script>
  <script type="text/javascript" src="/js/placeholder.js?<? echo $rand_script; ?>"></script>
  <script type="text/javascript" src="/js/wndLibrary.js?<? echo $rand_script; ?>"></script>
  <script type="text/javascript" src="/js/all.js?<? echo $rand_script; ?>"></script><? if($ugroup == 4 || $ugroup == 3 || $ugroup == 5) { ?>

  <script type="text/javascript" src="/js/admin.panel.js?<? echo $rand_script; ?>"></script><? } ?>
  
  <script type="text/javascript" src="/js/other.js?<? echo $rand_script; ?>"></script>
  <script type="text/javascript" src="/js/common.js?<? echo $rand_script; ?>"></script>
  <script type="text/javascript" src="/js/wiki.js?<? echo $rand_script; ?>"></script>
  <script type="text/javascript" src="/js/jquery.datetimepicker.full.js?<? echo $rand_script; ?>"></script>
  <script src="//ulogin.ru/js/ulogin.js"></script>
  <script type="text/javascript" src="//vk.com/js/api/openapi.js?151"></script>
