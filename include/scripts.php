<? if($page_name == 'admin.users') { ?>
  <div id="admin_actions_menu">
   <div onclick="nav.go('', '/admin/modules/users/?section=admin')" class="mnav <? if($section == 'admin') echo 'active'; ?>">Администраторы</div>
   <div onclick="nav.go('', '/admin/modules/users/?section=agent')" class="mnav <? if($section == 'agent') echo 'active'; ?>">Испыта-срок</div>
   <div onclick="nav.go('', '/admin/modules/users/?section=instructor')" class="mnav <? if($section == 'instructor') echo 'active'; ?>">Инструкторы</div>
   <div onclick="nav.go('', '/admin/modules/users/?section=uban_type')" class="mnav <? if($section == 'uban_type') echo 'active'; ?>">Заявки<? echo $num; ?></div>
  </div>
 <? } ?>

  <script id="mainscripts" type="text/javascript">
   $(function() {
   <?php if($rules == 0 && $user_logged) { ?> $("body").addClass("rules"); <? } ?>

    // placeholder к полям
    _placeholder('#input_tasks_search'); // поиск
    _placeholder('#support_add_content_field_theme');
    _placeholder('#support_add_field_theme');
    _placeholder('#support_add_field_text');
    _placeholder('#support_question_add_comment_field_text');
    _placeholder('#chat_message');
    _placeholder('#ustart');
    _placeholder('#uoldstop');
    _placeholder('#udate');
    _placeholder('#ustop');
    _placeholder('#usbor');
    _placeholder('#usbor2');
    _placeholder('#speed_start');
    _placeholder('#edit_name');
    _placeholder('#edit_lastname');
    _placeholder('#edit_date');
    _placeholder('#edit_city');
    _placeholder('#edit_server');
    _placeholder('#edit_truck');
    <? if($page_name == 'reg') { ?>
    _placeholder('#first_name');
    _placeholder('#last_name');
    _placeholder('#reg_login');
    _placeholder('#reg_password');
    _placeholder('#reg_email');
    _placeholder('#reg_nickname');
    _placeholder('#reg_invited_nickname');
    _placeholder('#reg_captcha_code');
    <? } ?>

    <? if($page_name == 'add_convoys' || $page_name == 'edit_convoys') { ?>
       _placeholder('#udate');
       _placeholder('#usbor');
       _placeholder('#usbor2');
       _placeholder('#ustart');
       _placeholder('#uoldstop');
       _placeholder('#ustop');
       _placeholder('#ulong');
       _placeholder('#udate_post');
       _placeholder('#uconvoys_field_text');
    <? } ?>

    <? if($ugroup == 4 || $ugroup == 3 || $ugroup == 5) { ?>_placeholder('#admin_balance_add_field_input');<? } ?>

    <? if($page_name == 'employee' || $page_name == 'admin.users') { ?>search_mini_info();<? } ?>


    if(ajax_nav) {
     stManager.load('/css/main.css?<? echo $rand_script; ?>', 'css');
     stManager.load('/js/all.js?<? echo $rand_script; ?>', 'js');<? if($ugroup == 4 || $ugroup == 3 || $ugroup == 5) { ?>
     stManager.load('/js/admin.panel.js?<? echo $rand_script; ?>', 'js');<? } ?> <? if($page_name == 'site_page_add' || $page_name == 'site_page_all') { ?>
     stManager.load('/js/wiki.js?<? echo $rand_script; ?>', 'js');<? } ?>

    }
       <? if($page_name == 'site_page_add' || $page_name == 'site_page_all' || $page_name == 'blog_add') { ?>
    $("#editor").wysibb();
       <? } ?>

    minSelect._new('admin_actions_menu_d', 170, '', $('#admin_actions_menu').html());

<? if($page_name == 'add_convoys' || $page_name == 'add_ride' || $page_name == 'edit_convoys') { ?>
  $.datetimepicker.setLocale('ru');
  jQuery('#udate').datetimepicker({
   i18n:{
    de:{
     months:[
      'Januar','Februar','März','April',
      'Mai','Juni','Juli','August',
      'September','Oktober','November','Dezember',
     ],
     dayOfWeek:[
      "So.", "Mo", "Di", "Mi",
      "Do", "Fr", "Sa.",
     ]
    }
   },
   timepicker:false,
   format:'d.m.Y'
  });
  jQuery('#usbor').datetimepicker({
  datepicker:false,
  format:'H:i' });
  jQuery('#usbor2').datetimepicker({
   datepicker:false,
   allowTimes:[
    '12:00', '13:00', '15:00',
    '17:00', '18:00', '19:10', '19:20', '19:30', '19:40', '19:50', '20:00'
  ],
  format:'H:i'
  });

  var active_select = $('#old_select_convoys_add_value').val();

  if(active_select) {
      select.create('convoys_add', {width: 332}, [[0, '- не выбрано -'], [1, 'Закрытый конвой'], [2, 'Покатушка'], [3, 'Открытый конвой'], active_select]);
  } else {
      select.create('convoys_add', {width: 332}, [[0, '- не выбрано -'], [1, 'Закрытый конвой'], [2, 'Покатушка'], [3, 'Открытый конвой'], 0]);
  }

  jQuery('#udate_post2').datetimepicker();
  jQuery('#udate_post').datetimepicker({
  format:'d.m.y H:i'
  });
<? } ?>
<? if($page_name == 'profile') { ?>
  $.datetimepicker.setLocale('ru');
  jQuery('#edit_date').datetimepicker({
   i18n:{
    de:{
     months:[
      'Januar','Februar','März','April',
      'Mai','Juni','Juli','August',
      'September','Oktober','November','Dezember',
     ],
     dayOfWeek:[
      "So.", "Mo", "Di", "Mi",
      "Do", "Fr", "Sa.",
     ]
    }
   },
   timepicker:false,
   format:'d.m.Y'
  });

  select.create('server', {width: 231}, [[0, '- Выберите сервер -'], [1, 'Europe 1 [ETS2]'], [2, 'Europe 2 [ETS2]'], [3, 'Europe 3 [ETS2]'], [4, 'United States [ETS2]'], [5, 'South America [ETS2]'], [6, 'Hong Kong [ETS2]'], <? echo $server; ?>]);
  select.create('truck', {width: 231}, [[0, '- Выберите тягач -'], [1, 'Daf XF 105 (2005 г.)'], [2, 'Daf XF Euro 6 (2013 г.)'], [3, 'Iveco Stralis (2002 г.)'], [4, 'Iveco Stralis Hi-Way (2013 г.)'], [5, 'Man TGX (2008 г.)'], [6, 'Mercedes-Benz Actros MP3 (2009 г.)'], [7, 'Mercedes-Benz Actros MP4 (2014 г.)'], [8, 'Renault Magnum IV (2005 г.)'], [9, 'Renault Premium Route (2006 г.)'], [10, 'Scania R (2004 г.)'], [11, 'Scania Streamline (2013 г.)'], [12, 'Volvo FH16 (2012 г.)'], [13, 'Volvo FH16 (2009 г.)'], [14, 'Skoda Superb'], <? echo $truck; ?>]);
  select.create('users_day', {width: 100}, ageList(1, 31, <? echo $day; ?>, 'День'));
  select.create('users_month', {width: 149, input_name: 'users_month'}, [[0, "Месяц"], [1, "Января"], [2,"Февраля"], [3,"Марта"], [4,"Апреля"], [5,"Мая"], [6,"Июня"], [7,"Июля"], [8,"Августа"], [9,"Сентября"], [10,"Октября"], [11,"Ноября"], [12,"Декабря"], <? echo $month; ?>]);
  select.create('users_year', {width: 100}, ageList(1975, 2002, <? echo $year; ?>, 'Год'));
<? } ?>

   <?php if($rules == 0 && $user_logged) { ?> promo_new._promo_pravila(<? echo $user_id ?>); <? } ?>

    // прокрутка до таблицы моих жалоб
    if($("#admin_tasks_blacklist_table").html()) {<? if($_GET['list'] == 1) { ?>$('html, body').animate({scrollTop: $("#admin_tasks_blacklist_table").offset().top}, 500);<? } ?>} <? if($_POST['activated_email'] == 1) { ?>
    cnt_black._show({title: 'Регистрация подтверждена.', text: 'Теперь Вы можете пользоваться всеми полномочиями сайта.'});<? } ?>
    _placeholder('#admin_site_page_add_short_url');
    _placeholder('#admin_site_page_add_name');
    $('#admin_site_page_add_short_url_f').click(function() {
     $('#admin_site_page_add_short_url').focus();
    });
    if($('#editor_html').val()) {
     $("#editor").htmlcode($('#editor_html').val());
    }<? if($ugroup == 4 || $ugroup == 5) { ?>
    if($('#list_edit_que_status').html()) {
     minSelect._new('edit_que_navigation', 150, 'Изменить статус', $('#list_edit_que_status').html());
    }<? } ?> <? if($page_name == 'restore') { ?>
    radiobtn._new('restore_radiobtn1', {title: 'Восстановить через E-mail'});
    radiobtn._new('restore_radiobtn2', {title: 'Восстановить с помощью страницы ВКонтакте'});
    $('#restore_field_code_o input[type="text"]').click(function() {
     $(this).css('border', '1px solid #C0CAD5');
    });
    $('#restore_field_email, #restore_field_code').keydown(function(event) {
     var keyCode = event.which;
     if(keyCode == 13) $('#restore_next_button').click();
    });<? } ?>

 <?php if(!$user_logged && $page_name == 'reg') { ?>
  select.create('users_day', {width: 89}, ageList(1, 31, 0, 'День'));
  select.create('users_month', {width: 108, input_name: 'users_month'}, [[0, "Месяц"], [1, "Января"], [2,"Февраля"], [3,"Марта"], [4,"Апреля"], [5,"Мая"], [6,"Июня"], [7,"Июля"], [8,"Августа"], [9,"Сентября"], [10,"Октября"], [11,"Ноября"], [12,"Декабря"], 0]);
  select.create('users_year', {width: 89}, ageList(1975, 2002, 0, 'Год'));
 <? } ?> 
  $('#header_notif').click(function() {
      $('#header_notif_wrap').toggle();
      users._notif();
  });

  list_menu_setting_header = 0;

  $('#header_notif').hover(function() {
      list_menu_setting_header = 0;
  }, function() {
      list_menu_setting_header = 1;
  });

  $('#header_notif_wrap').hover(function() {
      list_menu_setting_header = 0;
  }, function() {
      list_menu_setting_header = 1;
  });


  $('body').click(function() {
      if (list_menu_setting_header) {
          $('#header_notif_wrap').hide();
      }
  });

<?php if($user_logged) { ?>
  $(document).ready(function(){
    $('#notif_loading').fadeIn();
    $('#result_notif').load('/modules/archive/get.php', function(){
      setTimeout("$('#notif_loading').fadeOut(500);",500);
      setTimeout("$('#result_notif').fadeIn();",1000);
    });
  });

$(function () {
  $('.ft_users_search').on('click', function () {
      $('.ft_search').toggleClass('__opened');
    });
});
<? } ?>

   createTooltips();

   VK.Widgets.Group("vk_groups", {mode: 1, width: "173"}, 129589437);

});
  </script>
