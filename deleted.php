<?php
$root = $_SERVER['DOCUMENT_ROOT'];

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');
require($root.'/inc/classes/tasks_blacklist.php');

if(!$udel) {
 header('Location: /tasks');
 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Аккаунт удален.</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
  <div id="header_load"></div>   
 <div id="page">
  <div id="black_bg"></div>
  <div id="loading"><div id="load"></div></div>
  <div id="loading_page"></div>
  <div id="flat_profile_header">
   <div id="flat_profile_header_inner">
    <div id="flat_profile_header_inner_content">
     <div id="flat_profile_header_inner_content_left">
      <div id="head_loader"><div class="upload"></div></div>

      <a href="/tasks" onclick="nav.go(this);">
       <span id="flat_logo"></span>
      </a>
     </div>

     <div id="flat_profile_header_inner_content_menu" style="float: right;">
      <a href="/logout?hash=<? echo $user_uhash; ?>" onclick="nav.go(this); return false">выйти</a>
     </div>
    </div>
   </div>
  </div>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
        <div class="info-header-block deleted_img">
            <h4>Аккаунт удален.</h4>
            <p>К сожалению, аккаунт был удалён Администратором.</p>
            <p>Возможно вы были уволены из компании или давно не заходили в аккаунт. </p>
            <p>Ваша информацию будет хранить пока не <b>пройдёт 1-год</b>.</p>
            <div class="info-bottom">
                Что-бы восстановить аккаунт зайдите в TeamSpeak!
            </div>
        </div>
      </div>
     </div>
     <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
 </body>
</html>
