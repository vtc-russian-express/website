var users = {
    _post_login: function() {
        var ulogin = _val('#ulogin') ? $('#ulogin').val() : '';
        var upassword = _val('#upassword') ? $('#upassword').val() : '';
        if (!ulogin) {
            $('#ulogin').focus();
            return;
        } else if (upassword.length < 6) {
            $('#upassword').focus();
            return;
        }
        if ($('#login_in').html()) {
            $('#login_in .blue_button').html('<div class="upload"></div>');
        } else {
            $('#login_form_error').hide();
            $('#new_main_header_logo_form_overflow_right_button, #flat_auth_submit_button .blue_button').html('<div class="upload"></div>');
        }
        $.post('/auth', {
            ulogin: ulogin,
            upassword: upassword
        }, function(data) {
            var response = $.parseJSON(data);
            var error_title = response.error_title;
            var error_text = response.error_text;
            if ($('#login_in').html()) {
                $('#login_in .blue_button').html('Войти в систему');
            } else {
                $('#new_main_header_logo_form_overflow_right_button, #flat_auth_submit_button .blue_button').html('Войти');
            }
            if (error_text) {
                $('#login_form_error').show().html(error_text);
            } else if (response.response == 1) {
                $('#new_main_header_logo_form_overflow_right_button, #flat_auth_submit_button .blue_button').html('<div class="upload"></div>');
                window.location.href = '/tasks';
            }
        });
    },
    reg: function() {
        var first_name = $('#first_name');
        var last_name = $('#last_name');
        var login = $('#reg_login');
        var password = $('#reg_password');
        var email = $('#reg_email');
        var reg_nickname = isValueExst($('#reg_nickname'));
        var users_day = select.getActiveValue('users_day');
        var users_month = select.getActiveValue('users_month');
        var users_year = select.getActiveValue('users_year');
        var vk_uid2 = $('#reg_vk_id').val();
        var vk_uid = $('#reg_vk_id_ajax').val();
        var invited_nickname = $('#reg_invited_nickname');
        var captcha_code = $('#captcha_code').val();
        $('#form_reg_error').hide();
        if (!isname(first_name.val())) {
            $('.error.first_name').html('Имя может состоять только из «Русских» символов.');
            first_name.focus();
        } else if (!isname(last_name.val())) {
            $('.error.last_name').html('Фамилия может состоять только из «Русских» символов.');
            last_name.focus();
        } else if (!isLogin(login.val())) {
            $('.error.reg_login').html('Может содержать только латинские символы, некоторые знаки или цифры и не превышать 2-20 символов.');
            login.focus();
        } else if (!isPassword(password.val())) {
            $('.error.reg_password').html('Может содержать только латинские символы, некоторые знаки или цифры и не превышать 6-30 символов.');
            password.focus();
        } else if (!isEmail(email.val())) {
            $('.error.reg_email').html('E-mail выглядит не так.');
            email.focus();
        } else if (reg_nickname.length < 5) {
            $('.error.reg_nickname').html('Укажите ваш никнейм.');
            $('#reg_nickname').focus();
        } else if (vk_uid2.length < 1) {
            $('.error.reg_vk_id').html('Необходимо получить адрес вашей страницы.');
        } else if (users_day < 1) {
            $('.error.reg_date').html('Выберите день рождения.');
        } else if (users_month < 1) {
            $('.error.reg_date').html('Выберите месяц рождения.');
        } else if (users_year < 1) {
            $('.error.reg_date').html('Выберите год рождения.');
        } else {
            $('#reg_submit_button .blue_button').html('<div class="upload"></div>');
            $.post('/users/reg', {
                first_name: first_name.val(),
                last_name: last_name.val(),
                login: login.val(),
                password: password.val(),
                email: email.val(),
                reg_nickname: reg_nickname,
                users_day: users_day,
                users_month: users_month,
                users_year: users_year,
                vk_uid: vk_uid,
                invited_nickname: invited_nickname.val(),
                captcha_code: captcha_code,
                captcha_key: $('#captcha_key').val(),
                ref: $('#ureg_ref').val()
            }, function(data) {
                var result = $.parseJSON(data);
                var error_msg = result.error_msg;
                $('#reg_submit_button .blue_button').html('Продолжить');
                if (error_msg == 'Неверно введен код безопасности.') {
                    $('#wnd_white_main').css('opacity', 0);
                    captcha_box._show(function() {
                        $('#captcha_code').val($('#captcha_text').val());
                        setTimeout(function() {
                            _wndBlue._closed();
                            $('#black_bg').show();
                            $('#wnd_white_main').css('opacity', 1);
                            $('#reg_submit_button').click();
                        }, 10);
                    });
                } else if (result.response) {
                    $('#reg_submit_button .blue_button').html('<div class="upload"></div>');
                    nav.go('', '/blocked', function() {
                        cnt_black._show({
                            text: 'Вы отправили заявку на вступление в Russian Express в ближайшее время мы её расмотрим.'
                        });
                    });
                } else if (error_msg) {
                    if (result.field_name) {
                        $('#' + result.field_name).focus();
                    }
                    $('#form_reg_error').show().html('<div class="error_msg">' + error_msg + '</div>');
                }
            });
        }
    },
    change_password: function() {
        $('#my_settings_change_password_error').hide();
        $('#my_settings_old_password, #my_settings_new_password, #my_settings_new_password2').removeClass('field_error_i');
        if ($('#my_settings_old_password').val().length < 6) {
            $('#my_settings_change_password_error').show().html('<div class="error_msgm">Пароль должен состоять не менее, чем из 6 символов.</div>');
            $('#my_settings_old_password').focus();
            $('#my_settings_old_password').addClass('field_error_i');
            return;
        } else if ($('#my_settings_new_password').val().length < 6) {
            $('#my_settings_change_password_error').show().html('<div class="error_msgm">Пароль должен состоять не менее, чем из 6 символов.</div>');
            $('#my_settings_new_password').focus();
            $('#my_settings_new_password').addClass('field_error_i');
            return;
        } else if ($('#my_settings_new_password2').val().length < 6) {
            $('#my_settings_change_password_error').show().html('<div class="error_msgm">Пароль должен состоять не менее, чем из 6 символов.</div>');
            $('#my_settings_new_password2').focus();
            $('#my_settings_new_password2').addClass('field_error_i');
            return;
        }
        $('#settings_password_button .blue_button').html('<div class="upload"></div>');
        $.post('/settings/change.password', {
            old_password: $('#my_settings_old_password').val(),
            new_password: $('#my_settings_new_password').val(),
            new_password2: $('#my_settings_new_password2').val(),
            ssid: $('#ssid').val()
        }, function(data) {
            var response = $.parseJSON(data);
            var error_text = response.error_text;
            $('#settings_password_button .blue_button').html('Изменить пароль');
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            if (error_text) {
                $('#my_settings_change_password_error').show().html('<div class="error_msgm">' + error_text + '</div>');
            } else if (response.success == 1) {
                $('#my_settings_change_password_error').show().html('<div class="msg">Пароль успешно изменен.</div>');
            } else {
                $('#my_settings_change_password_error').show().html('<div class="error_msgm">Неизвестная ошибка.</div>');
            }
        });
    },
    change_login: function() {
        $('#my_settings_account_error').hide();
        $('#my_settings_account_login_field').removeClass('field_error_i');
        if ($('#my_settings_account_login_field').val().length < 1) {
            $('#my_settings_account_error').show().html('<div class="error_msgm">Слишком короткий логин.</div>');
            $('#my_settings_account_login_field').focus();
            $('#my_settings_account_login_field').addClass('field_error_i');
            return;
        }
        $('#settings_login_button .blue_button').html('<div class="upload"></div>');
        $.post('/settings/change.login', {
            login: $('#my_settings_account_login_field').val(),
            ssid: $('#ssid').val()
        }, function(data) {
            var response = $.parseJSON(data);
            var error_text = response.error_text;
            $('#settings_login_button .blue_button').html('Изменить логин');
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            if (error_text) {
                $('#my_settings_account_error').show().html('<div class="error_msgm">' + error_text + '</div>');
            } else if (response.success == 1) {
                $('#my_settings_account_error').show().html('<div class="msg">Логин успешно изменен.</div>');
            } else {
                $('#my_settings_account_error').show().html('<div class="error_msgm">Неизвестная ошибка.</div>');
            }
        });
    },
    change_steam: function() {
        $('#my_settings_account_error_steam').hide();
        $('#my_settings_account_steam_field').removeClass('field_error_i');
        if ($('#my_settings_account_steam_field').val().length < 1) {
            $('#my_settings_account_error_steam').show().html('<div class="error_msgm">Слишком короткий steamid.</div>');
            $('#my_settings_account_steam_field').focus();
            $('#my_settings_account_steam_field').addClass('field_error_i');
            return;
        }
        $('#settings_steam_button .blue_button').html('<div class="upload"></div>');
        $.post('/settings/change.steam', {
            steam: $('#my_settings_account_steam_field').val(),
            ssid: $('#ssid').val()
        }, function(data) {
            var response = $.parseJSON(data);
            var error_text = response.error_text;
            $('#settings_steam_button .blue_button').html('Изменить steamid');
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            if (error_text) {
                $('#my_settings_account_error_steam').show().html('<div class="error_msgm">' + error_text + '</div>');
            } else if (response.success == 1) {
                $('#my_settings_account_error_steam').show().html('<div class="msg">steamid успешно изменен.</div>');
            } else {
                $('#my_settings_account_error_steam').show().html('<div class="error_msgm">Неизвестная ошибка.</div>');
            }
        });
    },
    change_name: function() {
        var edit_name = _val('#edit_name') ? $('#edit_name').val() : '';
        var edit_lastname = _val('#edit_lastname') ? $('#edit_lastname').val() : '';
        var edit_city = _val('#edit_city') ? $('#edit_city').val() : '';
        var edit_day = ($('#select_users_day_value').val() ? $('#select_users_day_value').val() : $('#old_users_day_value').val());
        var edit_month = ($('#select_users_month_value').val() ? $('#select_users_month_value').val() : $('#old_users_month_value').val());
        var edit_year = ($('#select_users_year_value').val() ? $('#select_users_year_value').val() : $('#old_users_year_value').val());
        if (edit_name.length < 1) {
            $('#edit_name').focus();
            return;
        } else if (edit_lastname.length < 1) {
            $('#edit_lastname').focus();
            return;
        } else if (edit_city.length < 1) {
            $('#edit_city').focus();
            return;
        }
        $("#form_edit_error").hide();
        $('#edit_button .blue_button').html('<div class="upload"></div>');
        $.post('/settings/change.name', {
            edit_name: edit_name, //имя
            edit_lastname: edit_lastname, //фамилия
            edit_day: edit_day,
            edit_month: edit_month,
            edit_year: edit_year,
            edit_city: edit_city
        }, function(data) {
            var response = $.parseJSON(data);
            var error_text = response.error_text;
            var error_name = response.error_name;
            var error_lastname = response.error_lastname;
            var error_date = response.error_date;
            var error_city = response.error_city;
            $('#edit_button .blue_button').html('Сохранить информацию');
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            if (error_text) {
                $('#form_edit_error').show().html('<div class="error_msgm">' + error_text + '</div>');
            } else if (error_name == 1) {
                $('#error_name').show().html('Ваша Имя. должна состоять только из «Русских» букв.');
            } else if (error_lastname == 1) {
                $('#error_lastname').show().html('Ваша Фамилия. должна состоять только из «Русских» букв.');
            } else if (error_date == 1) {
                $('#error_date').show().html('Необходимо указать дату рождения.');
            } else if (error_city == 1) {
                $('#error_city').show().html('Необходимо указать город.');
            } else if (response.success == 1) {
                $('#success').show().html('Данные успешно обновлены.');
            } else {
                $('#form_edit_error').show().html('Неизвестная ошибка.');
            }
        });
    },
    user_interests: function() {
        var server = ($('#select_server_value').val() ? $('#select_server_value').val() : $('#old_server').val());
        var truck = ($('#select_truck_value').val() ? $('#select_truck_value').val() : $('#old_truck').val());
        $('#edit_track .blue_button').html('<div class="upload"></div>');
        $.post('/settings/change.interests', {
            server: server,
            truck: truck
        }, function(data) {
            var response = $.parseJSON(data);
            var error_text = response.error_text;
            $('#edit_track .blue_button').html('Сохранить информацию');
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            if (error_text) {
                $('#form_edit_error_track').show().html('<div class="error_msgm">' + error_text + '</div>');
            } else if (response.success == 1) {
                $('#success_truck').show().html('Данные успешно обновлены.');
            } else {
                $('#form_edit_error').show().html('Неизвестная ошибка.');
            }
        });
    },
    upload_avatar: function() {
        $("#avatar_upload_iframe_submit").trigger("click");
        $("#upload_avatar_loader").show().html('<div class="upload"></div>');
        $('iframe[name="avatar_upload_iframe"]').load(function() {
            try {
                var a = $.parseJSON($(this).contents().text());
                var b = a.error_text;
                if (a.success == 1) {
                    //$("#result_avatar img, .avatar a img").attr("src", a.avatar_src);
                    $('#result_avatar, #header_avatar').css('background-image', 'url(' + a.avatar_src + ')');
                    $('#upload_avatar_loader').hide();
                } else {
                    nav.error_head(b);
                    $('#upload_avatar_loader').hide();
                }
            } catch (c) {
                nav.error_head('Произошла ошибка. Попробуйте позже.');
                $('#upload_avatar_loader').hide();
            }
        })
    },
    upload_cover: function() {
        $("#cover_upload_iframe_submit").trigger("click");
        $("#upload_cover_loader").show().html('<div class="upload"></div>');
        $('iframe[name="cover_upload_iframe"]').load(function() {
            try {
                var a = $.parseJSON($(this).contents().text());
                var b = a.error_text;
                if (a.success == 1) {
                    $('#result_cover').css('background-image', 'url(' + a.cover_src + ')');
                    $('#upload_cover_loader').hide();
                } else {
                    nav.error_head(b);
                    $('#upload_cover_loader').hide();
                }
            } catch (c) {
                nav.error_head('Произошла ошибка. Попробуйте позже.');
                $('#upload_cover_loader').hide();
            }
        })
    },
    change_send_nik: function() {
        $('#my_settings_account_error_nik').hide();
        $('#my_settings_account_nik_field').removeClass('field_error_i');
        if ($('#my_settings_account_nik_field').val().length < 1) {
            $('#my_settings_account_error_nik').show().html('<div class="error_msgm">Слишком короткий никнейм.</div>');
            $('#my_settings_account_nik_field').focus();
            $('#my_settings_account_nik_field').addClass('field_error_i');
            return;
        }
        $('#settings_nik_button .blue_button').html('<div class="upload"></div>');
        $.post('/exchange/voutes', {
            count_nik: $('#exchange_count_nik').val(),
            count: $('#my_settings_account_nik_field').val()
        }, function(data) {
            var result = $.parseJSON(data);
            var error_msg = result.error_msg;
            $('#settings_nik_button .blue_button').html('Изменить никнейм');
            if (error_msg == 'login') {
                nav.go('', '/');
                return;
            }
            if (error_msg) {
                $('#my_settings_account_error_nik').show().html('<div class="error_msgm">' + error_msg + '</div>');
            } else if (result.response == 1) {
                $('#new_nik').show().html('<div id="my_settings_email_notif">Ваша заявка на смену никнейма рассматривается.</div>');
            } else {
                $('#my_settings_account_error_nik').show().html('<div class="error_msgm">Неизвестная ошибка.</div>');
            }
        });
    },
    delete_account: function() {
        nav.loader_page();
        $.getJSON('/settings/delete.account', {
            ssid: $('#ssid').val()
        }, function(data) {
            var response = data;
            if (response.success == 1) {
                if (response.type == 'return') {
                    nav.go('', '/tasks');
                } else {
                    nav.go('', '/deleted');
                }
            }
        });
    },
    reemail: function() {
        $('#reemail_a').hide();
        cnt_black._show({
            text: 'Письмо со ссылкой для подтверждения выслано повторно.'
        });
        $.getJSON('/reemail', {}, function(data) {
            var response = data;
            var error_text = response.error_text;
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
        });
    },
    _support_bottem: function() {
        nav.go('', '/support/new');
    },
    _next: function() {
        var page_num_id = $('#next_page_small_d');
        var page_num = page_num_id.text();
        $('#users_load_next').show().html('<div class="upload"></div>');
        $.get('/competition/users.next', {
            page: page_num
        }, function(data) {
            var response = data;
            $('#users_load_next').html('Показать ещё сотрудников »');
            if (response) {
                $('#next_page_small_c').append(response);
                page_num_id.text(page_num_id.text() * 1 + 1);
            } else {
                $('#users_load_next').hide();
            }
        });
    },
    _notif: function() {
        $.get('/archive/get', {
            type: 2
        }, function(data) {
            var response = data;
            if (response) {
                $('#notify_count').hide();
            } else {}
        });
    },
    _dle_notif: function(id) {
        $.get('/archive/get', {
            id: id,
            type: 3
        }, function(data) {
            var response = data;
            if (response) {
                $('#notif_id' + id).slideUp();
            } else {
                $('#notif_id' + id).slideUp();
            }
        });
    },
    _action_menu_show: function(id) {
        if (id) {
            $('#action_menu' + id).show();
        }
        $('#action_menu' + id).hover(function() {}, function() {
            $('#action_menu' + id).fadeOut(200);
        });
    }
}
var _head = {
    _up: function() {
        $('body, html').animate({
            scrollTop: 0
        }, 200);
    }
}
var _money = {
    _plus: function(n) {
        var money = $('.balance_menu_left_num').text() * 1;
        var money_plus = money + (n * 1);
        $('.balance_menu_left_num').text(money_plus);
        $('.balance_menu_left_num_desc').text(declOfNum(money_plus, ['балл', 'балла', 'баллов']));
        if ($('#balance_counter').html()) {
            var balance_settings_num = $('#balance_counter').find('b').text().match(/([0-9]+)/);
            var balance_settings_num_result = balance_settings_num[1] * 1;
            var balance_settings_num_sum_result = balance_settings_num_result + (n * 1);
            $('#balance_counter').find('b').html(balance_settings_num_sum_result + ' ' + declOfNum(balance_settings_num_sum_result, ['балл', 'балла', 'баллов']));
        }
    },
    _minus: function(n) {
        var money = $('.balance_menu_left_num').text() * 1;
        var money_plus = money - (n * 1);
        $('.balance_menu_left_num').text(money_plus);
        $('.balance_menu_left_num_desc').text(declOfNum(money_plus, ['балл', 'балла', 'баллов']));
        if ($('#balance_counter').html()) {
            var balance_settings_num = $('#balance_counter').find('b').text().match(/([0-9]+)/);
            var balance_settings_num_result = balance_settings_num[1] * 1;
            var balance_settings_num_sum_result = balance_settings_num_result - (n * 1);
            $('#balance_counter').find('b').html(balance_settings_num_sum_result + ' ' + declOfNum(balance_settings_num_sum_result, ['балл', 'балла', 'баллов']));
        }
    }
}
var refs = {
    _next: function() {
        var page_num_id = $('#next_page_small_d');
        var page_num = page_num_id.text();
        $('#next_page_small_t').show().html('<div class="upload"></div>');
        $.get('/settings/ref.next', {
            page: page_num
        }, function(data) {
            var response = data;
            $('#next_page_small_t').html('Показать еще рефералов');
            if (response) {
                $('#next_page_small_c').append(response);
                page_num_id.text(page_num_id.text() * 1 + 1);
            } else {
                $('#next_page_small_t').hide();
            }
        });
    }
}
var support = {
    _new: function() {
        $('#support_add_field_theme, #support_add_field_text').removeClass('field_error_i');
        $('#support_images_attach_img_field_ids').val('');
        var title = _val('#support_add_field_theme') ? $('#support_add_field_theme').val() : '';
        var message = _val('#support_add_field_text') ? $('#support_add_field_text').val() : '';
        $('.support_images_attach_img_field_id').each(function() {
            var id = $(this).val() ? $(this).val() + ',' : '';
            var values = $('#support_images_attach_img_field_ids').val();
            $('#support_images_attach_img_field_ids').val(values + '' + id);
        });
        if (title.length < 2) {
            $('#support_add_field_theme').focus();
            $('#support_add_field_theme').addClass('field_error_i');
            return;
        } else if (message.length < 2) {
            $('#support_add_field_text').focus();
            $('#support_add_field_text').addClass('field_error_i');
            return;
        }
        $('#support_add_content_buttons_left .blue_button').html('<div class="upload"></div>');
        $('#error_msg_support_error').html('');
        $.post('/support/add.post', {
            title: title,
            message: message,
            photo_attaches: $('#support_images_attach_img_field_ids').val(),
            ssid: $('#ssid').val()
        }, function(data) {
            var response = $.parseJSON(data);
            var error_text = response.error_text;
            $('#support_add_content_buttons_left .blue_button').html('Отправить');
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            if (error_text) {
                if (error_text.indexOf('короткий заголовок') > -1) {
                    $('#support_add_field_theme').addClass('field_error_i');
                } else if (error_text.indexOf('короткий текст') > -1) {
                    $('#support_add_field_text').addClass('field_error_i');
                } else {
                    nav.error_head(error_text);
                }
            } else if (response.success == 1) {
                $('#support_add_content_buttons_left .blue_button').html('<div class="upload"></div>');
                nav.go('', '/support/question?id=' + response.id);
            } else {
                $('#error_msg_support_error').html('Неизвестная ошибка.');
            }
        });
    },
    _add_comment: function(id) {
        $('#support_question_add_comment_field_text').removeClass('field_error_i');
        $('#support_images_attach_img_field_ids').val('');
        var message = _val('#support_question_add_comment_field_text') ? $('#support_question_add_comment_field_text').val() : '';
        $('#error_msg_support_error').html('');
        $('.support_images_attach_img_field_id').each(function() {
            var id = $(this).val() ? $(this).val() + ',' : '';
            var values = $('#support_images_attach_img_field_ids').val();
            $('#support_images_attach_img_field_ids').val(values + '' + id);
        });
        if (message.length < 2) {
            $('#support_question_add_comment_field_text').focus();
            $('#support_question_add_comment_field_text').addClass('field_error_i');
            return;
        }
        $('#support_question_add_comment_txt_buttons_left .blue_button').html('<div class="upload"></div>');
        $.post('/support/add.comment', {
            id: id,
            photo_attaches: $('#support_images_attach_img_field_ids').val(),
            text: message
        }, function(data) {
            var response = $.parseJSON(data);
            var error_text = response.error_text;
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            $('#support_question_add_comment_txt_buttons_left .blue_button').html('Отправить');
            if (error_text) {
                if (error_text.indexOf('короткий комментарий') > -1) {
                    $('#support_question_add_comment_field_text').addClass('field_error_i');
                } else {
                    nav.error_head(error_text);
                }
            } else if (response.success == 1) {
                $('#support_question_add_comment_txt_buttons_left .blue_button').html('<div class="upload"></div>');
                nav.go('', '/support/question?id=' + id);
            } else {
                $('#error_msg_support_error').html('Неизвестная ошибка.');
            }
        });
    },
    _delete_post: function(id) {
        var text = 'Вы действительно хотите удалить вопрос? Это действие нельзя будет отменить.';
        _wndBlue._show(410, {
            title: 'Удаление вопроса',
            text: '<div id="task_del_box">' + text + '</div>',
            _wsend: 80,
            _tsend: 'Удалить',
            _send: function() {
                $('#box_button_blue .blue_button').html('<div class="upload"></div>');
                $.getJSON('/support/del.post', {
                    id: id
                }, function(data) {
                    $('#box_button_blue .blue_button').html('<div class="upload"></div>');
                    _wndBlue._closed();
                    nav.go('', '/support?que_del=1');
                });
            }
        });
    },
    _rate_comment: function(id, type) {
        $.getJSON('/support/rate.comment', {
            id: id,
            type: type
        }, function(data) {
            var response = data;
            var error_text = response.error_text;
            if (error_text == 'login') {
                nav.go('', '/');
                return;
            }
            if (response.success == 1) {
                if (type == 1) {
                    $('#rate_support_comment' + id).html('<span class="rate_result_comment_support">Вы оставили положительный отзыв</span>');
                } else {
                    $('#rate_support_comment' + id).html('<span class="rate_result_comment_support">Вы оставили негативный отзыв</span>');
                }
            } else {
                nav.error_head('Неизвестная ошибка.');
            }
        });
    },
    upload_img: function(type) {
        if ($('#support_images_attach').find('img').size() >= 5) {
            _wndBlue._show(410, {
                title: 'Ошибка',
                text: '<div id="task_del_box">Вы можете прикрепить к вопросу не более 5 файлов.</div>'
            });
            return false;
        }
        var file_split = $('#support_add_img_file').val().split('\\');
        var filename = file_split[file_split.length - 1];
        $('#support_upload_iframe').contents().text('');
        $('#support_upload_iframe_submit').trigger('click');
        if (type == 'comment') {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow progress_grad_support_overflow_comment">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        } else {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        }
        var iframe_content_interval = setInterval(function() {
            var iframe_content = $('#support_upload_iframe').contents().text();
            if (iframe_content) {
                $('#support_grad_progress').hide();
                var iframe_content_json = $.parseJSON(iframe_content);
                var iframe_content_json_error = iframe_content_json.error_text;
                var iframe_content_json_big_img = iframe_content_json.result_big_file;
                var iframe_content_json_mini_img = iframe_content_json.result_mini_file;
                var iframe_content_json_id = iframe_content_json.id;
                if (iframe_content_json.error_text == 'login') {
                    nav.go('', '/');
                }
                if (iframe_content_json.error_text) {
                    nav.error_head(iframe_content_json.error_text);
                } else if (iframe_content_json.success == 1) {
                    $('#support_images_attach').show().append('\
     <span id="support_images_attach_img_wrap_id' + iframe_content_json_id + '">\
      <input type="hidden" value="' + iframe_content_json_id + '" class="support_images_attach_img_field_id"> \
      <div class="support_images_attach_img_wrap">\
       <div class="support_images_attach_img_inner">\
        <div onclick="support.del_img(' + iframe_content_json_id + ')" class="support_images_attach_img_inner_bdel"><div class="support_images_attach_img_inner_bdel_closed"></div></div>\
       </div>\
       <a href="/images/support/uploads/' + iframe_content_json_big_img + '" target="_blank"><img src="/images/support/uploads/' + iframe_content_json_mini_img + '"></a>\
      </div>\
     </div>\
     ');
                }
                clearInterval(iframe_content_interval);
            }
        }, 10);
    },
    del_img: function(id) {
        $('#support_images_attach_img_wrap_id' + id).remove();
    },
    _support_show: function() {
        support_show = 1;
        if (support_show) {
            $('#support_add_content').show();
        }
    }
}
var balance = {
    _next: function() {
        var page_num_id = $('#next_page_small_d');
        var page_num = page_num_id.text();
        $('#next_page_small_t').show().html('<div class="upload"></div>');
        $.get('/settings/balance.next', {
            page: page_num
        }, function(data) {
            var response = data;
            $('#next_page_small_t').html('Показать предыдущие платежи');
            if (response) {
                $('#next_page_small_c').append(response);
                page_num_id.text(page_num_id.text() * 1 + 1);
            } else {
                $('#next_page_small_t').hide();
            }
        });
    }
}
var pay = {
    _show_type: function() {
        pay._type_robokassa();
    },
    _type_robokassa: function() {
        var template = '\
  <div id="balance_pay_white">\
   Пожертвования осуществляется с помощью сервиса приёма платежей <b>ROBOKASSA</b>.\
   <div align="center" id="balance_pay_loader">\
    <div class="progress7"></div>\
    <div class="progress7_text">Ожидаем завершения оплаты.. <a href="javascript://" onclick="pay._type_loader_hide();">Отмена</a></div>\
   </div>\
   <div id="balance_pay_no_loader">\
    <div id="pay_error_msgm" class="error_msgm">Не удалось обнаружить Ваш платёж. <a href="javascript://" onclick="pay._check_webmoney();">Перепроверить</a>.</div>\
    <div id="balance_pay_inner">\
     <div style="overflow: hidden">\
      <div style="float: left">\
       <div onclick="$(\'#box_white_pay_count\').val(50)" class="pay_radio_btn pay_radio_btn_y" id="radiobtn1"></div>\
       <div onclick="$(\'#box_white_pay_count\').val(100)" class="pay_radio_btn pay_radio_btn_y" id="radiobtn2"></div>\
       <div onclick="$(\'#box_white_pay_count\').val(200)" class="pay_radio_btn pay_radio_btn_y" id="radiobtn3"></div>\
       <div onclick="$(\'#box_white_pay_count\').val(300)" class="pay_radio_btn pay_radio_btn_y" id="radiobtn4"></div>\
      </div>\
      <div style="float: right; margin-right: 15px;">\
       <img style="max-width: 150px" src="/images/robokassa_logo.png">\
      </div>\
     </div>\
     <input type="hidden" id="box_white_pay_count">\
     <div onclick="$(\'.other_pay_balance\').show(); $(\'#box_white_pay_count\').val(\'\');" class="pay_radio_btn" id="radiobtn5"></div>\
     <div id="pay_button_get" class="blue_button_wrap small_blue_button"><div class="blue_button">Перейти к оплате</div></div>\
     <div style="margin-left: 5px" onclick="_wndWhite._closed()" class="return_button">Отмена</div>\
    </div>\
   </div>\
  </div>\
  \
  ';
        _wndWhite._show(530, {
            title: 'Пожертвования компании',
            onShow: function() {
                radiobtn._new('radiobtn1', {
                    title: '<div class="pay_title_balance">50 руб</div>'
                });
                radiobtn._new('radiobtn2', {
                    title: '<div class="pay_title_balance">100 руб</div>'
                });
                radiobtn._new('radiobtn3', {
                    title: '<div class="pay_title_balance">200 руб</div>'
                });
                radiobtn._new('radiobtn4', {
                    title: '<div class="pay_title_balance">300 руб</div>'
                });
                radiobtn._new('radiobtn5', {
                    title: '<div class="pay_title_no_balance">Другое количество <div class="other_pay_balance"><input iplaceholder="Укажите сумму" id="other_pay_balance" type="text"> <span id="pay_sum_rur_real"></span></div></div>'
                });
                $('#other_pay_balance').keyup(function() {
                    if (!$(this).val().match(/^([0-9]+)$/)) {
                        $(this).val('');
                        $('#pay_sum_rur_real').hide();
                    } else {
                        var result_points = Math.floor(($(this).val() * 1) * 1 * 10) / 10;
                        var result_points_desc = Math.floor(($(this).val() * 1) * 0.1);
                        $('#pay_sum_rur_real').show().html('');
                        $('#box_white_pay_count').val($(this).val());
                    }
                });
                $('#other_pay_balance').keydown(function(event) {
                    var keyCode = event.which;
                    if (keyCode == 13) $('#pay_button_get').click();
                });
                _placeholder('#other_pay_balance');
                $('.pay_radio_btn_y').click(function() {
                    $('.other_pay_balance').hide();
                });
                $('#pay_button_get').click(function() {
                    var points = $('#box_white_pay_count').val() * 1;
                    var amount = Math.floor(($('#box_white_pay_count').val() * 1) * 1 * 10) / 10;
                    if (points < 30) {
                        alert('Минимальная сумма - 30 руб!');
                        return false;
                    }
                    if (!points) {
                        return false;
                    } else {
                        window.location.href = '/pay/robokassa/index.php?act=points&amount=' + amount + '&description=Пожертвования';
                    }
                });
                $('#wnd_white_main').css({
                    position: 'fixed',
                    top: ($(window).height() - $('#wnd_white_main').height()) / 2,
                    left: ($(window).width() - 500) / 2,
                    width: 500
                });
            },
            text: template
        });
    }
}
var restore = {
    change_type: function(val) {
        $('#restore_type_val').val(val);
        if (val == 1) {
            $('#restore_next_button .blue_button').html('<div class="upload"></div>');
            nav.go('', '/restore?type=email');
        }
    },
    restore_email: function() {
        $('#restore_form_er_wrap').html('');
        $('#restore_field_code_o').hide();
        var email = $('#restore_field_email').val();
        var code = $('#restore_field_code').val();
        if (!email) {
            $('#restore_field_email').focus();
        } else {
            $('#restore_next_button .blue_button').html('<div class="upload"></div>');
            $.getJSON('/restore/email', {
                email: email,
                code: code
            }, function(data) {
                var response = data;
                var error_text = response.error_text;
                $('#restore_next_button .blue_button').html('Продолжить');
                if (error_text) {
                    $('#restore_form_er_wrap').html('<div id="restore_form_er" class="error_msg">' + error_text + '</div>');
                } else if (response.email) {
                    $('#restore_form_er_wrap').html('<div id="restore_form_er" class="info_restore_msg">На Ваш e-mail <b>' + response.email + '</b>, указанный при регистрации, отправлен проверочный код. Введите его в форму ниже и нажмите "Продолжить".</div>');
                    $('#restore_field_code_o').show('toggle');
                } else if (response.success == 1) {
                    $('#restore_types_checks').html('<div id="restore_form_er" class="info_restore_msg"><b>Заявка на восстановление доступа одобрена!</b><br /> <div style="padding-left: 5px; padding-top: 5px;">Логин для входа: <b>' + response.ulogin + '</b> <br /> Пароль для входа: <b>' + response.upassword + '</b></div></div> <div id="restore_g_help">В дальнейшем, Вы можете сменить пароль на свой, зайдя в аккаунт и перейдя в раздел <b>Мои настройки</b>.</div>');
                } else {
                    alert(response.text);
                }
            });
        }
    }
}
var promo_new = {
    _promo_pravila: function(uid) {
        var template = '\
            <div id="new_flat_form_description">\
                <div id="new_flat_form_title">Пожалуйста ознакомьтесь с правилами компании.</div>\
                <div id="new_flat_form_description_text">\
                В компанию "Russian Express" принимаются люди строго от 18-ти лет. Если вы приняли правила <br>(Ваше действие записывается в логи на сайте, патом при нарушении отмазаться не получиться.) \
                </div>\
               </div>\
               <div id="new_flat_form_error_msg" class="error_msgm"></div>\
               <div id="new_flat_form_send_form2" style="margin: 0 auto; padding-top: 10px; padding-bottom: 10px;overflow-y: scroll;height: 300px;">\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
                Сотрудник вступивший в ВТК <b>Russian Express</b> принимается на испытательный срок (1 неделя), по истечению которого, принимается решение на полное оформление в ВТК или отказ от участия в жизни компании (как с нашей стороны, так и с вашей) <br> Новые сотрудники, в обязательном порядке, должны быть подписаны на рассылку регламента о конвое\собрании. <br> У Сотрудников не должны быть скрытые аккаунты в VK и Steam (Для того что бы администрация компании могла связаться с вами)\
        </div>\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
        <b>\
        1. Общие положения:</b><br>1.1. Сотрудник, вступивший в ВТК RussianExpress обязан: <br> 1.2. Иметь микрофон и программу TeamSpeak3. (https://www.teamspeak.com/downloads.html) <br> 1.3. Перекрашивать свой тягач на время конвоя в официальную раскраску компании. (SunRise, видео присутствует в закрытой группе компании) <br> 1.4. Поставить приставку RussianExpress: перед игровым ником на сайте www.truckersMP.com (Двоеточие оставить) <br> 1.5. Желательно уметь пользоваться программой Tedit. <br> 1.6. Иметь стабильный онлайн. (Присутствовать не менее чем на 6 конвоях в месяц) <br> 1.7. Проявлять уважение ко всем участникам движения и быть адекватным.\
        </div>\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
        <b>\
        2. Правила проведения конвоев: </b><br>2.1. Сотрудник должен прибыть на старт конвоя на подготовленной машине (Регламент выкладывается за ранее) <br> 2.2. Сотрудник должен помнить свой порядковый номер и место в колонне (Номера обновляются в конце месяца) <br> 2.3. Иметь на карте проложенный маршрут (согласно регламенту) <br> 2.4. Соблюдать требования Администрации\Ведущего\Замыкающего. <br> 2.5. В случае каких-либо неполадок (вылет из игры) сотрудник должен предупредить колонну. Так же, если надо отойти от компьютера, сотрудник называет свой ник и останавливается на обочине.\
        </div>\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
        <b>\
        2.6. На конвое компании запрещено: </b><br>2.6.1. Нарушать рядность и очередность в соответствии с порядковым номером без разрешения Администратора\Ведущего или без согласия участника, которого собираетесь обойти. За исключением ухода от ДТП. <br> 2.6.2. Нарушать тишину в ТС после соответствующей команды <br> 2.6.3. Хамить и оскорблять Любых участников движения, как в канале ТС, так и по рации\в чате. <br> 2.6.4. Категорически запрещено использование СпидХака во время конвоя. <br> 2.6.5. Во время конвоя запрещено использование проблесковых маячков и злоупотребление звуковым сигналом (Согласно правилам Мультиплеера за это выписывают БАН) <br> 2.6.6. К конвою НЕ допускаются сотрудники в наркотическом\алкогольном опьянении, а также сотрудники с "Внешним заказом" или с ограничителем скорости. <br> Прим: <br> -За нарушение одного\нескольких пунктов настоящих правил, сотрудник может быть снят с конвоя\забанен в ТС или уволен из компании. <br> -Для сотрудников, чей аккаунт заблокирован (БАН) и ТОЛЬКО после оценки причины (Фото\видео), Администрация предлагает воспользоваться Дежурным аккаунтом для участия в конвоях. К пользователю данного аккаунта применяются особые правила. В случае выдачи временного бана от Администрации TruckersMP, сотрудник понижается в должности до "Стажера" с обнулением всех баллов. В случае перманентного БАНа от Администрации игры, сотрудник увольняется без права восстановления. Кроме того, все крупные компании на VTCpanel`e оповещаются об этом.\
        </div>\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
        <b>\
        3. Правила нахождения в TeamSpeak: </b><br>3.1. Желающие вступить в ВТК RussianExpress, автоматически попадают в "Гостевую" комнату. <br> Далее, они могут перейти в соответствующий канал "Прием в компанию RussianExpress" или "Прием в компанию LIMMA", где они ожидают Администрацию. <br> 3.1. Все сотрудники компании должны проводить конвои покатушки и т.д. в комнатах предназначенных для этого. «Конвой 1, Конвой 2, Конвой 3» и другие комнаты предназначенные для этого. <br> 3.2. В комнате "свободное общение" сидят друзья Игроков, находящихся в компании. <br> 3.3. Гости компании назначаются Администраторами на свое усмотрение. <br> 3.4. Флуд\спам\не адекватное поведение, пресекаются. <br> 3.5. Микрофон ОБЯЗАТЕЛЬНО должен быть настроен на "Кнопку" <br> 3.6. Все Закрытые/Открытые/Совместные конвои проводятся строго в отведенной для этого комнате.\
        </div>\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
        <b>\
        4. Штрафные баллы: </b><br>-Баллы получают за одно или несколько нарушений действующих правил. Нарушения суммируются. <br> -штрафные баллы можно погасить двумя способами. <br> На часть пунктов, настоящих правил действует скидка 50%, если штраф погашен в течении 3х дней, с момента выдачи. <br> Первый способ: <br> -1 штрафной балл равен 1000км. с "Внешним заказом". Т.Е Человек, получивший балл, должен взять любой внешний заказ с пробегом не менее 1000км. Но если 3 дня, с момента выдачи балла, не прошли, то разрешается проехать 500км. <br> Второй способ: <br> Оплатить штраф Перевести на счет компании 5 рублей. (Переведенные средства, ни в коем случае не уходят какому-либо человеку "В карман". Они идут на развитие компании. (Оплата ТС/Реклама/Призы на конкурсы)) <br> 4.1. Один штрафной балл начисляется за: <br> - Не соблюдение рядности в колонне (Все автомобили в колонне идут строго друг за другом), за исключением моментов связанных с предотвращением ДТП. <br> - Не предоставлении приоритета на регулируемых\не регулируемых перекрестах. (Вся колонна проходит регулируемые перекрёстки на красный сигнал светофора ТОЛЬКО, если не создаются помехи движению других ТС или по указанию ведущего. <br> - За выезд на перекресток при заторе. <br> - За необоснованное торможение (Если машина глохнет, то включается аварийка или машина останавливается на обочине и отправляется на СТО) <br> - За намеренное движение по тротуарам. <br> - За встраивание в колонну перед замыкающим без предупреждения или без команды замыкающего. <br> - За "Лишние" звуки в канал ТС (Чихание\кашель\микрофон в ТС настроен на активацию голосом) во время конвоя\разговоры не по теме или перебивание, говорящего человека. "Балаган" в канале ТС. (Во время конвоя, если говорит ведущий, все должны соблюдать тишину. <br> - За ответы на вопросы по маршруту/скорости/расстояния, которые заданы не вам. <br> - За Оскорбление сотрудника/участника движения как в канале ТС, так и в внутри игровой рации или чате. <br> - За прогул обговоренного экзамена без уведомления Инструктора/ уважительной причины (Сгорел комп/в пробке/на работе/бабушка приехала, не считается уважительной причиной. Предупредить можно с телефона) <br> 4.2. Два штрафных балла начисляются: <br> - За Перебивание Администратора/Ведущего/Замыкающего/Проверяющего. <br>  - За полное нежелание слушать команды ведущего/замыкающего, касающиеся конвоя (за данное деяние сотрудник может быть уволен без объяснения причин). <br> - За ДТП в колонне из-за несоблюдения дистанции. (Все машины двигаются на свободной дистанции, но не более 200 метров друг от друга) <br> Штраф за минимальную дистанцию отсутствует. <br> - За выяснение отношений на дороге во время движения колонны. <br> - За обгон кого-либо в колонне без разрешения/указания причины. (Кроме случаев по предотвращению ДТП) <br> - За обгон на узкоколейках. В том числе встраивание перед замыкающим. <br> - За использование не нормативной лексики после замечания Администратора. (Все-таки у многих дети/мамы/бабушки рядом) <br> - За Съезд с маршрута. Все машины в колонне двигаются за ведущим что бы не случилось. <br> 4.3. Пять штрафных баллов начисляется за: <br> - За ДТП с иными участниками движения (Не предоставление приоритета без ДТП считается отдельно. В случае ДТП по вашей вине на перекрестке/съезде/выезде начисляются 5 баллов) <br> - За разжигание межнациональной розни или предвзятого отношения к другим сотрудникам компании. (Все баталии происходят после конвоя, а не вовремя) <br> - За выяснение отношений в канале ТС во время движения колонны. <br> - За использование СпидХака во время колонны. <br> 4.4. Прочие нарушения: <br> - За использование СпидХака вне конвоя (с доказательством от какого-либо игрока) с приставкой RussianExpress - 5 штрафных баллов и минус 5 баллов из личного рейтинга водителя в таблице. Без права оплаты и скидки. <br> - За неадекватное поведение во время конвоя, по решению Администрации, сотрудник может быть отстранен от конвоя или уволен, без объяснения причины. <br> - Для сотрудников, которые находятся на испытательном сроке, за нарушение пункта 4.1 - выносится предупреждение. За нарушение пункта 4.2 - начисляется 0.5 штрафных балла без права погашения со скидкой или оплатой. <br> За нарушение пункта 4.3 - сотрудник на испытательном сроке увольняется. <br> - Если сотрудник на испытательном сроке не откатывает их до окончания испытательного срока - сотрудник так же увольняется. <br> - Если сотрудник, прошедший исп. срок, и получивший штрафные баллы не гасит их в течении 15ти дней с момента начисления, то баллы удваиваются. Если сотрудник не гасит штрафные баллы в течении 30ти дней - Выносится вопрос о его увольнении из компании.\
        </div>\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
        <b>\
        5. Стримерам: </b><br> 5.1. Запрещено показывать маршрут конвоя вовремя стрима. <br> 5.2. Запрещается говорить маршрут игрокам вне компании. <br> 5.3. На Видео должны присутствовать оверлеи, названия компании и адрес ТС (194.58.107.174:9999). Под видео должна быть ссылка на открытую группу компании. (https://vk.com/club129589437) <br> 5.4. Подписчики "Стриммера" не должны мешать движению конвоя. <br> 5.5. Подписчики должны находится за замыкающем колонны и не препятствовать обгону сотрудников, которые догоняют конвой по каким-либо причинам (Заезд на СТО/АЗС) <br> - При нарушении, "Подписчик" отправляется в Бан или на СТО. (По усмотрению сопровождающего или Администрации)\
        </div>\
        <div class="text_block" style="padding: 0px 25px;line-height: 18px;margin-bottom: 7px;">\
        <b>\
        6. Правила сайта: </b><br> 6.1. Запрещено накручивать баллы путём пожертвования (Бан на сайте на усмотрения разработчика). <br> 6.2. Запрещено оскорблять других участников в чате (Бан в чате на усмотрения администратора). <br> 6.3. Запрещено показывать внутренний интерфейс сайта, [ВИДЕО, СКРИНШОТЫ И ДР.] - (Бан на сайте на усмотрения разработчика). <br> 6.4. Запрещено копировать какую-либо информацию с сайта без разрешения разработчика, [ИКОНКИ, ИЗОБРАЖЕНИЯ И ДР] - (Бан на сайте на усмотрения разработчика).\
        </div>\
            </div>\
           </div>\
               </div>\
    ';
        _wndBlue._show(585, {
            title: 'Правила компании',
            text: template,
            _wsend: 150,
            _tsend: 'С правилами ознакомлен',
            _send: function() {
                $('#box_button_blue .blue_button').html('<div class="upload"></div>');
                $.post('/modules/users/rules.php', {
                    uid: uid,
                    rules: 1
                }, function(data) {
                    var result = $.parseJSON(data);
                    var error_msg = result.error_msg;
                    $('#new_flat_form_button .blue_button').html('С правилами ознакомлен <div class="reg_form_white_breg_right"></div>');
                    if (error_msg) {
                        $('#new_flat_form_error_msg').show().html(error_msg + '.');
                    } else if (result.response == 1) {
                        _wndBlue._closed();
                        _wndBottom._show('Правила компании', 'Мы очень рады что вы ознакомились с правилами. Следите за обновлениями правил в разделе - <a href="/page/rules" onclick="nav.go(this); return false">правила</a>', {
                            img: '/images/wnd_bottom/send_tasks.png?1'
                        });
                        $('body').removeClass('rules');
                    } else {
                        $('#new_flat_form_error_msg').show().html('Неизвестная ошибка.');
                    }
                });
            }
        });
    }
}
var exchange = {
    voutes: function() {
        var count = $('#exchange_count_voutes');
        $('#exchange_form_error').hide();
        if (count.val() < 1) {
            count.focus();
            return false;
        }
        $('#exchange_points_button .blue_button').html('<div class="upload"></div>');
        $.post('/exchange/voutes', {
            count_nik: $('#exchange_count_nik').val(),
            count: count.val()
        }, function(data) {
            var result = $.parseJSON(data);
            var error_msg = result.error_msg;
            $('#exchange_points_button .blue_button').html('Отправить');
            if (error_msg) {
                $('#exchange_form_error').show().html(error_msg);
            } else if (result.response == 1) {
                _money._minus(parseInt(result.points));
                $('#exchange_history').show();
                var text = $('#exchange_count_nik').val();
                $('#exchange_history_ajax').prepend('\
     <div class="exchange_history">\
      <div class="exchange_history_left">\
       Смена никнейма с <b>' + text + '</b> на <b>' + result.count + '</b>\
      </div>\
      <div class="exchange_history_right exchange_history_status_wait">\
       Рассматривается..\
      </div>\
     </div>\
    ');
            } else {
                $('#exchange_form_error').show().html('Неизвестная ошибка');
            }
        });
    },
    set_status: function(id, status) {
        if (status == 2) {
            $('#admin_exchange_requests_button_ok' + id + ' .blue_button').html('<div class="upload"></div>');
        } else {
            $('#admin_exchange_requests_button_error' + id + ' .blue_button').html('<div class="upload"></div>');
        }
        $.getJSON('/exchange/set_status', {
            id: id,
            status: status
        }, function(data) {
            $('#admin_exchange_requests' + id).hide();
        });
    }
}
var urgent = {
    voutes: function(uid) {
        var count = $('#exchange_count_voutes');
        $('#exchange_form_error').hide();
        if (count.val() < 1) {
            count.focus();
            return false;
        }
        $('#support_images_attach_img_field_ids').val('');
        $('.support_images_attach_img_field_id').each(function() {
            var id = $(this).val() ? $(this).val() + ',' : '';
            var values = $('#support_images_attach_img_field_ids').val();
            $('#support_images_attach_img_field_ids').val(values + '' + id);
        });
        $('#exchange_points_button .blue_button').html('<div class="upload"></div>');
        $.post('/urgent/voutes', {
            photo_attaches: $('#support_images_attach_img_field_ids').val(),
            count_nik: $('#exchange_count_nik').val(),
            count: count.val()
        }, function(data) {
            var result = $.parseJSON(data);
            var error_msg = result.error_msg;
            $('#exchange_points_button .blue_button').html('Отправить');
            if (error_msg) {
                $('#exchange_form_error').show().html(error_msg);
            } else if (result.response == 1) {
                _money._minus(parseInt(result.points));
                $('#express_content_send').show();
                var text = $('#images_attach_img_field_url').val();
                $('#exchange_history_ajax').prepend('\
      <div class="block" style="margin-right: 9px;width: 97%;margin-top: 15px;margin-bottom: -5px;">\
       <div class="price_wait">Рассматривается..</div>\
        <div class="time">Отправка заявки</div>\
        <ul style="padding-bottom: 0;">\
        <li>Проехал км: <b>' + result.count + ' км</b></li>\
        <li>Скриншот: <a href="/images/uploads/urgent/' + text + '" target="_blank">Посмотреть скриншот...</a> </li>\
      </ul>\
    </div>\
    ');
            } else {
                $('#exchange_form_error').show().html('Неизвестная ошибка');
            }
        });
    },
    addurgent: function(uid) {
        var template = '\
            <div id="new_flat_form_description">\
                <div id="new_flat_form_title">Здесь вы сможете отправить отчет о срочном заказе</div>\
                <div id="new_flat_form_description_text">\
                Пожалуйста укажите ваш пробег в км а так-же прикрепите скриншот с табом.\
                </div>\
               </div>\
               <div id="new_flat_form_error_msg" class="error_msgm"></div>\
               <div id="new_flat_form_send_form">\
                <div class="new_flat_form_field_label">Ваш пробег км:</div>\
                <div class="new_flat_form_field"><input type="text" id="exchange_count_voutes"></div>\
                <div class="new_flat_form_field_label">Сколько заработано денег?</div>\
                <div class="new_flat_form_field"><input type="text" id="urgent_money"></div>\
           <div id="support_images_attach"></div>\
                 <input type="hidden" id="support_images_attach_img_field_ids">\
                 <div id="support_grad_progress"></div>\
                 <div id="support_add_content_buttons">\
                 <div id="express_content_send_buttons_right">\
                 <a href="javascript://" id="test_images">\
                Прикрепить изображение\
                <iframe id="support_upload_iframe" name="support_upload_iframe"></iframe>\
                <form method="post" enctype="multipart/form-data" action="/urgent/img.upload" target="support_upload_iframe">\
                <input id="support_add_img_file" onchange="urgent.upload_img(); return false;" type="file" name="file">\
                <input id="support_upload_iframe_submit" style="display: none;" type="submit">\
                </form>\
              </a>\
            </div>\
           </div>\
                <div id="new_flat_form_button" class="blue_button_wrap"><div class="blue_button">Отправить <div class="reg_form_white_breg_right"></div></div></div>\
               </div>\
              ';
        _wndBlue._show(500, {
            title: 'Отправка отчета <div class="users_new_left">#' + uid + '</div>',
            text: template,
            onContent: function() {
                $('#new_flat_form_field input').keydown(function(event) {
                    var keyCode = event.which;
                    if (keyCode == 13) {
                        $('#new_flat_form_button').click();
                        return false;
                    }
                });
                $('#new_flat_form_button').click(function() {
                    var count = $('#exchange_count_voutes').val();
                    var money = $('#urgent_money').val();
                    $('#support_images_attach_img_field_ids').val('');
                    $('.support_images_attach_img_field_id').each(function() {
                        var id = $(this).val() ? $(this).val() + ',' : '';
                        var values = $('#support_images_attach_img_field_ids').val();
                        $('#support_images_attach_img_field_ids').val(values + '' + id);
                    });
                    if (!count) {
                        $('#exchange_count_voutes').focus();
                    } else if (!money) {
                        $('#urgent_money').focus();
                    } else {
                        $('#new_flat_form_error_msg').hide();
                        $('#new_flat_form_button .blue_button').html('<div class="upload"></div>');
                        $.post('/urgent/voutes', {
                            uid: uid,
                            photo_attaches: $('#support_images_attach_img_field_ids').val(),
                            count_nik: $('#exchange_count_nik').val(),
                            count: count,
                            money: money
                        }, function(data) {
                            var result = $.parseJSON(data);
                            var error_msg = result.error_msg;
                            $('#new_flat_form_button .blue_button').html('Продолжить <div class="reg_form_white_breg_right"></div>');
                            if (error_msg) {
                                $('#new_flat_form_error_msg').show().html(error_msg + '.');
                            } else if (result.response == 1) {
                                _wndBlue._closed();
                                _wndBottom._show('Срочные заказы', 'Отчет был отправлен, после рассмотрения администратором вы получите уведомление.', {
                                    img: '/images/wnd_bottom/send_tasks.png?1'
                                });
                            } else {
                                $('#new_flat_form_error_msg').show().html('Неизвестная ошибка.');
                            }
                        });
                    }
                });
            }
        });
    },
    set_status: function(id, status) {
        $.getJSON('/urgent/set_status', {
            id: id,
            status: status
        }, function(data) {
            $('#admin_exchange_requests' + id).hide();
        });
    },
    upload_img: function(type) {
        if ($('#support_images_attach').find('img').size() >= 2) {
            _wndBlue._show(410, {
                title: 'Ошибка',
                text: '<div id="task_del_box">Вы можете прикрепить не более 2-х скриншотов.</div>'
            });
            return false;
        }
        var file_split = $('#support_add_img_file').val().split('\\');
        var filename = file_split[file_split.length - 1];
        $('#support_upload_iframe').contents().text('');
        $('#support_upload_iframe_submit').trigger('click');
        if (type == 'comment') {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow progress_grad_support_overflow_comment">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        } else {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        }
        var iframe_content_interval = setInterval(function() {
            var iframe_content = $('#support_upload_iframe').contents().text();
            if (iframe_content) {
                $('#support_grad_progress').hide();
                var iframe_content_json = $.parseJSON(iframe_content);
                var iframe_content_json_error = iframe_content_json.error_text;
                var iframe_content_json_big_img = iframe_content_json.result_big_file;
                var iframe_content_json_mini_img = iframe_content_json.result_mini_file;
                var iframe_content_json_id = iframe_content_json.id;
                if (iframe_content_json.error_text == 'login') {
                    nav.go('', '/');
                }
                if (iframe_content_json.error_text) {
                    nav.error_head(iframe_content_json.error_text);
                } else if (iframe_content_json.success == 1) {
                    $('#support_images_attach').show().append('\
     <span id="support_images_attach_img_wrap_id' + iframe_content_json_id + '">\
      <input type="hidden" value="' + iframe_content_json_id + '" class="support_images_attach_img_field_id"> \
      <div class="support_images_attach_img_wrap">\
       <div class="support_images_attach_img_inner">\
        <div onclick="support.del_img(' + iframe_content_json_id + ')" class="support_images_attach_img_inner_bdel"><div class="support_images_attach_img_inner_bdel_closed"></div></div>\
       </div>\
       <a href="/images/uploads/urgent/' + iframe_content_json_big_img + '" target="_blank"><img src="/images/uploads/urgent/' + iframe_content_json_mini_img + '"></a>\
     <input type="hidden" value="' + iframe_content_json_big_img + '" id="images_attach_img_field_url">\
      </div>\
     </div>\
     ');
                }
                clearInterval(iframe_content_interval);
            }
        }, 10);
    },
    del_img: function(id) {
        $('#support_images_attach_img_wrap_id' + id).remove();
    }
}
var penalty = {
    addpenalty: function(uid) {
        var template = '\
            <div id="new_flat_form_description">\
                <div id="new_flat_form_title">Данная функция позволит вам погасить штрафные баллы.</div>\
                <div id="new_flat_form_description_text">\
                Пожалуйста укажите ваш пробег в км а так-же прикрепите скриншот с табом.\
                </div>\
               </div>\
               <div id="new_flat_form_error_msg" class="error_msgm"></div>\
               <div id="new_flat_form_send_form">\
                <div class="new_flat_form_field_label">Ваш пробег км:</div>\
                <div class="new_flat_form_field"><input type="text" id="exchange_count_voutes"></div>\
                <div class="new_flat_form_field_label">Сколько заработано денег?</div>\
                <div class="new_flat_form_field"><input type="text" id="money"></div>\
           <div id="support_images_attach"></div>\
                 <input type="hidden" id="support_images_attach_img_field_ids">\
                 <div id="support_grad_progress"></div>\
                 <div id="support_add_content_buttons">\
                 <div id="express_content_send_buttons_right">\
                 <a href="javascript://" id="test_images">\
                Прикрепить изображение\
                <iframe id="support_upload_iframe" name="support_upload_iframe"></iframe>\
                <form method="post" enctype="multipart/form-data" action="/penalty/img.upload" target="support_upload_iframe">\
                <input id="support_add_img_file" onchange="penalty.upload_img(); return false;" type="file" name="file">\
                <input id="support_upload_iframe_submit" style="display: none;" type="submit">\
                </form>\
              </a>\
            </div>\
           </div>\
                <div id="new_flat_form_button" class="blue_button_wrap"><div class="blue_button">Отправить <div class="reg_form_white_breg_right"></div></div></div>\
               </div>\
              ';
        _wndBlue._show(500, {
            title: 'Погашение штрафа',
            text: template,
            onContent: function() {
                $('#new_flat_form_field input').keydown(function(event) {
                    var keyCode = event.which;
                    if (keyCode == 13) {
                        $('#new_flat_form_button').click();
                        return false;
                    }
                });
                $('#new_flat_form_button').click(function() {
                    var count = $('#exchange_count_voutes').val();
                    var money = $('#money').val();
                    $('#support_images_attach_img_field_ids').val('');
                    $('.support_images_attach_img_field_id').each(function() {
                        var id = $(this).val() ? $(this).val() + ',' : '';
                        var values = $('#support_images_attach_img_field_ids').val();
                        $('#support_images_attach_img_field_ids').val(values + '' + id);
                    });
                    if (!count) {
                        $('#exchange_count_voutes').focus();
                    } else if (!money) {
                        $('#money').focus();
                    } else {
                        $('#new_flat_form_error_msg').hide();
                        $('#new_flat_form_button .blue_button').html('<div class="upload"></div>');
                        $.post('/penalty/voutes', {
                            uid: uid,
                            photo_attaches: $('#support_images_attach_img_field_ids').val(),
                            count_nik: $('#exchange_count_nik').val(),
                            count: count,
                            upoints: $('#upoints' + uid + ' ').val(),
                            money: money
                        }, function(data) {
                            var result = $.parseJSON(data);
                            var error_msg = result.error_msg;
                            $('#new_flat_form_button .blue_button').html('Продолжить <div class="reg_form_white_breg_right"></div>');
                            if (error_msg) {
                                $('#new_flat_form_error_msg').show().html(error_msg + '.');
                            } else if (result.response == 1) {
                                _wndBlue._closed();
                                _wndBottom._show('Штрафные баллы', 'Ваша заявка была отправлена ожидайте рассмотрения администратором.', {
                                    img: '/images/wnd_bottom/send_tasks.png?1'
                                });
                                $('#complaintsids' + uid + ' .footer').html('<div class="buttons" style="opacity:0.5;"><div class="blue_button_wrap"><div class="blue_button">Погасить штраф</div></div><div class="blue_button_wrap" style="margin-left: 4px;"><div class="blue_button">Оплатить штраф</div></div></div> <div class="status"><div class="result_success">Рассматривается..</div></div></div>');
                                //$('#complaintsids' + uid + ' .footer').html('<div class="buttons" style="opacity:0.5;"><div class="blue_button_wrap"><div class="blue_button">Погасить штраф</div></div></div> <div class="status"><div class="result_success">Рассматривается..</div></div>');
                            } else {
                                $('#new_flat_form_error_msg').show().html('Неизвестная ошибка.');
                            }
                        });
                    }
                });
            }
        });
    },
    set_status: function(id, status, uid) {
        $.getJSON('/penalty/set_status', {
            id: id,
            status: status,
            penalty_id: $('#penalty_id' + uid + ' ').val()
        }, function(data) {
            $('#admin_exchange_requests' + id).hide();
        });
    },
    upload_img: function(type) {
        if ($('#support_images_attach').find('img').size() >= 2) {
            _wndBlue._show(410, {
                title: 'Ошибка',
                text: '<div id="task_del_box">Вы можете прикрепить не более 2-х скриншотов.</div>'
            });
            return false;
        }
        var file_split = $('#support_add_img_file').val().split('\\');
        var filename = file_split[file_split.length - 1];
        $('#support_upload_iframe').contents().text('');
        $('#support_upload_iframe_submit').trigger('click');
        if (type == 'comment') {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow progress_grad_support_overflow_comment">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        } else {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        }
        var iframe_content_interval = setInterval(function() {
            var iframe_content = $('#support_upload_iframe').contents().text();
            if (iframe_content) {
                $('#support_grad_progress').hide();
                var iframe_content_json = $.parseJSON(iframe_content);
                var iframe_content_json_error = iframe_content_json.error_text;
                var iframe_content_json_big_img = iframe_content_json.result_big_file;
                var iframe_content_json_mini_img = iframe_content_json.result_mini_file;
                var iframe_content_json_id = iframe_content_json.id;
                if (iframe_content_json.error_text == 'login') {
                    nav.go('', '/');
                }
                if (iframe_content_json.error_text) {
                    nav.error_head(iframe_content_json.error_text);
                } else if (iframe_content_json.success == 1) {
                    $('#support_images_attach').show().append('\
     <span id="support_images_attach_img_wrap_id' + iframe_content_json_id + '">\
      <input type="hidden" value="' + iframe_content_json_id + '" class="support_images_attach_img_field_id"> \
      <div class="support_images_attach_img_wrap">\
       <div class="support_images_attach_img_inner">\
        <div onclick="support.del_img(' + iframe_content_json_id + ')" class="support_images_attach_img_inner_bdel"><div class="support_images_attach_img_inner_bdel_closed"></div></div>\
       </div>\
       <a href="/images/uploads/penalty/' + iframe_content_json_big_img + '" target="_blank"><img src="/images/uploads/penalty/' + iframe_content_json_mini_img + '"></a>\
     <input type="hidden" value="' + iframe_content_json_big_img + '" id="images_attach_img_field_url">\
      </div>\
     </div>\
     ');
                }
                clearInterval(iframe_content_interval);
            }
        }, 10);
    },
    del_img: function(id) {
        $('#support_images_attach_img_wrap_id' + id).remove();
    },
    fastOrderPayBox: function(uid) {
        var amount = ($('#upoints' + uid + ' ').val() * 20).toFixed(0);
        var upoints = $('#pay' + uid + ' ').val();
        var penalty_id = $('#pay_com_ids' + uid + ' ').val();
        var template = '<div style="padding: 15px; text-align: center"><div style="font-size: 12px">Что-бы погасить <b>' + upoints + '</b> ' + declOfNum(upoints, ['штрафной', 'штрафных', 'штрафных']) + ' ' + declOfNum(upoints, ['балл', 'балла', 'баллов']) + ', необходимо заплатить <b>' + amount + ' ' + declOfNum(amount, ['рубль', 'рубля', 'рублей']) + '</b>.</div> <div style="margin-top: 20px; width: 140px" class="blue_button_wrap" id="fast_order_pay_button" onclick="penalty.fastOrderPay(' + upoints + ', ' + uid + ')"><div class="blue_button">Оплатить штраф <div id="reg_form_white_breg_right"></div></div></div></div>';
        _wndWhite._show(400, {
            title: 'Погашение штрафа',
            text: template
        });
    },
    fastOrderPay: function(upoints, uid) {
        var amount = ($('#upoints' + uid + ' ').val() * 1).toFixed(0);
        var unique_key = $('#fast_order_unique_key').val();
        var penalty_id = $('#pay_com_ids' + uid + ' ').val();
        if (!amount) {
            return false;
        }
        window.location.href = '/pay/robokassa/index.php?act=penalty&amount=' + amount + '&unique_key=' + unique_key + '&penalty_id=' + penalty_id + '&description=Погашение штрафа';
        $('#fast_order_pay_button .blue_button').html('<div class="upload"></div>');
    },
    send_delete: function(uid, upoints, user_id) {
        var text = 'Вы можете подать на апиляцию штраф, если вы не согласны со штрафом. <br /> <br />\Рассмотрение заявок, происходит в ручном режиме и занимает до 12 часов!';
        _wndBlue._show(430, {
            title: 'Апелляция штрафа',
            text: '<div id="task_del_box">' + text + '</div>',
            _wsend: 80,
            _tsend: 'Отправить',
            _send: function() {
                $('#box_button_blue .blue_button').html('<div class="upload"></div>');
                $.post('/penalty/send_appeal', {
                    uid: uid,
                    upoints: upoints,
                    user_id: user_id,
                    admin_text: $('#fast_admin_text' + uid + ' ').val()
                }, function(data) {
                    var result = $.parseJSON(data);
                    var error_msg = result.error_msg;
                    $('#new_flat_form_button .blue_button').html('Продолжить <div class="reg_form_white_breg_right"></div>');
                    if (error_msg) {
                        $('#new_flat_form_error_msg').show().html(error_msg + '.');
                    } else if (result.response == 1) {
                        _wndBlue._closed();
                        _wndBottom._show('Штрафные баллы', 'Ваша заявка была отправлена ожидайте рассмотрения администратором.', {
                            img: '/images/wnd_bottom/send_tasks.png?1'
                        });
                        $('#complaintsids' + uid + ' .footer').html('<div class="buttons" style="opacity:0.5;"><div class="blue_button_wrap"><div class="blue_button">Погасить штраф</div></div><div class="blue_button_wrap" style="margin-left: 4px;"><div class="blue_button">Оплатить штраф</div></div></div> <div class="status"><div class="result_success">Рассматривается..</div></div></div>');
                    } else {
                        $('#new_flat_form_error_msg').show().html('Неизвестная ошибка.');
                    }
                });
            }
        });
    },
    set_appeal: function(id, status, uid) {
        $.getJSON('/penalty/set_appeal', {
            id: id,
            status: status,
            penalty_id: $('#penalty_id' + uid + ' ').val()
        }, function(data) {
            $('#admin_exchange_requests' + id).hide();
        });
    },
    set_error_head: function() {
        nav.error_head('Повторно отправить заявку на апелляцию нельзя.');
        nav.loader_page_hide();
    }
}

function authvk(token) {
    $.getJSON("//ulogin.ru/token.php?host=" + encodeURIComponent(location.toString()) + "&token=" + token + "&callback=?", function(data) {
        data = $.parseJSON(data.toString());
        if (!data.error) {
            $.post('/auth_vk', {
                uvk_id: data.uid
            }, function(data) {
                var response = $.parseJSON(data);
                var error_title = response.error_title;
                var error_text = response.error_text;
                if ($('#login_in').html()) {
                    $('#login_in .new_flat_button_auth').html('Войти через ВКонтакте');
                } else {
                    $('#new_main_header_logo_form_overflow_right_button, #flat_auth_submit_button .new_flat_button_auth').html('Войти через ВКонтакте');
                }
                if (error_text) {
                    $('#login_form_error').show().html(error_text);
                } else if (response.response == 1) {
                    window.location.href = '/tasks';
                }
            });
        }
    });
}

function reg_get_vk(token) {
    $.getJSON("//ulogin.ru/token.php?host=" + encodeURIComponent(location.toString()) + "&token=" + token + "&callback=?", function(data) {
        data = $.parseJSON(data.toString());
        if(!data.error) {
            $('.reg_vk_add').hide();
            $('#ajax_vk').html('<input type="text" id="reg_vk_id" disabled="true" value="vk.com/id'+ data.uid +'"><input type="hidden" value="'+ data.uid +'" id="reg_vk_id_ajax" />');
        }
    });
}

function addvk(token) {
    $.getJSON("//ulogin.ru/token.php?host=" + encodeURIComponent(location.toString()) + "&token=" + token + "&callback=?", function(data) {
        data = $.parseJSON(data.toString());
        if (!data.error) {
            $.post('/f.vk.add', {
                uvk_id: data.uid
            }, function(data) {
                var response = $.parseJSON(data);
                var error_title = response.error_title;
                var error_text = response.error_text;
                if (error_text) {
                    $('#my_settings_account_new_vk_error').show().html(error_text);
                } else if (response.success == 1) {
                    cnt_black._show({
                        title: 'Страница ВКонтакте прикреплена.',
                        text: response.text
                    });
                    $('.field_settings_vk').html('vk.com/id' + response.vkid + ' ');
                    $('#button').html('<div id="settings_steam_button" class="blue_button_wrap small_blue_button my_setting_account_change_account_no_active" onclick="accounts.change_vk(1);"><div class="blue_button">Отвязать страницу</div></div>');
                }
            });
        }
    });
}

function search_mini_info() {
    $('#input_tasks_search').on('input', function() {
        var search_url = _val('#input_tasks_search') ? $('#input_tasks_search').val() : '';
        if (!search_url) {
            $('#task_info_url').hide();
            $('#task_info_url').show().html('<div class="task_mini_info"><div class="task_mini_info_avatar"><img src="/images/error_search.png"></div><div class="task_mini_info_title">Ничего не найдено</div><div class="task_mini_info_desc">По вашему запросу не найдено ни одного сотрудника.</div></div>');
            return;
        }
        $('#task_info_url').hide();
        $('#task_add_url_loader').show();
        $.getJSON('/admin/modules/users/search.php', {
            search: search_url
        }, function(data) {
            var response = data;
            var messages = response.messages;
            $('#task_add_url_loader').hide();
            if (messages == null) {
                $('#task_info_url').show().html('<div class="task_mini_info"><div class="task_mini_info_avatar"><img src="/images/error_search.png"></div><div class="task_mini_info_title">Ничего не найдено</div><div class="task_mini_info_desc">По вашему запросу не найдено ни одного сотрудника.</div></div>');
            } else {
                $('#task_info_url').show().html(' ' + messages + ' ');
            }
        });
    });
}
var accounts = {
    change_vk: function(uid) {
        $('#my_setting_account_change_account_no_active').find('.blue_button').html('<div class="upload"></div>');
        $('#my_settings_account_new_vk_error').hide().html('');
        $.post('/f.vk.add', {
            uid: uid,
            type: 1
        }, function(data) {
            var result = $.parseJSON(data);
            var error_text = result.error_text;
            $('#my_setting_account_change_account_no_active').find('.blue_button').html('Отвязать страницу');
            if (error_text) {
                $('#my_settings_account_new_vk_error').show().html('<div class="error_msgm">' + error_text + '</div>');
            } else if (result.success == 1) {
                $('.field_settings_vk').html('не прикреплена');
                $('#button').html('<div id="uLoginb4af8ba2" data-ulogin="display=buttons;fields=first_name,last_name;redirect_uri=;callback=addvk"><div id="settings_steam_button" class="blue_button_wrap small_blue_button" data-uloginbutton = "vkontakte"><div class="blue_button">Привязать страницу</div></div></div>');
            }
        });
    }
}
var convoys = {
    _add: function(post_status) {
        var udate = _val('#udate') ? $('#udate').val() : '';
        var usbor = _val('#usbor') ? $('#usbor').val() : '';
        var usbor2 = _val('#usbor2') ? $('#usbor2').val() : '';
        var ustart = _val('#ustart') ? $('#ustart').val() : '';
        var uoldstop = _val('#uoldstop') ? $('#uoldstop').val() : '';
        var ustop = _val('#ustop') ? $('#ustop').val() : '';
        var ulong = _val('#ulong') ? $('#ulong').val() : '';
        var status = $('#select_convoys_add_value').val();
        var uconvoys_field_text = _val('#uconvoys_field_text') ? $('#uconvoys_field_text').val() : '';
        var udate_post = $('#udate_post').val();
        $('#support_images_attach_img_field_ids').val('');
        $('.support_images_attach_img_field_id').each(function() {
            var id = $(this).val() ? $(this).val() + ',' : '';
            var values = $('#support_images_attach_img_field_ids').val();
            $('#support_images_attach_img_field_ids').val(values + '' + id);
        });
        if(udate.length < 1) {
            $('#udate').focus();
            return;
        } else if(usbor.length < 1) {
            $('#usbor').focus();
            return;
        } else if(usbor2.length < 1) {
            $('#usbor2').focus();
            return;
        } else if(ustart.length < 1) {
            $('#ustart').focus();
            return;
        } else if(uoldstop.length < 1) {
            $('#uoldstop').focus();
            return;
        } else if(ustop.length < 1) {
            $('#ustop').focus();
            return;
        } else if(ulong.length < 1) {
            $('#ulong').focus();
            return;
        }

        $('#task_add_error').hide();
        $('#add_task_button .blue_button').html('<div class="upload"></div>');
        $.post('/convoys/add.post', {
            post_status: post_status,
            udate_post: udate_post,
            udate: udate,
            usbor: usbor,
            usbor2: usbor2,
            ustart: ustart,
            uoldstop: uoldstop,
            ustop: ustop,
            ulong: ulong,
            uconvoys_field_text: uconvoys_field_text,
            status: status,
            photo_attaches: $('#support_images_attach_img_field_ids').val()
        }, function(data) {
            var result = $.parseJSON(data);
            var error_msg = result.error_text;
            $('#add_task_button .blue_button').html('Добавить конвой');
            if (error_msg) {
                $('#task_add_error').show().html(error_msg);
            } else if(result.success == 1) {
                _wndBottom._show('Конвои компании', 'Поздравляем вы добавили новый конвой.', {
                    img: '/images/wnd_bottom/send_tasks.png?1'
                });
            } else {
                $('#task_add_error').show().html('Неизвестная ошибка');
            }
        });
    },
    _save_convoys: function(id) {
        var udate = $('#udate').val();
        var usbor = _val('#usbor') ? $('#usbor').val() : '';
        var usbor2 = _val('#usbor2') ? $('#usbor2').val() : '';
        var ustart = _val('#ustart') ? $('#ustart').val() : '';
        var uoldstop = _val('#uoldstop') ? $('#uoldstop').val() : '';
        var ustop = _val('#ustop') ? $('#ustop').val() : '';
        var ulong = _val('#ulong') ? $('#ulong').val() : '';
        var status = $('#select_convoys_add_value').val();
        var desc = _val('#uconvoys_field_text') ? $('#uconvoys_field_text').val() : '';
        var udate_post = _val('#udate_post') ? $('#udate_post').val() : 0;

        $("#task_add_error").hide();
        $('#add_task_button .blue_button').html('<div class="upload"></div>');
        $.post('/convoys/edit.convoys', {
            id: id,
            udate: udate,
            usbor: usbor,
            usbor2: usbor2,
            ustart: ustart,
            uoldstop: uoldstop,
            ustop: ustop,
            ulong: ulong,
            status: status,
            desc: desc,
            udate_post: udate_post
        }, function(data) {
            var response = $.parseJSON(data);
            var error_msg = response.error_msg;
            $('#add_task_button .blue_button').html('Сохранить изменения');
            if (error_msg == 'login') {
                nav.go('', '/');
                return;
            }
            if (response.success == 1) {
                $('#task_add_error').show().html('Изменения сохранены.');
            } else if(error_msg) {
                $('#task_add_error').show().html(error_msg);
            } else {
                $('#task_add_error').show().html('Неизвестная ошибка.');
            }
        });
    },
    _next: function() {
        var page_num_id = $('#next_page_small_d');
        var page_num = page_num_id.text();
        $('#convoys_load_next').show().html('<div class="upload"></div>');
        $.get('/convoys/convoys.next', {
            page: page_num
        }, function(data) {
            var response = data;
            $('#convoys_load_next').html('Показать прошедшие мероприятия »');
            if (response) {
                $('#next_page_small_c').append(response);
                page_num_id.text(page_num_id.text() * 1 + 1);
            } else {
                $('#convoys_load_next').hide();
            }
        });
    },
    _next_history: function() {
        var page_num_id = $('#next_page_small_d');
        var page_num = page_num_id.text();
        $('#convoys_load_next').show().html('<div class="upload"></div>');
        $.get('/convoys/history.next', {
            page: page_num
        }, function(data) {
            var response = data;
            $('#convoys_load_next').html('Показать еще мероприятия »');
            if (response) {
                $('#next_page_small_c').append(response);
                page_num_id.text(page_num_id.text() * 1 + 1);
            } else {
                $('#convoys_load_next').hide();
            }
        });
    },
    _delete: function(id) {
        var text = 'Вы действительно хотите удалить мероприятие? Это действие нельзя будет отменить.';
        _wndBlue._show(430, {
            title: 'Удаление мероприятия',
            text: '<div id="task_del_box">' + text + '</div>',
            _wsend: 80,
            _tsend: 'Удалить',
            _send: function() {
                $('#box_button_blue .blue_button').html('<div class="upload"></div>');
                $.getJSON('/convoys/delete.convoys', {
                    id: id
                }, function(data) {
                    var response = data;
                    var error_text = response.error_text;
                    $('#box_button_blue .blue_button').html('Удалить');
                    _wndBlue._closed();
                    if (error_text == 'login') {
                        nav.go('', '/');
                        return;
                    }
                    if (error_text) {
                        $('#task_add_error').hide();
                        $('#task_add_error').show().html(error_text);
                        _wndBlue._closed();
                    } else if (response.success) {
                        if (response.id > 0) {
                            $('#convoysids' + id).hide();
                        }
                    } else {
                        $('#convoysids' + id).hide();
                        $('#task_add_error').show().html('Неизвестная ошибка.');
                    }
                });
            }
        });
    },
    _info_left: function() {
        nav.go('', '/convoys');
    },
    _loading_add: function() {
        nav.go('', '/convoys/add');
    },
    _loading_info: function(id) {
        nav.go('', '/convoys' + id);
    },
    upload_img: function(type) {
        if ($('#support_images_attach').find('img').size() >= 2) {
            _wndBlue._show(410, {
                title: 'Ошибка',
                text: '<div id="task_del_box">Вы можете прикрепить не более 2-х скриншотов.</div>'
            });
            return false;
        }
        var file_split = $('#support_add_img_file').val().split('\\');
        var filename = file_split[file_split.length - 1];
        $('#support_upload_iframe').contents().text('');
        $('#support_upload_iframe_submit').trigger('click');
        if (type == 'comment') {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow progress_grad_support_overflow_comment" style="margin-top: 0px;margin-left: 160px;">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        } else {
            $('#support_grad_progress').show().html('\
    <div class="progress_grad_support_overflow" style="margin-top: 0px;margin-left: 160px;">\
    <div class="progress_grad"></div><div class="progress_grad_support_overflow_left">' + filename + '</div>\
    </div>\
   ');
        }
        var iframe_content_interval = setInterval(function() {
            var iframe_content = $('#support_upload_iframe').contents().text();
            if (iframe_content) {
                $('#support_grad_progress').hide();
                var iframe_content_json = $.parseJSON(iframe_content);
                var iframe_content_json_error = iframe_content_json.error_text;
                var iframe_content_json_big_img = iframe_content_json.result_big_file;
                var iframe_content_json_mini_img = iframe_content_json.result_mini_file;
                var iframe_content_json_id = iframe_content_json.id;
                if (iframe_content_json.error_text == 'login') {
                    nav.go('', '/');
                }
                if (iframe_content_json.error_text) {
                    nav.error_head(iframe_content_json.error_text);
                } else if (iframe_content_json.success == 1) {
                    $('#convoys_images_attach').show().append('\
     <span id="support_images_attach_img_wrap_id' + iframe_content_json_id + '">\
      <input type="hidden" value="' + iframe_content_json_id + '" class="support_images_attach_img_field_id"> \
      <div class="support_images_attach_img_wrap">\
       <div class="support_images_attach_img_inner">\
        <div onclick="support.del_img(' + iframe_content_json_id + ')" class="support_images_attach_img_inner_bdel"><div class="support_images_attach_img_inner_bdel_closed"></div></div>\
       </div>\
       <a href="/images/uploads/convoys/' + iframe_content_json_big_img + '" target="_blank"><img src="/images/uploads/convoys/' + iframe_content_json_mini_img + '"></a>\
     <input type="hidden" value="' + iframe_content_json_big_img + '" id="images_attach_img_field_url">\
      </div>\
     </div>\
     ');
                }
                clearInterval(iframe_content_interval);
            }
        }, 10);
    },
    del_img: function(id) {
        $('#support_images_attach_img_wrap_id' + id).remove();
    }
}
var select = {
    create: function(id, params, data, onClickFunction) {
        var select_id = 'select_' + id;
        var select_id_content = 'select_content_' + select_id;
        var select_id_show = select_id + '_show';
        var select_id_hover = select_id + '_hover';
        var active_value = data[data.length - 1];
        template_content = '';
        var template = '\
   <div id="' + select_id + '" class="select">\
    <div class="select_title">\
     <div class="select_title_inner">\
      <div class="select_title_left"></div>\
      <div class="select_title_right">\
       <div class="fl_r select_arrow"></div>\
      </div>\
     </div>\
    </div>\
    <div id="' + select_id_content + '" class="select_content">\
     <div class="select_content_inner">\
      \
     </div>\
    </div>\
    \
    <input type="hidden" id="' + select_id + '_show" value="0">\
    <input type="hidden" id="' + select_id + '_hover" value="1">\
    <input type="hidden"' + ((params.input_name) ? ' name="' + params.input_name + '"' : '') + ' id="' + select_id + '_value" value="' + active_value + '">\
   </div>\
  ';
        $('#' + id).html(template);
        if (!active_value.length) {
            for (i = 0; i < data.length; i++) {
                var value = data[i][0];
                if (value == active_value) {
                    $('#' + select_id).find('.select_title_left').text(data[i][1]);
                }
            }
        } else {
            $('#' + select_id).find('.select_title_left').text(data[0][1]);
            $('#' + select_id + '_value').val(data[0][0]);
        }
        $('#' + select_id + ' .select_title, #' + select_id + ' .select_content').css({
            width: params.width
        });
        $('#' + select_id).find('.select_title').click(function() {
            if (!parseInt($('#' + select_id_show).val())) {
                $('#' + select_id_show).val(1);
                $('#' + select_id_hover).val(1);
                $(this).addClass('active');
                $('#' + select_id).find('.select_content').show();
                template_content = '';
                for (i = 0; i < data.length; i++) {
                    var value = data[i][0];
                    var title = data[i][1];
                    if (title) {
                        template_content += '\
       <a' + ((value == select.getActiveValue(id)) ? ' class="active"' : '') + ' href="javascript://" onclick="select.setActiveValue(this, \'' + id + '\', ' + value + ');">' + title + '</a>\
      ';
                    }
                }
                $('#' + select_id).find('.select_content_inner').html(template_content);
                $('#' + select_id).find('.select_content_inner a').click(function() {
                    ((onClickFunction) ? onClickFunction() : '');
                });
                $('#' + select_id).find('.select_content_inner a').hover(function() {
                    $('#' + select_id).find('.select_content_inner a').removeClass('active');
                });
                $('#' + select_id).hover(function() {
                    $('#' + select_id_hover).val(1);
                }, function() {
                    $('#' + select_id_hover).val(0);
                });
                $('body').click(function() {
                    if (!parseInt($('#' + select_id_hover).val())) {
                        select.close(id);
                    }
                });
            } else {
                select.close(id);
            }
        });
    },
    getActiveValue: function(id) {
        return $('#select_' + id + '_value').val();
    },
    setActiveValue: function(a, id, value) {
        $('#select_' + id).find('.select_title_left').text($(a).text());
        $('#select_' + id + '_value').val(value);
        select.close(id);
    },
    close: function(id) {
        $('#select_' + id + '_show').val(0);
        $('#select_' + id).find('.select_title').removeClass('active');
        $('#select_' + id).find('.select_content').hide();
    }
}

function ageList(start, end, active, title) {
    var data = new Array();
    data.push([0, title]);
    for (i = start; i <= end; i++) {
        data.push([i, i]);
    }
    if (active) {
        data.push(active);
    }
    return data;
}

function myrand(c_min, c_max) {
    return Math.round(Math.random() * (c_max - c_min) + c_min);
}

function setCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    setCookie(name, "", -1);
}
var captcha_box = {
    _show: function(fn) {
        var _title = 'Введите код с картинки';
        var _rand = myrand(1111111111, 999999999);
        var template = '\
   <div id="captcha_box">\
    <div class="img">\
     <img src="/secure?captcha_key=' + _rand + '">\
    </div>\
    <div class="field">\
     <input iplaceholder="Введите код сюда" value="" maxlength="6" type="text" id="captcha_text">\
    </div>\
   </div>\
  ';
        _wndBlue._show(350, {
            title: _title,
            text: template,
            onContent: function() {
                _placeholder('#captcha_text');
                $('#captcha_key').val(_rand);
                $('#captcha_box').find('img').click(function() {
                    captcha_box._reload();
                });
                // отправка по Enter
                $('#captcha_text').keydown(function(event) {
                    var keyCode = event.which;
                    if (keyCode == 13) {
                        $('#box_button_blue .blue_button').html('<div class="upload"></div>');
                        $('#captcha_code').val($('#captcha_text').val());
                        fn();
                    }
                });
                $('#box_button_blue').click(function() {
                    $('#captcha_code').val($('#captcha_text').val());
                    $('#box_button_blue .blue_button').html('<div class="upload"></div>');
                });
            },
            _wsend: 95,
            _tsend: 'Отправить',
            _send: fn
        });
    },
    _reload: function() {
        var rand = myrand(1111111111, 999999999);
        $('#captcha_key').val(rand);
        $('#captcha_box img').attr('src', '/secure?captcha_key=' + rand);
    }
}
var page_name = iPopular.page_name;
var section = iPopular.section;

function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}
var ajax_nav = '';
var nav = {
    go: function(a, b, ufn, back) {
        var url = b ? b : $(a).attr('href');
        loader.showHead(20);
        $.get(url, function(data) {
            loader.showHead(100);
            var ajax_nav = 1;
            var response = data;
            var title_page = response.match(/<title>(.*?)<\/title>/i);
            var content_page = response.match(/<div id="page">([\s\S]*)<\/div>/i);
            var content_script = response.match(/<script id="mainscripts" type="text\/javascript">([\s\S]*)<\/script>/i);
            var content_script_result = content_script ? content_script[1] : '';
            if (content_page) {
                ufn ? ufn() : '';
                // закрываем боксы
                _wndWhite._closed();
                _wndBlue._closed();
                nav.loader(0);
                // изменяем title
                document.title = title_page[1].toString();
                // загружаем содержимое
                $('#page').html(content_page[1].toString());
                // live
                $('#live_counter').html("<a href='http://www.liveinternet.ru/click' " + "target=_blank><img src='//counter.yadro.ru/hit?t44.6;r" + escape(document.referrer) + ((typeof(screen) == "undefined") ? "" : ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) + ";" + Math.random() + "' alt='' title='LiveInternet' " + "border='0' width='31' height='31'><\/a>");
                if (content_script_result) {
                    eval(content_script_result);
                }

                function _placeholder(a) {
                    var _placeholder = $(a).attr('iplaceholder');
                    var _value = $(a).val();
                    if (!_value) {
                        setTimeout(function() {
                            $(a).val(_placeholder)
                        }, 0);
                        $(a).addClass('placeholder');
                    }
                    $(a).focus(function() {
                        $(a).removeClass('placeholder');
                        if ($(this).val() == _placeholder) $(this).val('');
                    });
                    $(a).blur(function() {
                        if (!$(this).val()) {
                            $(this).val(_placeholder);
                            $(a).addClass('placeholder');
                        }
                    });
                }

                function _val(a) {
                    return $(a).attr('class').indexOf('placeholder') > -1 ? 0 : 1;
                }
                // меняем url
                if (!back) {
                    window.history.ready = true;
                    history.pushState ? history.pushState({}, '', url) : location.hash = url;
                }
                if ($('#hpush_url_nav_val').html()) {
                    $('#hpush_url_nav_val').html(url);
                } else {
                    $('body').append('<div style="display: none" id="hpush_url_nav_val">' + url + '</div>');
                }
                setTimeout(function() {
                    loader.hideHead();
                }, 400);
            } else {
                nav.error_head('Сервис временно недоступен. Попробуйте позже.');
                loader.hideHead();
            }
        });
    },
    loader: function(a) {
        a ? $('#loading').show().css({
            position: 'fixed',
            top: ($(window).height() / 2 - 64),
            left: ($(window).width() - 64) / 2
        }) : $('#loading').hide();
    },
    loader_page: function() {
        $('#head_loader').css('opacity', 1);
    },
    loader_page_hide: function() {
        $('#head_loader').css('opacity', 0);
    },
    error_head: function(text) {
        $('#error_head').fadeIn(400).html(text);
        setTimeout(function() {
            $('#error_head').fadeOut(400);
        }, 3500);
    }
}
var loader = {
    showHead: function(percent, next) {
        $('#header_load').show().animate({
            width: percent + '%'
        }, 150);
    },
    hideHead: function() {
        $('#header_load').css('width', 0).hide();
    }
}
initPushStream = function() {
    return new PushStream({
        host: ('queue.' + window.location.hostname).replace('dev.', ''),
        useSSL: window.location.protocol.indexOf('s') != -1,
        modes: 'longpolling',
        urlPrefixLongpolling: '/im2',
        jsonTextKey: 'message',
        jsonTimeKey: 'ts',
        messagesControlByArgument: true,
        tagArgument: 'tag',
        timeArgument: 'ts',
    });
}

function timeAgoN(ts) {
    var c = getTimestamp() - parseInt(ts);
    var a = [
        ['секунду', 'секунды', 'секунд'],
        ['минуту', 'минуты', 'минут'],
        ['час', 'часа', 'часов'],
    ];
    if (c < 0) {
        return '0 секунд';
    } else if (c < 60) {
        return c + ' ' + declOfNum(c, a[0]);
    } else if (c < 3600) {
        c = parseInt(c / 60);
        return c + ' ' + declOfNum(c, a[1]);
    }
    c = parseInt(c / 3600);
    return c + ' ' + declOfNum(c, a[2]);
}

function getTimestamp() {
    return parseInt(Date.now() / 1000) + tz;
}
var radiobtn = {
    _new: function(id, obj) {
        var template = '\
   <div class="radiobtn_o">\
    <div class="radiobtn radiobtn_no"></div>\
    <div class="radiobtn_t">' + obj.title + '</div>\
   </div>\
  ';
        $('#' + id).html(template);
        $('.radiobtn_o').click(function() {
            $('.radiobtn_o').find('.radiobtn').removeClass('radiobtn_active').addClass('radiobtn_no');
            $(this).find('.radiobtn').removeClass('radiobtn_no').addClass('radiobtn_active');
        });
    },
    _check: function() {}
}
var minSelect = {
    _new: function(id, width, title, text) {
        var template = '\
  <div class="minselect_wrap">\
   <a id="aminselect_' + id + '" href="javascript://"><div class="actions_menu_icon"></div></a>\
   <div style="width: ' + width + 'px" id="minselect_' + id + '" class="minselect">\
    <div class="minselect_content">' + text + '</div>\
   </div>\
  </div>\
  ';
        $('#' + id).html(template);
        $('#aminselect_' + id + ', #minselect_' + id).click(function() {
            $('#minselect_' + id).toggle();
        });
        $('#minselect_' + id).hover(function() {}, function() {
            $('#minselect_' + id).fadeOut(200);
        });
        $('#minselect_' + id).find('.mnav').click(function() {
            setTimeout(function() {
                $('#minselect_' + id).hide();
            }, 10);
        });
    }
}

function isname(name) {
    return name.match(/^[а-яё]+$/i);
}

function isLogin(login) {
    return login.match(/^([@a-zA-Z0-9\-\.\_\*]){2,30}$/gi);
}

function isPassword(password) {
    return password.match(/^([@a-zA-Z0-9\-\.\_\*]){6,32}$/gi);
}

function isEmail(email) {
    return email.match(/^[a-zA-Z0-9_\.\-]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,6}$/i);
}

function isValueExst(a) {
    return (a.attr('iplaceholder') == a.val()) ? '' : a.val();
}

function createTooltips(a) {
    $('[class^="tooltip_users_info"]').hover(function() {
        var b = $(this).attr("class").toString().match(/tooltip\_users\_info(\d+)/i);
        $("#script_click" + b[1]).trigger("click");
        $("#tooltip_users_info").css({
            position: "absolute",
            width: 280,
            top: $("#users_id" + b[1]).find(".result_amount").offset().top - ($(".tooltip_msg").height() / 2) + ($("#users_id" + b[1]).find(".result_amount").height() / 2),
            left: $("#users_id" + b[1]).find(".tooltip_users_info_result" + b[1]).offset().left - $(".tooltip_wrap").width() - 22
        });
        $("#users_id" + b[1]).find(".admin_users_icon .admin_users_icons.info").css({
            opacity: 1
        })
    }, function() {
        $('.tooltip_msg').hover(function() {}, function() {
            $('div[id^="tooltip_"]').html("").find(".tooltip_wrap").hide();
        });
    });
}
var tooltip = {
    create: function(c, f, e) {
        var b = $(c).width();
        if (e.position == "left") {
            $("#tooltip_" + f).css({
                marginLeft: b
            }).html('<div style="' + ((e.marginTop) ? "margin-top: " + e.marginTop + "px;" : "") + '" class="tooltip_wrap">     <div class="clearfix">      <div class="tooltip_msg">' + e.msg + '</div>      <div class="tooltip_right_arrow_right">       <div class="tooltip_right_arrow"></div>      </div>     </div>    </div>   ')
        } else {
            if (e.position == "top") {
                $("#tooltip_" + f).css({
                    marginLeft: b
                }).html('<div style="' + ((e.marginTop) ? "margin-top: " + e.marginTop + "px;" : "") + '" class="tooltip_wrap">     <div class="clearfix">      <div class="tooltip_msg_top">' + e.msg + '</div>      <div class="tooltip_top_arrow_bottom">       <div class="tooltip_top_arrow"></div>      </div>     </div>    </div>   ')
            } else {
                $("#tooltip_" + f).css({
                    marginLeft: b
                }).html('<div style="' + ((e.marginTop) ? "margin-top: " + e.marginTop + "px;" : "") + '" class="tooltip_wrap">     <div class="clearfix">      <div class="tooltip_left_arrow_left">       <div class="tooltip_left_arrow"></div>      </div>      <div class="tooltip_msg">' + e.msg + "</div>     </div>    </div>   ")
            }
        }
        $("#tooltip_" + f).find(".tooltip_wrap").stop().fadeIn(150);
        if (e.marginLeft) {
            $("#tooltip_" + f).css({
                marginLeft: e.marginLeft
            })
        }
        var d = $(".tooltip_msg").height();
        $(".tooltip_left_arrow, .tooltip_right_arrow").css({
            marginTop: (d / 2)
        });
        $(".tooltip_top_arrow").css({
            marginLeft: ($(".tooltip_msg_top").width() / 2)
        })
    }
}

function bbCodesTooltips(a) {
    return a.replace(/\[br\]/gi, "<br />").replace(/\[b\](.*?)\[\/b\]/gi, "<b>$1</b>").replace(/\[div class=\"(.*?)\"\](.*?)\[\/div\]/gi, '<div class="$1">$2</div>')
}