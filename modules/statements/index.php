<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'statements';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
include($root.'/inc/system/profile.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');
require($root.'/inc/classes/convoys.php');
require($root.'/inc/classes/users.php');

if($uban_type) {
 header('Location: /blocked');
 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Мои заявления</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
<div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>
   <div id="content">
       <? include($root.'/include/left.php') ?>
    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
      <div class="info-header-block convoys_img">
         <h4>Мои заявления</h4>
         <p>Вы можете написать заявления на отпуск, на смену ника, на увольнение, или же записаться на экзамен.</p>
         <p>Вы можете взять отпуск минимум на один месяц. После увольнения ваш аккаунт будет заблокирован</p>
         <p>Ваше заявления будет рассмотрено в течение <b>12 часов</b>.</p>
         <div class="info-bottom" onclick="users._support_bottem();">
          Если у вас возникли какие-то вопросы то напишите в поддержку »
         </div>
      </div>

      <ul class="statements_select" id="statements_add_select">
          <li>
              <div class="block vk">
                  <div class="icon socials vk"><i></i></div>
                  <div class="name">Заявление на отпуск</div>
                  <div class="desc">Вы сможете взять отпуск на определенное время</div>
              </div>
          </li>
          <li>
              <div class="block vk">
                  <div class="icon socials exam"><i></i></div>
                  <div class="name">Записаться на экзамен</div>
                  <div class="desc">Вы сможете отправить заявку в автошколу что-бы записатся на экзамен</div>
              </div>
          </li>
          <li>
              <div class="block vk">
                  <div class="icon socials nickname"><i></i></div>
                  <div class="name">Заявление на смену никнейма</div>
                  <div class="desc">Вы сможете отправить заявку на смену никнейма</div>
              </div>
          </li>
          <li>
              <div class="block vk">
                  <div class="icon socials exit"><i></i></div>
                  <div class="name">Заявление на увольнение</div>
                  <div class="desc">Вы сможете отправить заявку на увольнение</div>
              </div>
          </li>
      </ul>
       </div>
      </div>
     </div>
     <input type="hidden" id="task_wnd_open_val">
     <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
 </div>
</div>
<? include($root.'/include/scripts.php'); ?>
 </body>
</html>
