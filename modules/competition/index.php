<?php
$root = $_SERVER['DOCUMENT_ROOT'];

$page_name = 'employee';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
require($root.'/inc/system/profile.php');
require($root.'/inc/classes/employee.php');

if(!$user_logged) {
 // если пользователь неавторизован, перенаправляем на главную
 header('Location: /');
 exit;
}

$section = $_GET['section'];

$search = $_GET['q'];

if($section == 'online') {
 $section = $section;
} else {
 $section = '';
}

if($section == 'online') {
  $users_limit = 50;
} else {
  $users_limit = 12;
}

if($section == 'online') {
 $section_name = 'Сейчас на сайте';
} else {
 $section_name = 'Все сотрудники';
}

$newusers = $top->users_list_num();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Сотрудники</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
	  <div class="main nopad">

      <div class="tasks_tabs" style="margin-bottom:0px;">
          <div class="ft_search">
          <input id="input_tasks_search" value="<? echo fxss($search); ?>" iplaceholder="Например: Николай Белый или никнейм сотрудника" type="text">
             <div id="task_add_url_loader"><div class="upload"></div></div>
             <div id="task_info_url"></div>
           </div>
        <a class="tab <? if(!$section) echo 'active'; ?>" href="/users" onclick="nav.go(this); return false;"><div class="tabdiv">Все сотрудники</div></a>
        <a class="tab <? if($section == 'online') echo 'active'; ?>" href="/users?section=online" onclick="nav.go(this); return false;"><div class="tabdiv">Сейчас на сайте</div></a>
        <div class="ft_users_search"></div>
     </div>

     <div class="ft_users_search_panel" style="display: none;">
             <input id="input_tasks_search" value="<? echo fxss($search); ?>" iplaceholder="Например: Николай Белый или никнейм сотрудника" type="text">
             <div id="task_add_url_loader"><div class="upload"></div></div>
             <div id="task_info_url"></div>
     </div>

      <div class="clearfix">
       <div class="flat_users">
           <div id="head-info-block"><span><? if(!$newusers) { ?> <? } else { ?><? echo declOfNum($newusers, array('Найден', 'Найдено', 'Найдено')); ?> <? echo $newusers; ?> <? echo declOfNum($newusers, array('сотрудник', 'сотрудника', 'сотрудников')); ?><? } ?></span></div>
           <div class="list_users">
               <? echo $top->users_list(); ?>
               <div id="next_page_small_c"></div>
           </div>
       </div>
       </div>

       <? if($newusers > $users_limit) { ?><div id="users_load_next" class="load_next" style="margin-bottom: 0px;border-radius: 0px 0px 4px 4px;margin-top: -3px;border-top: 1px solid #e7e8ec;" onclick="users._next();">Показать ещё сотрудников »</div><? } ?>
       <div id="next_page_small_d">1</div>

      </div>
     </div>
    </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
