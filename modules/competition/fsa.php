<?php
$root = $_SERVER['DOCUMENT_ROOT'];

$page_name = 'competition';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
require($root.'/inc/system/profile.php');
require($root.'/inc/classes/profile.php');
require($root.'/inc/classes/tasks_blacklist.php');

if(!$user_logged) {
 // если пользователь неавторизован, перенаправляем на главную
 header('Location: /');
 exit;
}

$act = $db->escape($_GET['act']);

if($act == 'refs') {
 $act = $act;
} else {
 $act = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Конкурс Russian Express</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>   
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">


	  <div class="main nopad">


        <div id="search_bg_hr"></div>
        <div id="tasks_bar_wrap">
         <div id="tasks_bar_wrap_left">
          <div id="tasks_bar">Конкурс</div>
         </div>
        </div>
        <div id="competition_l">
		<div class="competition_stats_desc">Благодорим <b>Вас</b> за участие в <b>конкурсе</b>.</div>
         <div class="user_stats_overflow">
          <div class="user_stats_left">
           <div class="user_stats_left_block user_stats_all_tasks_num">35</div>
          </div>
          <div class="user_stats_right user_stats_all_tasks_num_color">человек приняла участие.</div>
         </div>

		 <h1>Сроки</h1>
		 <ul style="padding-bottom: 0;">
            <li>Начало конкурса: <span id="since">10 июня 2017 в 19:00</span></li>
            <li>Окончание конкурса: <span id="since">10 июня 2017 в 23:00</span></li>
            <li>Количество призов:</li>
			<div id="since_r">
		    <li>1) Скандинавия</li>
			<li>2) ДЛС Франция</li>
			<li>3) ДЛС Польша</li>
			</div>
         </ul>

		 <h1>Финальные результаты</h1>



		<div class="wk_hider_box">
		<div class="wk_hider_header">
		<div class="wk_hider_title">Победитель <div class="wk_hider_title_r">Приз</div></div>
		</div>
		<div id="wk_table" class="wk_hider_body">

		<table>
		<tbody>
		 <tr>
		 <td>
		  <span><b>1.</b></span>
		  </td>
		  <td>
		  <b>Pavel Durov | Николай</b>
         </td><td>
		  <b><span id="wk_hider_r">Скандинавия</span></b>
         </td></tr>
		  <tr>
		 <td>
		  <span><b>2.</b></span>
		  </td>
		  <td>
		  <b>#Meow(Ваня)</b>
         </td><td>
		  <b><span id="wk_hider_r">ДЛС Франция</span></b>
         </td></tr>
		  <tr>
		 <td>
		  <span><b>3.</b></span>
		  </td>
		  <td>
		  <b>Cloud Wolf(Юра)</b>
         </td><td>
		  <b><span id="wk_hider_r">ДЛС Польша</span></b>
         </td></tr>
		  <tr>
		 <td>
		  <span><b>4.</b></span>
		  </td>
		  <td>
		  <b>HoTaByChh|Костя</b>
         </td><td>
		  <b><span id="wk_hider_r">+4 балла</span></b>
         </td></tr>
		  <tr>
		 <td>
		  <span><b>5.</b></span>
		  </td>
		  <td>
		  <b>smail702(МАКСИМ)</b>
         </td><td>
		  <b><span id="wk_hider_r">+2 балла</span></b>
         </td></tr>
		  <tr>
		 <td>
		  <span><b>6.</b></span>
		  </td>
		  <td>
		  <b>Саня Moskow</b>
         </td><td>
		  <b><span id="wk_hider_r">без приза.</span></b>
         </td></tr>
		  <tr>
		 <td>
		  <span><b>8.</b></span>
		  </td>
		  <td>
		  <b>ЕВГЕН 71РУС|ЖЕНЯ</b>
         </td><td>
		  <b><span id="wk_hider_r">без приза.</span></b>
         </td></tr>
		   <tr>
		 <td>
		  <span><b>9.</b></span>
		  </td>
		  <td>
		  <b>Zentorno(Павел)</b>
         </td><td>
		  <b><span id="wk_hider_r">без приза.</span></b>
         </td></tr>
		   <tr>
		 <td>
		  <span><b>10.</b></span>
		  </td>
		  <td>
		  <b>ADANDZO|Игорь</b>
         </td><td>
		  <b><span id="wk_hider_r">без приза.</span></b>
         </td></tr>
          </tbody>
		</table>


		 </div></div>






        </div>
       </div>



   </div>
  </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
