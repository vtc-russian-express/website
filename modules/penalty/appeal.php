<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'penalty_r';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/penalty.php');

$list_section = $_GET['section'];
$list_section_t = ($list_section == 'deleted' || $list_section == 'new' || $list_section == 'considered') ? 1 : 0;

if($ugroup != 4) {
 header('Location: /tasks');

 exit;
}

if($list_section == 'considered') {
    $section_title = 'Список рассмотренных заявок';
} elseif($list_section == 'deleted') {
    $section_title = 'Список отклоненных заявок';
} else {
    $section_title = 'Список заявок';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
  <title>Заявки на апелляцию штрафов</title>
  <? include($root.'/include/head.php') ?>

</head>
<body>
 <div id="header_load"></div>  
 <div id="page">
  <? include($root.'/include/header.php') ?>

  <div id="content">
    <? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
         <div class="tasks_tabs">
             <a class="tab <? if(!$list_section_t) echo ' active'; ?>" href="/penalty/appeal" onclick="nav.go(this); return false;"><div class="tabdiv">Все заявки</div></a>
             <a class="tab <? if($list_section == 'considered') echo ' active'; ?>" href="/penalty/appeal/?section=considered" onclick="nav.go(this); return false;"><div class="tabdiv">Расмотренные</div></a>
             <a class="tab <? if($list_section == 'deleted') echo ' active'; ?>" href="/penalty/appeal/?section=deleted" onclick="nav.go(this); return false;"><div class="tabdiv">Отклонненые</div></a>
         </div>
           <div class="list_tasks">
               <div id="head-info-block"><span><? echo $section_title; ?></span></div>
               <? echo $urgent->admin_requests_appeal(); ?>
           </div>
       </div>
     </div>
   </div>
   <? include($root.'/include/footer.php') ?>

 </div>
</div>
</div>
<? include($root.'/include/scripts.php') ?>
</body>
</html>
