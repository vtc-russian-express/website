<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'penalty';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/penalty.php');

$urgent_requests = $urgent->user_requests();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Погашение штрафных баллов</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
        <div class="penalty_content_send">
         <h4>Погашения штрафных баллов</h4>
          <p>Данная функция позволит вам отправить заявку на погашения <b>штрафных баллов</b>.</p>
            <p>Внимательно почитайте <a href="/page/rules_f" onclick="nav.go(this); return false;">правила</a> перед отправкой!.</p>
			<p>Если ваша заявка отклонина обратитесь в <a href="/support/new" class="agold" onclick="nav.go(this); return false;">поддержку</a>.</p>
		 </div>
		 <div id="express_content_send2">
         <div id="exchange_form_content">
          <div id="exchange_form_content_inner">
           <div class="exchange_form_field_title">Ваш никнейм:</div>
           <div class="exchange_form_field_input">
            <input type="text" id="exchange_count_nik" value="<? if($users_nik) {echo $users_nik;} else {echo 'не указан';} ?>" disabled="true" />
           </div>
           <div class="exchange_form_field_title">Подтверждающий скриншот (с <b style="color:#EB6361;">ТАБОМ</b>):</div>
	        <div id="support_images_attach"></div>
            <input type="hidden" id="support_images_attach_img_field_ids">
            <div id="support_grad_progress"></div>
             <div id="support_add_content_buttons">
              <div id="express_content_send_buttons_right">
              <a href="javascript://" id="test_images">
              Прикрепить изображение
               <iframe id="support_upload_iframe" name="support_upload_iframe"></iframe>
               <form method="post" enctype="multipart/form-data" action="/penalty/img.upload" target="support_upload_iframe">
               <input id="support_add_img_file" onchange="penalty.upload_img(); return false;" type="file" name="file">
               <input id="support_upload_iframe_submit" style="display: none;" type="submit">
               </form>
             </a>
            </div>
           </div>
		   <div class="exchange_form_field_title">Сколько штрафных баллов:</div>
           <div class="exchange_form_field_input">
            <input type="text" id="exchange_points_voutes" />
          </div>
		  <div class="exchange_form_field_title">Ваш пробег в км:</div>
           <div class="exchange_form_field_input">
            <input type="text" id="exchange_count_voutes" />
           </div>
           <div id="exchange_form_error"></div>
           <div onclick="penalty.voutes()" id="exchange_points_button" class="blue_button_wrap small_blue_button"><div class="blue_button">Отправить</div></div>
          </div>
         </div>
		</div>
         <div<? if(!$urgent_requests) echo ' style="display: none"'; ?> id="express_content_send">
          <div id="exchange_history_ajax"></div>
           <? echo $urgent_requests; ?>

        </div>
       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>

<? include($root.'/include/scripts.php') ?>
 </body>
</html>
