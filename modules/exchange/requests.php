<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'exchange_requests';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/exchange.php');

if($ugroup != 4) {
 header('Location: /tasks');

 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Заявки на смену никнейма</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
<div id="header_load"></div>   
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
         <div class="tasks_tabs">
             <a class="tab active" href="/exchange/requests" onclick="nav.go(this); return false;"><div class="tabdiv">Все заявки</div></a>
             <a class="tab" href="/exchange/requests" onclick="nav.go(this); return false;"><div class="tabdiv">Расмотренные</div></a>
             <a class="tab" href="/exchange/requests" onclick="nav.go(this); return false;"><div class="tabdiv">Отклонненые</div></a>
         </div>
        <? echo $exchange->admin_requests(); ?>
       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
