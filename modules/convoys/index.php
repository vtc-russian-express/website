<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'convoys';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
include($root.'/inc/system/profile.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');
require($root.'/inc/classes/convoys.php');
require($root.'/inc/classes/users.php');

$convoys_id = $_GET['id'];

if($uban_type) {
 header('Location: /blocked');
 exit;
}

$convoys_list_num = $convoys->convoys_num();
$convoys_all = $convoys->convoys_all($convoys_id);
$uconvoys = $convoys_all ? $convoys_all : '<div id="convoys_null">Ни найдено ни одного мероприятия.</div>';

$index_convoys = $convoys->convoys_index();

$number = $user->info_number($user_id);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Конвои компании</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
<div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>
   <div id="content">
       <? include($root.'/include/left.php') ?>
    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
     <? if(!$convoys_id) { ?>
      <div class="info-header-block convoys_img">
         <h4>Мероприятия компании</h4>
         <p>Здесь отображаются мероприятия добавленные администратором на ближайшие время.</p>
         <p>Неадекватное поведение или оскорбление сотрудников компании карается увольнением с компании!</p>
         <?php if($user_logged) { ?> <p><span class="convoys_number">Ваш порядковый номер в колонне <b><? echo $number; ?></b>.</span></p> <? } ?>
         <div class="info-bottom" onclick="users._support_bottem();">
          Если у вас возникли какие-то вопросы то напишите в поддержку »
         </div>
      </div>

      <div class="new_wrap_convoys">
       <? echo $index_convoys; ?>
       <span id="next_page_small_c"></span>
      <? if($convoys_list_num > 3) { ?>
      <div id="convoys_load_next" class="load_next" style="margin-bottom: 0px;" onclick="convoys._next();">Показать прошедшие мероприятия »</div>
      <? } ?>
      <div id="next_page_small_d">1</div>
      </div>

    <? } else { ?>

      <? echo $uconvoys; ?>

     <? } ?>

       </div>
      </div>
     </div>
     <input type="hidden" id="task_wnd_open_val">
     <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
 </div>
</div>
<? include($root.'/include/scripts.php'); ?>
 </body>
</html>
