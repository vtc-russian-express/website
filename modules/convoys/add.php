<?php

$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'add_convoys';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');
require($root.'/inc/classes/convoys.php');
require($root.'/inc/classes/users.php');

if($ugroup != 4) {
  header('Location: /tasks');
  exit;
}
$section = $_GET['section'];

if(!empty($section)){

    switch($section){
        case 'likes':   $cat_id = 1; break;
        case 'reposts': $cat_id = 2; break;
        case 'list':    $cat_id = 3; break;
        default:
            $cat_id = 1;
    }

}

$convoys_list_num = $convoys->convoys_history_num();
$result_convoys_history = $convoys->convoys_history();
$number = $user->info_number($user_id);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Добавление конвоя</title>
<? include($root.'/include/head.php'); ?>

 </head>
 <body>
<div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
           <div class="info-header-block convoys_img">
               <h4>Мероприятия компании</h4>
               <p>Здесь отображаются мероприятия добавленные администратором на ближайшие время.</p>
               <p>Неадекватное поведение или оскорбление сотрудников компании карается увольнением с компании!</p>
               <?php if($user_logged) { ?> <p><span class="convoys_number">Ваш порядковый номер в колонне <b><? echo $number; ?></b>.</span></p> <? } ?>
               <div class="info-bottom" onclick="users._support_bottem();">
                   Если у вас возникли какие-то вопросы то напишите в поддержку »
               </div>
           </div>
        <div class="tasks_tabs">
            <a class="tab <? if($cat_id <= 1) echo ' active'; ?>" href="/convoys/add?section=likes" onclick="nav.go(this); return false;"><div class="tabdiv">Добавление конвоя</div></a>
            <a class="tab <? if($cat_id == 2) echo ' active'; ?>" href="/convoys/add?section=reposts" onclick="nav.go(this); return false;"><div class="tabdiv">Отложенный конвой</div></a>
            <a class="tab <? if($cat_id == 3) echo ' active'; ?>" href="/convoys/add?section=list" onclick="nav.go(this); return false;"><div class="tabdiv">История</div></a>
        </div>

         <? if($cat_id <= 1) { ?>
             <div id="convoy_add">
                 <div id="convoys_add_inner">
                     <div id="task_add_error" class="error_msg error"></div>
                     <div class="legend">Дата проведения:</div>
                     <div class="field"><input type="text" id="udate" iplaceholder="23.01.2018"></div>
                     <div class="legend pad">Сбор/Выезд:</div>
                     <div class="field"><input type="text" id="usbor" style="width: 148px;" iplaceholder="12:00"><span style="margin: 0 5px 0 5px;">:</span><input type="text" id="usbor2" style="width: 148px;" iplaceholder="12:30"></div>
                     <div class="legend pad">Место старта:</div>
                     <div class="field"><input type="text" id="ustart" iplaceholder="Lyon(TRADE-AuX)"></div>
                     <div class="legend pad">Место отдыха:</div>
                     <div class="field"><input type="text" id="uoldstop" iplaceholder="Leipzig(Tre-eT)"></div>
                     <div class="legend pad">Место финиша:</div>
                     <div class="field"><input type="text" id="ustop" iplaceholder="Dortmund(карьер)"></div>
                     <div class="legend pad">Протяженность маршрута:</div>
                     <div class="field"><input type="text" id="ulong" iplaceholder="2588"></div>
                     <div class="legend pad">Тип:</div>
                     <div class="field"><div id="convoys_add"></div>
                     <div class="legend pad">Небольшое описание конвоя:</div>
                     <div class="field"><textarea iplaceholder="Небольшое описание конвоя..." id="uconvoys_field_text"></textarea></div>
                     <div class="legend pad">Прицеп/Маршрут:</div>
                     <div class="field">
                         <div id="convoys_images_attach"></div>
                         <input id="support_images_attach_img_field_ids" type="hidden">
                         <div id="support_grad_progress"></div>
                         <div id="convoys_add_content_buttons">
                             <div id="convoys_content_send_buttons_right">
                                 <a href="javascript://" id="test_images">Прекрепить изображение <iframe id="support_upload_iframe" name="support_upload_iframe"></iframe>
                                     <form action="/convoys/img.upload" enctype="multipart/form-data" method="post" target="support_upload_iframe"><input id="support_add_img_file" name="file" onchange="convoys.upload_img(); return false;" type="file"> <input id="support_upload_iframe_submit" style="display: none;" type="submit"></form>
                                 </a>
                             </div>
                         </div>
                     </div>
                         <div class="field"><div onclick="convoys._add(0)" id="add_task_button" class="blue_button_wrap"><div class="blue_button">Добавить мероприятия</div></div></div>
                     </div>
                 </div>
             </div>
         <? } elseif($cat_id == 2) { ?>
             <div id="convoy_add">
                 <div id="convoys_add_inner">
                     <div id="task_add_error" class="error_msg error"></div>
                     <div class="legend">Дата проведения:</div>
                     <div class="field"><input type="text" id="udate" iplaceholder="23.01.2018"></div>
                     <div class="legend pad">Сбор/Выезд:</div>
                     <div class="field"><input type="text" id="usbor" style="width: 148px;" iplaceholder="12:00"><span style="margin: 0 5px 0 5px;">:</span><input type="text" id="usbor2" style="width: 148px;" iplaceholder="12:30"></div>
                     <div class="legend pad">Место старта:</div>
                     <div class="field"><input type="text" id="ustart" iplaceholder="Lyon(TRADE-AuX)"></div>
                     <div class="legend pad">Место отдыха:</div>
                     <div class="field"><input type="text" id="uoldstop" iplaceholder="Leipzig(Tre-eT)"></div>
                     <div class="legend pad">Место финиша:</div>
                     <div class="field"><input type="text" id="ustop" iplaceholder="Dortmund(карьер)"></div>
                     <div class="legend pad">Протяженность маршрута:</div>
                     <div class="field"><input type="text" id="ulong" iplaceholder="2588"></div>
                     <div class="legend pad">Тип:</div>
                     <div class="field"><div id="convoys_add"></div>
                         <div class="legend pad">Небольшое описание конвоя:</div>
                         <div class="field"><textarea iplaceholder="Небольшое описание конвоя..." id="uconvoys_field_text"></textarea></div>
                     <div class="legend pad">Прицеп/Маршрут:</div>
                     <div class="field">
                         <div id="convoys_images_attach"></div>
                         <input id="support_images_attach_img_field_ids" type="hidden">
                         <div id="support_grad_progress"></div>
                         <div id="convoys_add_content_buttons">
                             <div id="convoys_content_send_buttons_right">
                                 <a href="javascript://" id="test_images">Прекрепить изображение <iframe id="support_upload_iframe" name="support_upload_iframe"></iframe>
                                     <form action="/convoys/img.upload" enctype="multipart/form-data" method="post" target="support_upload_iframe"><input id="support_add_img_file" name="file" onchange="convoys.upload_img(); return false;" type="file"> <input id="support_upload_iframe_submit" style="display: none;" type="submit"></form>
                                 </a>
                             </div>
                         </div>
                     </div>
                         <div class="legend pad">Дата публикации:</div>
                         <div class="field"><input type="text" id="udate_post"></div>
                         <div class="field"><div onclick="convoys._add(1)" id="add_task_button" class="blue_button_wrap"><div class="blue_button">Добавить мероприятия</div></div></div>
                     </div>
                 </div>
             </div>
         <? } elseif($cat_id == 3) { ?>
           <div class="list_tasks">
            <div id="head-info-block"><span>Список всех мероприятий</span></div>
               <?php echo (($result_convoys_history) ? $result_convoys_history : '<div class="fines_comment_question"><div id="my_tasks_blacklist_none">К сожалению, мероприятия не найдены.</div></div>'); ?>
               <div id="next_page_small_c"></div>
           </div>
           <?php if($convoys_list_num > 10) { ?><div id="convoys_load_next" class="load_next" style="margin-bottom: 0px;border-radius: 0px 0px 4px 4px;margin-top: -3px;border-top: 1px solid #e7e8ec;" onclick="convoys._next_history();">Показать еще мероприятия »</div><? } ?>
             <div id="next_page_small_d">1</div>
         <? } ?>

       <? if($cat_id <= 1) { ?>

         <!-- <div class="body">
          <?php echo (($result_convoys_history) ? $result_convoys_history : '<div class="fines_comment_question"><div id="my_tasks_blacklist_none">К сожалению, мероприятия не найдены.</div></div>'); ?>
 		     <span id="next_page_small_c"></span>
 		     <? if($convoys_list_num > 4) { ?>
 		     <div id="convoys_load_next" class="load_next" style="margin-bottom: 0px;margin-top: 0px;" onclick="convoys._next_history();">Показать еще мероприятия</div>
          <div id="next_page_small_d">1</div>
          <? } ?>
          </div> -->

          <? } elseif($cat_id == 2) { ?>

           <!--<div class="body">
            <?php echo (($result_convoys_history_post) ? $result_convoys_history_post : '<div class="fines_comment_question"><div id="my_tasks_blacklist_none">К сожалению, мероприятия не найдены.</div></div>'); ?>
   		     <span id="next_page_small_c"></span>
   		     <? if($convoys_list_num_post > 4) { ?>
   		     <div id="convoys_load_next" class="load_next" style="margin-bottom: 0px;margin-top: 0px;" onclick="convoys._next_history_post();">Показать еще отложенные мероприятия</div>
            <div id="next_page_small_d">1</div>
            <? } ?>
            </div> -->

           <? } ?>
		<input type="hidden" id="select_convoys_add_value" value="">
       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
