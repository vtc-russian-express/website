<?php
$root = $_SERVER['DOCUMENT_ROOT'];

$page_name = 'awards';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
require($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/profile.php');

if(!$user_logged) {
 // если пользователь неавторизован, перенаправляем на главную
 header('Location: /');
 exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Мои достижения</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
      <div id="right_wrap_b">
    <div id="right">
      <div class="main nopad">
        <div class="tasks_tabs">
            <a class="tab" href="/id<? echo $user_id; ?>" onclick="nav.go(this); return false;"><div class="tabdiv">Мой аккаунт</div></a>
            <a class="tab" href="/users" onclick="nav.go(this); return false;"><div class="tabdiv">Сотрудники</div></a>
            <a class="tab active" href="/award" onclick="nav.go(this); return false;"><div class="tabdiv">Мои достижения</div></a>
            <span class="num_users" style="margin-top: 16px;"><b>Николай</b>, у вас <b>1 уровень</b>.</span>
        </div>

        <div class="content_list_award">
            <div class="panel"><div class="count">Добро пожаловать</div></div>
            <div class="bundle">
                <div class="info-points">+3 балла</div>
                <div class="img"><img src="/images/awards/welcome/welcome_1.png"></div>
                <div class="points">
                    <span>ЭКЗАМЕН</span>
                    <i>Получите новою категорию стажёра.</i>
                </div>
            </div>
            <div class="bundle">
                <div class="info-points">+15 баллов</div>
                <div class="img"><img src="/images/awards/welcome/welcome_2.png"></div>
                <div class="points">
                  <span>3 ДРУГА</span>
                  <i>Пригласите 3-х друзей которые пройдут испытательный срок.</i>
                </div>
                <span class="empty"><b>2 друга приглашено.</b></span>
            </div>
            <div class="bundle">
                <div class="info-points">+10 баллов</div>
                <div class="img"><img src="/images/awards/welcome/welcome_3.png"></div>
                <div class="points">
                    <span>5 КОНВОЕВ</span>
                    <i>Необходимо проехать 5 конвоев</i>
                </div>
                <span class="empty"></span>
            </div>
            <div class="bundle">
                <div class="info-points">+8 баллов</div>
                <div class="img"><img src="/images/awards/welcome/welcome_4.png"></div>
                <div class="points">
                    <span>3 ЗАКАЗА</span>
                    <i>Необходимо выполнить 3 срочных заказ <b>+3 балла</b>.</i>
                </div>
                <span class="empty">
                      <b>Выполнено: 1 заказа.</b>
                    </span>
            </div>
            <div class="panel"><div class="count">Конвои компании</div></div>
            <div class="bundle">
                <div class="info-points">+10 баллов</div>
                <div class="img"><img src="/images/awards/convoy/award_convoy_1.png"></div>
                <div class="points">
                    <span>15 КОНВОЕВ</span>
                    <i>Проехать 15 конвоев</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+15 баллов</div>
                <div class="img"><img src="/images/awards/convoy/award_convoy_2.png"></div>
                <div class="points">
                    <span>30 КОНВОЕВ</span>
                    <i>Проехать 5 конвоев</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+30 баллов</div>
                <div class="img"><img src="/images/awards/convoy/award_convoy_3.png"></div>
                <div class="points">
                    <span>75 КОНВОЕВ</span>
                    <i>Проехать 5 конвоев и получить 30 баллов</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+50 баллов</div>
                <div class="img"><img src="/images/awards/convoy/award_convoy_4.png"></div>
                <div class="points">
                    <span>150 КОНВОЕВ</span>
                    <i>Проехать 5 конвоев и получить 50 баллов</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="panel"><div class="count">Срочные заказы</div></div>
            <div class="bundle">
                <div class="info-points">+5 баллов</div>
                <div class="img"><img src="/images/awards/tasks/award_tasks_1.png"></div>
                <div class="points">
                    <span>СТУДЕНТ</span>
                    <i>Выполнить 15 заказов</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+15 баллов</div>
                <div class="img"><img src="/images/awards/tasks/award_tasks_2.png"></div>
                <div class="points">
                    <span>РАБОТЯГА</span>
                    <i>Выполнить 30 заказов</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+30 баллов</div>
                <div class="img"><img src="/images/awards/tasks/award_tasks_4.png"></div>
                <div class="points">
                    <span>ДОСТАВЩИК</span>
                    <i>Выполнить 75 заказов</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+50 баллов</div>
                <div class="img"><img src="/images/awards/tasks/award_tasks_3.png"></div>
                <div class="points">
                    <span>МАСТЕР</span>
                    <i>Выполнить 150 заказов</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="panel"><div class="count">Финансы</div></div>
            <div class="bundle">
                <div class="info-points">+5 баллов</div>
                <div class="img"><img src="/images/awards/money/award_finance_1.png"></div>
                <div class="points">
                    <span>ИПШНИК</span>
                    <i>Заработать 200 000$</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+15 баллов</div>
                <div class="img"><img src="/images/awards/money/award_finance_2.png"></div>
                <div class="points">
                    <span>ЮНЫЙ БИЗНЕСМЕН</span>
                    <i>Заработать 500 000$</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+30 баллов</div>
                <div class="img"><img src="/images/awards/money/award_finance_3.png"></div>
                <div class="points">
                    <span>УМНЫЙ БИЗНЕСМЕН</span>
                    <i>Заработать 1,5млн $</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+50 баллов</div>
                <div class="img"><img src="/images/awards/money/award_finance_4.png"></div>
                <div class="points">
                    <span>МИЛЛИОНЕР</span>
                    <i>Заработать: 3,5млн $</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="panel"><div class="count">Расстояние</div></div>
            <div class="bundle">
                <div class="info-points">+5 баллов</div>
                <div class="img"><img src="/images/awards/road/road_1.png"></div>
                <div class="points">
                    <span>ВОДЯТЕЛ</span>
                    <i>Проехать 10-ТЫС КМ</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+15 баллов</div>
                <div class="img"><img src="/images/awards/road/road_2.png"></div>
                <div class="points">
                    <span>ПИОНЕР</span>
                    <i>Проехать 25-ТЫС КМ</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+30 баллов</div>
                <div class="img"><img src="/images/awards/road/road_3.png"></div>
                <div class="points">
                    <span>ЧАЙНИК</span>
                    <i>Проехать 75-ТЫС КМ</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
            <div class="bundle">
                <div class="info-points">+50 баллов</div>
                <div class="img"><img src="/images/awards/road/road_4.png"></div>
                <div class="points">
                    <span>ВОДИТЕЛЬ</span>
                    <i>Проехать 150-ТЫС КМ</i>
                </div>
                <span class="empty">
                      <div class="ui_progress_bar_body south">
                       <div id="user_rating" class="ui_progress_bar_content">0%</div>
                       <div id="user_rating_w" class="ui_progress_bar_tail" style="width: 0%;"></div>
                      </div>
                    </span>
            </div>
        </div>
      </div>
    </div>
  </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
