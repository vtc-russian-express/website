<?php
$root = $_SERVER['DOCUMENT_ROOT'];

$page_name = 'award';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
require($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/profile.php');
require($root.'/inc/classes/award.php');

if(!$user_logged) {
 // если пользователь неавторизован, перенаправляем на главную
 header('Location: /');
 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Мои достижения</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
      <div id="right_wrap_b">
    <div id="right">
      <div class="main nopad">
        <div class="info-header-block archive_img">
          <h4>Мои достижения</h4>
          <p>Здесь отображаются достижения которые вы должны выполнить.</p>
          <p>За каждое достижение вы получаете определенное количество баллов, с каждым уровнем достижения становятся сложней.</p>
          <p>Достижения в скором времени пополнятся!</p>
          <div class="info-bottom">
          Не пытайтесь обмануть систему, за это вы получите бан!
          </div>
        </div>

        <div class="content_list_award">
          <div id="head-info-block">
            <span>Список достижений</span>
            <a href="/support/new" onclick="nav.go(this); return false;">Нужна помощь ?</a>
          </div>
          <div class="list_awards">
            <? echo $award->award_list(); ?>
          </div>
        </div>

      </div>
    </div>
  </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
