<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'my_settings';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Мои настройки</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
        <div class="tasks_tabs">
            <a class="tab active" href="/settings" onclick="nav.go(this); return false;"><div class="tabdiv">Общее</div></a>
            <a class="tab" href="/settings/security" onclick="nav.go(this); return false;"><div class="tabdiv">Безопасность</div></a>
            <a class="tab" href="/settings/ref?menu=1" onclick="nav.go(this); return false;"><div class="tabdiv">Партнерская программа</div></a>
            <a class="tab" href="/settings/balance" onclick="nav.go(this); return false;"><div class="tabdiv">Баланс</div></a>
        </div>
		 <div class="info-header-block settings_img">
         <h4>Мои настройки</h4>
          <p>Здесь вы можете изменить свои данные.</p>
		       <p>Вы можете изменить ваши данные которые вы указывали при регистрации.</p>
           <p>Обратите внимание Имя и Фамилию можно изменять раз в 7 дней.</p>
           <div class="info-bottom" onclick="users._support_bottem();">
            Если у вас возникли вопросы то обратитесь в поддержку »
           </div>
          </div>
        <div id="my_settings">
         <div class="my_settings_content">
		  <div id="my_settings_account">
           <div id="my_settings_account_name">Изменить никнейм</div>
           <div id="my_settings_account_error_nik"></div>
           <div id="my_settings_account_content">
            <div class="overflow_field">
             <div class="label">Текущий никнейм:</div>
             <div class="field field_settings_login"><? echo $users_nik; ?></div>
			 <input id="exchange_count_nik" type="hidden" value="<? echo $users_nik; ?>">
            </div>
			<? if($users_nik_activated == 1) { ?>
            <div id="my_settings_email_notif">Ваша заявка на смену никнейма рассматривается.</div>
		    <? } else { ?>
            <div class="overflow_field" id="new_nik">
             <div class="label label_f">Новый никнейм:</div>
             <div class="field">
              <input id="my_settings_account_nik_field" type="text">
              <br />
              <div onclick="users.change_send_nik();" id="settings_nik_button" class="blue_button_wrap small_blue_button"><div class="blue_button">Изменить никнейм</div></div>
             </div>
            </div>
			 <? } ?>
           </div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
