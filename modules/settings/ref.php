<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'my_settings';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
include($root.'/inc/system/profile_redirect.php');
require($root.'/inc/classes/refs.php');

$my_refs_num = $refs->my_num();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Мои настройки</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
 <div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
         <div class="tasks_tabs">
             <a class="tab" href="/settings" onclick="nav.go(this); return false;"><div class="tabdiv">Общее</div></a>
             <a class="tab" href="/settings/security" onclick="nav.go(this); return false;"><div class="tabdiv">Безопасность</div></a>
             <a class="tab active" href="/settings/ref?menu=1" onclick="nav.go(this); return false;"><div class="tabdiv">Партнерская программа</div></a>
             <a class="tab" href="/settings/balance" onclick="nav.go(this); return false;"><div class="tabdiv">Баланс</div></a>
         </div>
        <div id="settings_ref_content">
        <div class="info-header-block ref_img">
         <h4>Партнерская программа</h4>
          <p>Здесь отображаються ваши приглашенные друзья которые прошли испытательный срок</p>
		      <p>За каждого друга вы получаете <b>5 баллов</b>.</p>
          <p>Ваша реферальная ссылка - <b>//vtc-express.ru/reg<? echo $user_id; ?></b></p>
          <p>Скиньте вашу реферальную ссылку другу что-бы он зарегистрировался по ней!</p>
       <div class="info-bottom" onclick="users._support_bottem();">
        Если у вас возникли вопросы то обратитесь в поддержку »
       </div>
          </div>
         <div id="settings_ref_c">
          <div id="settings_ref_count">
           <? if($my_refs_num) { ?>Вы уже пригласили <? echo $my_refs_num; ?> <? echo declOfNum($my_refs_num, array('реферала', 'рефералов', 'рефералов')); ?>.<? } ?>
          </div>
          <div id="settings_ref_body">
           <? if($my_refs_num) { ?><? echo $refs->my(); ?><div id="next_page_small_c"></div><? } else { ?><div class="fines_comment_question_ref"><div id="my_tasks_blacklist_none">Вы ещё не приглашали друзей.</div></div><? } ?>
            <? if($my_refs_num > 10) { ?><div style="display: block;" onclick="refs._next();" id="next_page_small_t">Показать еще рефералов</div><? } ?>
            <div id="next_page_small_d">1</div>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
  </div>
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
