<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$page_name = 'reg';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
include($root.'/inc/system/profile.php');
require($root.'/inc/classes/sessions.php');
include($root.'/inc/system/usession.php');

if($user_logged) {
 // если пользователь авторизован, перенаправляем на срочные заказы
 header('Location: /tasks');
 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
 <head>
  <title>Регистрация</title>
<? include($root.'/include/head.php') ?>

 </head>
 <body>
<div id="header_load"></div>
 <div id="page">
<? include($root.'/include/header.php') ?>

   <div id="content">
<? include($root.'/include/left.php') ?>

    <div id="right_wrap">
     <div id="right_wrap_b">
      <div id="right">
       <div class="main nopad">
        <div class="info-header-block bg_reg">
         <h4>Регистрация</h4>
          <p>Создайте свой аккаунт за считанные секунды, У нас зарегистрировано больше <b>3000</b> сотрудников.</p>
          <p>Выполняйте достижения или же срочные заказы и повышайте рейтинг своего профиля.</p>
          <p>Пожалуйста укажите ваше настоящее имя и фамилия, так-же <b>e-mail</b> на него будет выслано сообщение.</p> 
        </div>
        <div id="form_reg" class="form">
         <div id="form_reg_inner">
          <div id="reg_cat_title">
           <div class="clearfix">
            <div id="reg_cat_title_image">
             <div id="reg_cat_img"></div>
            </div>
            <div id="reg_cat_title_message_arrow"></div>
            <div id="reg_cat_title_message">
             <div id="reg_cat_title_message_inner">
              Пройдите простую, <br>регистрацию заполните свои <b>настоящие данные</b>, фейковые аккаунты будут удалены с сайта!
             </div>
            </div>
           </div>
          </div>
          <div id="form_reg_error"></div>
           <div class="legend">Имя</div>
           <div class="field"><input type="text" id="first_name" maxlength="20" iplaceholder="Введите имя"></div>
           <div class="error first_name"></div>
           <div class="legend pad">Фамилия:</div>
           <div class="field"><input type="text" id="last_name" maxlength="30" iplaceholder="Введите фамилию"></div>
           <div class="error last_name"></div>
           <div class="legend pad">Логин:</div>
           <div class="field"><input type="text" id="reg_login" maxlength="20" iplaceholder="Введите логин"></div>
           <div class="error reg_login"></div>
           <div class="legend pad">Пароль:</div>
           <div class="field"><input type="password" id="reg_password" maxlength="30" iplaceholder="Введите пароль"></div>
           <div class="error reg_password"></div>
           <div class="legend pad">E-mail:</div>
           <div class="field"><input type="text" id="reg_email" iplaceholder="Введите e-mail"></div>
           <div class="error reg_email"></div>
           <div class="legend pad">Никнейм:</div>
           <div class="field"><input type="text" id="reg_nickname" iplaceholder="Введите никнейм"></div>
           <div class="error reg_nickname"></div>
           <div class="legend pad">Страница ВКонтакте:</div>
             <div class="field">
                 <div id="uLogined046d9c" data-ulogin="display=buttons;fields=first_name,last_name;lang=ru;redirect_uri=;callback=reg_get_vk">
                     <div class="reg_vk_add"><a href="javascript://" data-uloginbutton="vkontakte">Получить адрес страницы »</a></div>
                 </div>
                 <div id="ajax_vk"><input type="text" id="reg_vk_id" disabled="true" value=""><input type="hidden" value="0" id="reg_vk_id_ajax" /></div>
             </div>
           <div class="error reg_vk_id"></div>
           <div class="legend pad">День рождения:</div>
           <div class="field" style="width: 335px;">
            <div id="users_day"></div>
            <div id="users_separator">—</div>
            <div id="users_month"></div>
            <div id="users_separator">—</div>
            <div id="users_year"></div>
           </div>
           <div class="error reg_date"></div>
           <div class="legend pad">Кто вас пригласил:</div>
           <div class="field"><input type="text" id="reg_invited_nickname" iplaceholder="Введите его никнейм"></div>
           <div id="reg_terms_message">
           Регистрируясь на сайте, Вы автоматически соглашаетесь с <a href="/page/rules" class="link_blank" target="_blank" onclick="nav.go(this); return false">правилами сайта</a>.
           </div>
          <div onclick="users.reg()" id="reg_submit_button" class="blue_button_wrap">
            <div class="blue_button">Зарегистрироваться</div>
         </div>
         </div> 
        </div>
       </div>
      </div>
     </div>
<? include($root.'/include/footer.php') ?>

    </div>
   </div>
   <div id="live_counter"></div>
   <input type="hidden" value="<? echo $_GET['ref']; ?>" id="ureg_ref" />
   <input type="hidden" id="captcha_key" />
   <input type="hidden" id="captcha_code" />
   <input type="hidden" value="1" id="ureg_wnd_open" />
  </div>
  <input type="hidden" value="<? echo $usession; ?>" id="ssid">
<? include($root.'/include/scripts.php') ?>
 </body>
</html>
