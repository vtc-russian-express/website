<?php
$root = $_SERVER['DOCUMENT_ROOT'];

$page_name = 'tasks';

require($root.'/inc/classes/db.php');
include($root.'/inc/system/redis.php');
include($root.'/inc/functions.php');
include($root.'/inc/variables.php');
require($root.'/inc/classes/users.php');
require($root.'/inc/system/profile.php');
require($root.'/inc/classes/orders.php');
require($root.'/inc/classes/tasks-2.php');

if(!$user_logged) {
    // если пользователь неавторизован, перенаправляем на главную
    header('Location: /');
    exit;
}

$list_section = $_GET['section'];
$list_section_t = ($list_section == 'rules') ? 1 : 0;

$tasks_profile_list = $tasks->profileList();

$tasks_list_num = $tasks->my_num();
$report_num = $tasks->all_report();

$check_number_reports = $tasks->check_number_reports();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <title>Срочные заказы</title>
    <? include($root.'/include/head.php') ?>
</head>
<body>
<div id="header_load"></div>
<div id="page">
    <? include($root.'/include/header.php') ?>
    <div id="content">
        <? include($root.'/include/left.php') ?>
        <div id="right_wrap">
            <div id="right_wrap_b">
                <div id="right">
                    <div class="main nopad">
                        <div class="info-header-block tasks_img">
                            <h4>Срочные заказы</h4>
                            <p>Здесь отображаются <b>срочные заказы</b> которые вы должны доставить в указанный срок</p>
                            <p>Срочные заказы распространяются на 2 типа: Дневные и Вечерние.</p>
                            <p>Дневные с <b>00:00</b> до <b>16:00</b> Вечерние с <b>16:00</b> до <b>00:00</b>.</p>
                            <div class="info-bottom" onclick="users._support_bottem();">
                                Если у вас возникли вопросы то обратитесь в поддержку »
                            </div>
                        </div>
                        <div class="list_tasks">
                            <div id="head-info-block">
                                <span>Список заказов</span>
                            </div>
                            <? if($category == 0) { ?>
                                <div id="tasks_none"><div id="text_no">Вы на испытательном сроке, после сдаче экзамена, срочные заказы будут доступны.</div></div>
                            <? } elseif($upoints_fine) { ?>
                                <div id="tasks_none"><div id="text_no">К сожалению, у Вас есть непогашенные <a href="/complaints" onclick="nav.go(this); return false">штрафы</a>, после погашения срочные заказы будут доступны.</div></div>
                            <? } elseif($check_number_reports == 3 && $ucategory == 1) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>3 заказа</b>. Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 6 && $ucategory == 2) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>6 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 9 && $ucategory == 3) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>9 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 9 && $ucategory == 4) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>9 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 12 && $ucategory == 5) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>12 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 12 && $ucategory == 6) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>12 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 15 && $ucategory == 7) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>15 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 18 && $ucategory == 8) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>18 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 18 && $ucategory == 9) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>18 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 21 && $ucategory == 10) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>21 заказ</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 16 && $ucategory == 11) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>16 заказов</b> Вы их выполнили.</div></div>
                            <? } elseif($check_number_reports == 16 && $ucategory == 12) { ?>
                                <div id="tasks_none"><div id="text_no">У категории <b><? echo $ucategory ?></b> есть лимит на выполнение срочных заказов в сутки <b>16 заказов</b> Вы их выполнили.</div></div>
                            <? } else { ?>
                                <?php echo (($tasks_profile_list) ? $tasks_profile_list : '<div id="tasks_none"><div id="text_no">К сожалению на сегодня нету срочных заказов.</div></div>'); ?>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <? include($root.'/include/footer.php') ?>
        </div>
    </div>
</div>
<? include($root.'/include/scripts.php') ?>
</body>
</html>