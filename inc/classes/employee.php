<?php
class top {
	public function users_list() {
		global $db, $user_logged, $ugroup, $noavatar, $online_limit;

		$page = (int) abs($_GET['page']) * 12;
		$section = $db->escape($_GET['section']);
		$search = $db->escape($_GET['q']);

		if($section == 'online') {
	     $section_sql = "AND `ulast_time` >= '$online_limit'";
	    } else {
	 	 $section_sql = "";
	    }

		if($section == 'online') {
			$users_limit = 50;
		} else {
			$users_limit = 12;
		}

		if($search) {
			$search_sql = "AND (`uname` LIKE '%" . $search ."%' OR `nik` LIKE '%" . $search ."%' OR `ulast_name` LIKE '%" . $search ."%')";
		} else {
			$search_sql = "";
		}

		$q = $db->query("SELECT * FROM  users WHERE udel = 0 AND uban_type = 0 ".$section_sql." ".$search_sql." LIMIT $page, $users_limit");

		$i = 1;

		while ($d = $db->assoc($q)) {
			$top_avatar = $d['uavatar'] ? $d['uavatar'] : $noavatar;
			$top_vk_id = $d['uvk_id'];
			$top_first_name = $d['uname'];
			$uid = $d['uid'];
			$users_nik = $d['nik'] ? $d['nik'] : 'не указан';
			$uvk_id = $d['uvk_id'];
			$uvk_id_page = $uvk_id ? '<a href="http://vk.com/id' . $uvk_id . '" target="_blank">vk.com/id' . $uvk_id . '</a>' : 'не прикреплена';
			$upoints = $d['upoints'];
			$upoints_fine = $d['upoints_fine'];
			$top_last_name = $d['ulast_name'];
			$top_last_time = $d['ulast_time'];
			$top_gender = $d['ugender'];
			$ugroup_emp = $d['ugroup'];
			$category = $d['category'];
			$uban_type = $d['uban_type'];
			$ulast_time = $d['ulast_time'];
			$convoys_count = $d['convoys_count'];
			$mile = $d['mile'];
			$num_convoys = top::users_convoy_num($uid);
			$num_urget = top::users_urgent_num($uid);

			if ($ulast_time >= $online_limit) {
				$result_time_online = '<span id="online"></span>';
			} else {
				$result_time_online = '';
			}


			$template .= '
			<div class="flat_users_list fl_l" onclick="nav.go(\'\', \'/id'.$uid.'\'); return false">
			    <div class="img" style="background-image: url('.$top_avatar.');"></div>
			    <div class="name">'.$top_first_name.' '.$top_last_name.' '.$result_time_online.'</div>

			    <div class="item">
			        <div class="clearfix">

			            <div class="fl_l count_wrap">
			                <div class="count">'.$upoints.'</div>
			                <div class="label">'.declOfNum($upoints, array('балл', 'балла', 'баллов')).'</div>
			            </div>

			            <div class="fl_l need_count_wrap">
			                <div class="count">'.$num_convoys.'</div>
			                <div class="label">'.declOfNum($num_convoys, array('конвой', 'конвоя', 'конвоев')).'</div>
			            </div>

			            <div class="fl_l speed_wrap">
			                <div class="speed">'.$mile.'</div>
			                <div class="label">Пробег</div>
			            </div>

			        </div>
			    </div>
			</div>
      ';
			$i++;
		}
		return $template ? $template : '<div class="no_results">Ваш запрос не дал результатов</div>';
	}

	public function users_convoy_num($uid = null) {
		global $db;

		$q = $db->query("SELECT `id` FROM `archive` WHERE `module` = '1' AND `type` = '2' AND `to` = '$uid' ORDER BY `id` DESC");
		$n = $db->num($q);

		return $n;
	}

	public function users_urgent_num($uid = null) {
		global $db;

		$q = $db->query("SELECT `id` FROM `urgent` WHERE `status` = '2' AND `no_active` = '1' AND `user_id` = '$uid' ORDER BY `id` DESC");
		$n = $db->num($q);

		return $n;
	}

	public function users_list_num() {
	 global $db, $online_limit;

	 $section = $db->escape($_GET['section']);
	 $search = $db->escape($_GET['search']);

	 if($section == 'online') {
    $section_sql = "AND `ulast_time` >= '$online_limit'";
	 } else {
		$section_sql = "";
	 }

	 if($search) {
	 	$search_sql = "AND `uname` LIKE '%".$search."%' OR `nik` LIKE '%".$search."%' OR `ulast_name`LIKE '%".$search."%'";
	 } else {
		$search_sql = "";
	 }

	 $q = $db->query("SELECT `uid`, `uname`, `ulast_name` FROM `users`WHERE `udel` = 0 ".$section_sql." ".$search_sql."");
	 $n = $db->num($q);


	 return $n;
	}

}

$top = new top;
