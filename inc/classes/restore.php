<?php
class restore {
 public function restore_email() {
  global $db, $dbName, $session, $site_url, $time;
  
  $email = $db->escape(trim($_GET['email']));
  $code = $db->escape(trim($_GET['code']));
  
  if(preg_match("/^[a-zA-Z0-9_\.\-]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,6}$/i", $email)) {
   $query = "SELECT `uid`, `ulogin`, `uemail` FROM `users` WHERE `uemail` = '$email'";
  } else {
   $query = "SELECT `uid`, `ulogin`, `uemail` FROM `users` WHERE `ulogin` = '$email'";
  }
  
  $q = $db->query($query);
  $d = $db->fetch($q);
  
  $d_uid = $d['uid'];
  $d_uemail = $d['uemail'];
  $d_ulogin = $d['ulogin'];
  $hash = md5("$d_uid+$d_uemail+$time");
  $new_password = rand_str(14);
  $new_password_md5 = md5(md5($new_password));
  
  if($d_uid) {
   if(!$session->get('unique_key_restore'.$d_uid)) {
    $session->add('unique_key_restore'.$d_uid, rand_str(6));
   }
   
   $key_get = $session->get('unique_key_restore'.$d_uid);
   
   if(!$code) {
    send_email($d_uemail, 'Восстановление доступа к аккаунту', 'Проверочный код: '.$key_get.' <br /> <br />С уважением, <br /><a href="'.$site_url.'">Команда Russian Express</a>.');
   }
   
   if($key_get == $code) {
    if($db->query("UPDATE  `$dbName`.`users` SET  `upassword` =  '$new_password_md5', `uhash` =  '$hash' WHERE  `users`.`uid` = '$d_uid';")) {
     $json = array('success' => 1, 'ulogin' => $d_ulogin, 'upassword' => $new_password);
    } else {
     $json = array('error_text' => 'Ошибка соединения с сервером. Попробуйте позже.');
    }
   } else {
    $json = array('email' => $d_uemail);
   }
  } elseif($key_get != $code) {
   $json = array('email' => $d_uemail);
  } else {
   $json = array('error_text' => 'Такой пользователь не зарегистрирован на Russian Express.');
  }
  
  return json_encode($json);
 }
}

$restore = new restore;
?>