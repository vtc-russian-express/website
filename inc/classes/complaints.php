<?php
class complaints
{
	public function list_table_user_num()
	{
		global $db, $user_id;

		$q = $db->query("SELECT `id` FROM `complaints` WHERE `to` = '$user_id'");
		$n = $db->num($q);

		return $n;
	}

	public function list_table_user()
	{ // строим таблицу штрафных баллов сотруднику
		global $db, $user_id;

		$uid = (int)$_GET['uid'];
		$page = (int)$_GET['page'];
		$start_page = (!$page) ? 0 : $page - 1;
		$start_limit = $start_page * 10;

		$q = $db->query(
			"
   SELECT complaints.id, complaints.section, complaints.url, complaints.type, complaints.time, complaints.upoints, users.uid, users.uavatar, users.uname, users.ulast_name
   FROM `complaints`
   INNER JOIN `users` ON complaints.from = users.uid
   WHERE complaints.to = '$user_id'
   ORDER BY `id` DESC LIMIT $start_limit, 10
  "
		);
		while ($d = $db->fetch($q)) {
			$section = $d['section'];
			$url = $d['url'];
			$type = $d['type'];
			$upoints = $d['upoints'];
			$time = $d['time'];
			$name = no_name($d['uname'] . ' ' . $d['ulast_name']);
			$avatar = no_avatar($d['uavatar']);
			$ids = $d['id'];
			$time_pay = time() + rand(999, 9999);
			switch ($type) {
				case 1:
					$status_title = 'Рассматривается..';
					$status_class = 'success';
					$status_button = '<div class="buttons" style="opacity:0.5;"><div class="blue_button_wrap"><div class="blue_button">Погасить штраф</div></div> <div id="pay_box" class="blue_button_wrap"><div class="blue_button">Оплатить штраф</div></div></div>';
					$button_appeal = '<div class="x" title="Обжаловать штраф"><i></i></div>';

					break;

				case 2:
					$status_title = 'Штраф погашен';
					$status_class = 'working';
					$status_button = '<div class="buttons" style="opacity:0.5;"><div class="blue_button_wrap"><div class="blue_button">Погасить штраф</div></div> <div id="pay_box" class="blue_button_wrap"><div class="blue_button">Оплатить штраф</div></div></div>';
					$button_appeal = '<div class="x" title="Обжаловать штраф"><i></i></div>';

					break;

				case 3:
					$status_title = 'Необходимо погасить штраф';
					$status_class = 'active';
					$status_button = '<div class="buttons"><div class="blue_button_wrap" onclick="penalty.addpenalty(' . $ids . ');"><div class="blue_button">Погасить штраф</div></div><div id="pay_box" class="blue_button_wrap" style="margin-left: 4px;" onclick="penalty.fastOrderPayBox(' . $ids . ');"><div class="blue_button">Оплатить штраф</div></div></div>';
					$button_appeal = '<div class="x" title="Обжаловать штраф" onclick="penalty.send_delete(' . $ids . ', ' . $upoints . ', ' . $user_id . ');"><i></i></div>';

					break;

				case 4:
					$status_title = 'Штраф оплачен';
					$status_class = 'pay';
					$status_button = '<div class="buttons" style="opacity:0.5;"><div class="blue_button_wrap"><div class="blue_button">Погасить штраф</div></div><div id="pay_box" style="margin-left: 4px;" class="blue_button_wrap"><div class="blue_button">Оплатить штраф</div></div></div>';
					$button_appeal = '<div class="x" title="Обжаловать штраф"><i></i></div>';

					break;

				case 5:
					$status_title = 'Апелляция одобрена';
					$status_class = 'working';
					$status_button = '<div class="buttons" style="opacity:0.5;"><div class="blue_button_wrap"><div class="blue_button">Погасить штраф</div></div><div id="pay_box" style="margin-left: 4px;" class="blue_button_wrap"><div class="blue_button">Оплатить штраф</div></div></div>';
					$button_appeal = '<div class="x" title="Обжаловать штраф"><i></i></div>';

					break;

				case 6:
					$status_title = 'Апелляция отклонена, штраф не погашен';
					$status_class = 'no_appe';
					$status_button = '<div class="buttons"><div class="blue_button_wrap" onclick="penalty.addpenalty(' . $ids . ');"><div class="blue_button">Погасить штраф</div></div><div id="pay_box" class="blue_button_wrap" style="margin-left: 4px;" onclick="penalty.fastOrderPayBox(' . $ids . ');"><div class="blue_button">Оплатить штраф</div></div></div>';
					$button_appeal = '<div class="x" title="Обжаловать штраф" onclick="penalty.set_error_head();"><i></i></div>';

					break;
			}

			$template .= '
      <div class="task my" id="complaintsids' . $ids . '">
          <div class="inner" id="complaintsid">
              <div class="image">
                  <div class="icon repost"></div>
              </div>
              <div class="title" style="width: 371px; overflow: hidden; word-break: break-all;">
                  <a>Штрафные баллы</a> вы получили <b>' . $upoints . '</b> ' . declOfNum($upoints, array('штрафной', 'штрафных', 'штрафных')) . ' ' . declOfNum($upoints, array('балл', 'балла', 'баллов')) . '</div>
              <div class="icons">
                  ' . $button_appeal . '
              </div>
          </div>
          <div id="tooltip_lot_money_info" style="margin-left: 16px;position: absolute;width: 280px;top: 1769px;left: 554px;"></div>
          <div class="footer">
              ' . $status_button . '
              <div class="status">
                  <div class="result_' . $status_class . '">' . $status_title . '</div>
              </div>
          </div>
          <input type="hidden" value="' . $upoints . '" id="upoints' . $ids . '">
          <input type="hidden" value="' . $upoints . '" id="pay' . $ids . '">
          <input type="hidden" value="' . $time_pay . '" id="fast_order_unique_key">
          <input type="hidden" value="' . $ids . '" id="pay_com_ids' . $ids . '">
          <input type="hidden" value="' . $url . '" id="fast_admin_text' . $ids . '">
      </div>
    ';
		}
		return $template;
	}
}

//onclick="penalty.fastOrderPayBox('.$ids.');"

$complaints = new complaints;
