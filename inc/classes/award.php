<?php
class award {
  public function award_list() {
   global $db, $user_id, $category, $count_tasks, $convoys_count, $money, $mile;

   $q = $db->query("SELECT `id`, `award_id`, `award_points`, `count`, `title`, `description`, `img`, `section`, `type` FROM `award` WHERE `del` = 0 ORDER BY `id` ASC");

   $i = 1;
   while($d = $db->assoc($q)) {
    $id = $d['id'];
    $award_id = $d['award_id'];
    $award_points = $d['award_points'];
    $count = $d['count'];
    $title = $d['title'];
    $description = $d['description'];
    $img = $d['img'];
    $section = $d['section'];
    $type = $d['type'];
    $check_award_done = check_award_done($award_id, $user_id, $type);
    $tasks_percent = $count_tasks*33/100;
    $ref_num = award::ref_num($user_id);

   if($check_award_done == 1) {
     $img = $img;
   } elseif($award_id == 1) {
     $img = '/images/awards/locked_friend.png';
   } else {
     $img = '/images/awards/locked.png';
   }

     if($ref_num <= $count && $type == 1) {
       $award_percent = round(($ref_num / $count) * 100);
     } elseif($convoys_count <= $count && $type == 2) {
       $award_percent = round(($convoys_count / $count) * 100);
     } elseif($count_tasks <= $count && $type == 3) {
       $award_percent = round(($count_tasks / $count) * 100);
     } elseif($money <= $count && $type == 4) {
       $award_percent = round(($money / $count) * 100);
     } elseif($mile <= $count && $type == 5) {
       $award_percent = round(($mile / $count) * 100);
     } elseif($award_id == 4 && $check_award_done == 0) {
       $award_percent = 0;
     } else {
       $award_percent = 100;
     }

     if($award_percent == 100) {
       $text_title = 'Выполнено.';
     } else {
      $s = '%';
      $text_title = $award_percent.''.$s;
     }

    $template .= '
    <div class="bundle" id="awr'.$id.'">
        <div class="info-points">'.$award_points.' '.declOfNum(abs($award_points), array('балл', 'балла', 'баллов')).'</div>
        <div class="img"><img src="'.$img.'"></div>
        <div class="points">
            <span>'.$title.'</span>
            <i>'.$description.'</i>
        </div>
        <span class="empty">
          <div class="ui_progress_bar_body south">
          <div id="user_rating" class="ui_progress_bar_content">'.$text_title.'</div>
          <div id="user_rating_w" class="ui_progress_bar_tail" style="width: '.$award_percent.'%;"></div>
          </div>
        </span>
    </div>
    ';
    $i++;
   }

   return $template;
  }
 public function ref_num($user_id = null) {
   global $db;

   $q = $db->query("SELECT `uid` FROM `users` WHERE `ref_uid` = '$user_id' AND `uban_type` = '0'");
   $n = $db->num($q);

   return $n;
 }
 public function count_award() {
   global $db;

   $q = $db->query("SELECT `id` FROM `award`");
   $n = $db->num($q);

   return $n;
 }
}

$award = new award;
?>
