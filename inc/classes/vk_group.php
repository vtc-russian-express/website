<?php
class vk_group {
    public function capi($code = null) {
        global $token_complaints;
        return $api_url = 'https://api.vk.com/method/execute?code='.urlencode($code).'&access_token='.$token_complaints;
    }

	public function vk_group_get() {
        global $db, $dbName, $time, $group_id;

        $banned_text = 'Необходимо подать заявку в нашу закрытую группу вконтакте vk.com/club117117618 если вы это уже сделали то обратитесь к администратору.';

        $select_vk_id = $db->query("SELECT `uvk_id` FROM `users`");
        $select_vk_id_array = array();
        while($select_vk_id_d = $db->assoc($select_vk_id)) {
            $select_vk_id_array[] = $select_vk_id_d['uvk_id'];
        }

        $calls = array();
        $users_result = array();
        for($i = 0; $i < 16; $i++) {
            $calls[] = 'API.groups.getMembers({"group_id":'.$group_id.', "count": 1000, "offset": '.($i * 1000).'})';
        }
        $code = "return [" . @implode(',', $calls) . "];";
        $api = json_decode(@vk::_post(vk_group::capi($code)));

        if($api->execute_errors || $api->error) {
            $vk_error = 1;
        } else {
            foreach($api->response as $response_key => $response_value) {
                $users = $api->response[$response_key]->users;
                foreach($users as $key => $value) {
                    $users_result[] = $value;
                }
            }
         }

        $user_vk_group = array_diff($select_vk_id_array, $users_result);
        $user_vk_group_count = count($user_vk_group);

        if($vk_error) {
            $json = array('error_text' => 'Ошибка соединения с сервером ВКонтакте. Попробуйте позже. <br /> Причиной этого может быть недоступность ссылки или потеря соединения.');
        } else {
            // Блокируем сотрудников
            $user_group_banned = "UPDATE  `$dbName`.`users` SET `uban_type` =  '1', `uban_text` = '$banned_text'  WHERE  `users`.`uvk_id` IN(".@implode(',', $user_vk_group).");";
            // получаем список uid'ов по их vk_id
            $user_vk_group_uid = $db->query("SELECT `uid` FROM `users` WHERE `uvk_id` IN(".@implode(',', $user_vk_group).")");
            $user_vk_group_array = array();
            while($user_vk_group_d = $db->fetch($user_vk_group_uid)) {
                $user_vk_group_array[] = $user_vk_group_d['uid'];
            }

            // цикл для таблиц logs_blocked
            for($i = 0; $i < $user_vk_group_count; $i++) {
                $for_uid = $user_vk_group_array[$i];
                $user_vk_group_add .= "(NULL ,  '1',  '$for_uid',  '1',  '$banned_text',  '$time',  'Google Chrome',  '192.168.0.1'),";
            }

            if($db->query("INSERT INTO `$dbName`.`logs_blocked` (`id` , `uid` , `to` , `type` , `text` , `time` , `browser` , `ip`) VALUES ".preg_replace('/,$/', '', $user_vk_group_add)."") && $db->query($user_group_banned)) {
                $json = array('success' => 1, 'users' => $user_vk_group_count);
            } else {
                $json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
            }

        }

        return json_encode($json);
	}
}

$vk_group = new vk_group;