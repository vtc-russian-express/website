<?php

class balance {

    public function send() {

        global $db, $dbName, $user_logged, $user_id, $ugroup, $time, $browser, $ip_address;

        if ($ugroup != 4) {
            return json_encode([ 'access' => 'denied' ]);
        }

        $user_ids = $_POST[ 'usersids' ];
        $count    = $_POST[ 'count' ];

        $q = $db->query("SELECT * FROM users WHERE uid IN(" . $user_ids . ");");
        $d = $q->fetch_all();

        $newid = [];
        foreach($d as $user){
            $newid[] = $user[0];
        }

        $user_points_plus = "UPDATE  `$dbName`.`users` SET  `upoints` =  upoints + '$count', `archive` = archive + 1  WHERE  `users`.`uid` IN(" . @implode(',', $newid) . ");";
        $users_edit_time  = "UPDATE  `$dbName`.`users` SET  `convoys_time` =  '$time' WHERE  `users`.`uid` IN(" . @implode(',', $newid) . ");";

        if (!$user_logged) {
            $error = [ 'error_msg' => 'Ошибка доступа.' ];
        } elseif ($count < 1) {
            $error = [ 'error_msg' => 'Необходимо ввести количество баллов.' ];
        } else {
            if ($count > 0) {

                $query = "INSERT INTO `$dbName`.`archive` (`id`, `from`, `to`, `section`, `url`, `tid`, `time`, `type`, `upoints`, `browser`, `ip`, `ucategory`, `module`, `urgent_id`, `points_plus`) VALUES ";

                foreach ($newid as $i => $value) {
                    $query .= "(NULL, '$user_id', '{$value}',  '1',  '0',  '0',  '$time',  '2',  '$count',  '$browser', '$ip_address', '0', '1', '0', '0')";
                    if ($i + 1 < count($newid)) {
                        $query .= ", ";
                    }
                }

                 $db->query($query);
                 $db->query($user_points_plus);
                 $db->query($users_edit_time);

                $mysqlError = @mysql_error();

                if($mysqlError){
                    $error = [ 'error_msg' => 'Ошибка: ' . $query . ' ' ];
                } else {
                    $error = [ 'response' => 1, 'count' => $count];
                }

            } else {
                $error = [ 'error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.' ];
            }
        }

        return json_encode($error);

    }

    public function users_list() {

        global $db, $noavatar;
        {

            $q = $db->query("SELECT * FROM users WHERE `udel` = '0' AND `uban_type` = '0' LIMIT 1000");

            while ($d = $db->assoc($q)) {
                $uid          = $d[ 'uid' ];
                $users_avatar = $d[ 'uavatar' ] ? $d[ 'uavatar' ] : $noavatar;
                $uvkid        = $d[ 'uvk_id' ];
                $uname        = $d[ 'uname' ];
                $ulast_name   = $d[ 'ulast_name' ];
                $ulast_time   = $d[ 'ulast_time' ];
                $ugender      = $d[ 'ugender' ];
                $users_nik    = $d[ 'nik' ];
                $upoints      = $d[ 'upoints' ];
                $balance_uids = $d[ 'uid' ];

                $template .= '
        <div class="wrap-users">
            <div class="inner">
                <div class="image"><img src="' . $users_avatar . '"></div>
                <div class="title"><a href="/id' . $uid . '" onclick="nav.go(this); return false">' . $uname . ' ' . $ulast_name . ' (' . $users_nik . ')</a> за все время заработал <b>' . $upoints . ' ' . declOfNum($upoints, [ 'балл', 'балла', 'баллов' ]) . '</b></div>
                <div class="checkbox_admin">
                    <input type="checkbox" name="users_ids[]" value="' . $balance_uids . '" />
                </div>
            </div>
        </div>
        ';
            }
        }

        return $template;
    }
}

$admin_balance = new balance;
