<?php
class exchange
{
	public function voutes()
	{
		global $db, $redis, $logs, $dbName, $exchange_one_voute, $user_logged, $user_id, $users_nik, $upoints, $uvk_id, $ugroup;

		$count = $_POST['count'];
		$count_nik = $_POST['count_nik'];

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif (!$users_nik) {
			$error = array(
				'error_msg' => 'Необходимо  указать <b><a style="color: #bd0000;" href="/settings" onclick="nav.go(this); return false;">Никнейм</a></b> в настройках.'
			);
		} elseif (!$count) {
			$error = array('error_msg' => 'Пожауйста укажите новый Никнейм.');
		} elseif ($ugroup == 6) {
			$error = array('error_msg' => 'У вас отпуск, вы не можете изменить никнейм.');
		} elseif (!$uvk_id) {
			$error = array(
				'error_msg' => 'Необходимо <b><a style="color: #bd0000;" href="javascript://" onclick="users._add_vk();">привязать страницу</a></b> ВКонтакте.'
			);
		} else {
			if ($db->query(
				"INSERT INTO `$dbName`.`exchange_requests` (`id`, `user_id`, `vk_id`, `nik`, `count`, `points`, `time`, `status`, `no_active`) VALUES (NULL, '$user_id', '$uvk_id', '$count_nik', '$count', '$points', '" . time() . "', '1', '0');"
			)) {
				$db->query("UPDATE `$dbName`.`users` SET `nik_activated` = `nik_activated` + '1' WHERE  `users`.`uid` = '$user_id';");

				$counter = $db->query("SELECT COUNT(*) as `count` FROM `exchange_requests` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_exchange_counter', $counter);

				$logs->exchange_request($user_id, $points);

				$error = array('response' => 1, 'count' => $count, 'points' => $points);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		}

		return json_encode($error);
	}

	public function user_requests()
	{
		global $db, $user_id;

		$q = $db->query("SELECT `count`, `points`, `nik`, `status` FROM `exchange_requests` WHERE `user_id` = '$user_id' ORDER BY `id` DESC");

		while ($d = $db->assoc($q)) {
			$count = $d['count'];
			$points = $d['points'];
			$count_nik = $d['nik'];
			$status = $d['status'];

			switch ($status) {
				case 1:
					$status_title = 'Рассматривается..';
					$status_class = 'wait';

					break;

				case 2:
					$status_title = 'Одобрено';
					$status_class = 'ok';

					break;

				case 3:
					$status_title = 'Отклонено';
					$status_class = 'error';

					break;
			}

			$template .= '
      <div class="exchange_history">
          <div class="exchange_history_left">
              Смена никнейма с <b>' . $count_nik . '</b> на <b>' . $count . '</b>
          </div>
          <div class="exchange_history_right exchange_history_status_' . $status_class . '">
              ' . $status_title . '
          </div>
      </div>
   ';
		}

		return $template;
	}

	public function admin_requests()
	{
		global $db;

		$user_ids = array();
		$requests = array();

		$users = array();

		$q = $db->query(
			"SELECT `id`, `user_id`, `vk_id`, `nik`, `count`, `points`, `time`, `status` FROM `exchange_requests` WHERE `no_active` = '0' ORDER BY `id` DESC"
		);

		while ($d = $db->assoc($q)) {
			$user_ids[] = $d['user_id'];

			$requests[] = array(
				'id' => $d['id'], 'user_id' => $d['user_id'], 'vk_id' => $d['vk_id'], 'nik' => $d['nik'], 'count' => $d['count'], 'points' => $d['points'], 'time' => $d['time'], 'status' => $d['status']
			);
		}

		$users_get = $db->query(
			"SELECT `uid`, `uvk_id`, `uname`, `ulast_name`, `uavatar` FROM `users` WHERE `uid` IN(" . implode(',', $user_ids) . ")"
		);

		while ($users_data = $db->assoc($users_get)) {
			$user_data_user_id = $users_data['uid'];
			$user_data_vk_id = $users_data['uvk_id'];
			$user_data_first_name = $users_data['uname'];
			$user_data_last_name = $users_data['ulast_name'];
			$user_data_avatar = $users_data['uavatar'];

			$users[$user_data_user_id] = array(
				'user_id' => $user_data_user_id, 'vk_id' => $user_data_vk_id, 'first_name' => $user_data_first_name, 'last_name' => $user_data_last_name,
				'avatar' => $user_data_avatar
			);
		}

		for ($i = 0; $i < count($requests); $i++) {
			$request_id = $requests[$i]['id'];
			$request_user_id = $requests[$i]['user_id'];
			$request_vk_id = $requests[$i]['vk_id'];
			$request_user_nik = $requests[$i]['nik'];
			$request_count = $requests[$i]['count'];
			$request_points = $requests[$i]['points'];
			$request_time = $requests[$i]['time'];

			$request_vk_id = $users[$request_user_id]['vk_id'];
			$request_first_name = $users[$request_user_id]['first_name'];
			$request_first_last_name = $users[$request_user_id]['last_name'];

			$template .= '
      <div id="admin_exchange_requests' . $request_id . '" class="admin_exchange_requests' . (($i % 2) ? ' admin_exchange_requests_active' : '') . '">
          <div class="admin_exchange_requests_left">
              <a href="/admin/modules/users/?search=http://vtc-express.ru/id' . $request_user_id . '" target="_blank"><b>' . (no_name($request_first_name . ' ' . $request_first_last_name)) . '</b></a> запросил сменить <b>' . $request_user_nik . '</b> на <b>' . $request_count . '</b></a>
              <div class="admin_exchange_requests_date">' . new_time($request_time) . '</div>
          </div>
          <div class="admin_exchange_requests_right">
              <div onclick="exchange.set_status(' . $request_id . ', 2)" id="admin_exchange_requests_button_ok' . $request_id . '" class="blue_button_wrap small_blue_button admin_exchange_requests_button_ok">
                  <div class="blue_button">Одобрить</div>
              </div>
              <div onclick="exchange.set_status(' . $request_id . ', 3)" id="admin_exchange_requests_button_error' . $request_id . '" class="blue_button_wrap small_blue_button admin_exchange_requests_button_error">
                  <div class="blue_button">Отклонить</div>
              </div>
          </div>
      </div>
   ';
		}

		return (($template) ? $template : '<div id="tasks_none"><div id="text_no">Не найдено ни одной заявки.</div></div>');
	}

	public function admin_set_status()
	{
		global $db, $redis, $user_id, $logs, $dbName, $user_logged, $ugroup;

		$id = (int)$_GET['id'];
		$status = (int)$_GET['status'];

		$q = $db->query(
			"SELECT `id`, `user_id`, `count`, `points`, `status` FROM `exchange_requests` WHERE `id` = '$id' AND `no_active` = '0'"
		);
		$d = $db->assoc($q);

		$user_id2 = $d['user_id'];
		$points = $d['points'];
		$dcount = $d['count'];

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($ugroup != 4) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($d['id']) {
			if ($db->query(
				"UPDATE `$dbName`.`exchange_requests` SET `status` = '$status', `no_active` = '1' WHERE `exchange_requests`.`id` = '$id';"
			)) {
				$counter = $db->query("SELECT COUNT(*) as `count` FROM `exchange_requests` WHERE `no_active` = '0'");
				$counter = $db->assoc($counter);

				$counter = $counter['count'];

				$redis->set('admin_exchange_counter', $counter);

				if ($status == 2) {
					$db->query("UPDATE `$dbName`.`users` SET `nik` = '$dcount' WHERE `users`.`uid` = '$user_id2'");
					$db->query("UPDATE `$dbName`.`users` SET `nik_activated` = `nik_activated` - '1' WHERE  `users`.`uid` = '$user_id2';");
					$logs->exchange_request($user_id, $points, $user_id2, $dcount);
				} elseif ($status == 3) {
					$db->query("UPDATE `$dbName`.`users` SET `upoints` = `upoints` + '$points' WHERE  `users`.`uid` = '$user_id2';");
					$db->query("UPDATE `$dbName`.`users` SET `nik_activated` = `nik_activated` - '1' WHERE  `users`.`uid` = '$user_id2';");
					$logs->exchange_request_error($user_id, $points, $user_id2, $dcount);
				}

				$error = array('response' => 1);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		} else {
			$error = array('error_msg' => 'Ошибка доступа.');
		}

		return json_encode($error);
	}
}

$exchange = new exchange;
