<?php
class chat
{
	public function bb_codes($message = null)
	{
		$message = preg_replace('/\[b\](.*?)\[\/b\]/is', '<b>$1</b>', $message);
		$message = preg_replace('/\[red\](.*?)\[\/red\]/is', '<b style="color:#a7020a;">$1</b>', $message);
		$message = preg_replace('/\[gold\](.*?)\[\/gold\]/is', '<b style="color:#daa520;">$1</b>', $message);
		$message = preg_replace('/\[url=(.*?)\](.*?)\[\/url\]/s', '<a href="$1" target="_blank">$2</a>', $message);

		return $message;
	}

	public function smilies()
	{
		return array(
			':smile1:' => '/images/emoji/D83DDE0A.png',
			':smile2:' => '/images/emoji/D83DDE0B.png',
			':smile3:' => '/images/emoji/D83DDE09.png',
			':smile4:' => '/images/emoji/D83DDE06.png',
			':smile5:' => '/images/emoji/D83DDE04.png',
			':smile6:' => '/images/emoji/D83DDE0C.png',
			':smile7:' => '/images/emoji/D83DDE0D.png',
			':smile8:' => '/images/emoji/D83DDE0F.png',
			':smile9:' => '/images/emoji/D83DDE11.png',
			':smile10:' => '/images/emoji/D83DDE0E.png',
			':smile11:' => '/images/emoji/D83DDE22.png',
			':smile12:' => '/images/emoji/D83DDE2D.png',
			':smile13:' => '/images/emoji/D83DDE25.png',
			':smile14:' => '/images/emoji/D83DDE14.png',
			':smile15:' => '/images/emoji/D83DDE1E.png',
			':smile16:' => '/images/emoji/D83DDC4D.png',
			':smile17:' => '/images/emoji/D83DDC4E.png',
			':smile18:' => '/images/emoji/D83DDC4F.png',
			':smile19:' => '/images/emoji/D83DDC4B.png',
			':smile20:' => '/images/emoji/D83DDC4C.png'

		);
	}

	public function smilies_replace($message = null)
	{
		$smilies = $this->smilies();

		foreach ($smilies as $key => $value) {
			$message = preg_replace('/' . $key . '/is', '<img class="smile" src="' . $value . '">', $message);
		}

		return $message;
	}

	public function messages($ajax = null)
	{
		global $db, $redis, $dbName, $noavatar, $user_id, $user_hash, $ugroup;

		$user_ids = array();
		$messages = array();

		$users = array();

  // удаленные сообщения
		$del_messages = array();

		$del_messages_get = $db->query("SELECT `id` FROM `chat_messages` WHERE `del` = '1' ORDER BY `id` DESC LIMIT 100");

		while ($del_messages_data = $db->assoc($del_messages_get)) {
			$del_messages[] = $del_messages_data['id'];
		}

		if ($ajax) {
			$last_msg_id = (int)$_GET['last_msg_id'];

			$last_msg_id_from_redis = $redis->get('chat_last_msg_id');

			if ($last_msg_id == $last_msg_id_from_redis) {
				echo json_encode(array('del_messages' => implode(',', $del_messages)));

				exit;
			}
		}

		if ($ajax && $last_msg_id == $last_msg_id_from_redis) {
			$q = $db->query("SELECT `id`, `user_id`, `message`, `time` FROM `chat_messages` WHERE `del` = '0' ORDER BY `id` DESC LIMIT 50");
		} else {
			$q = $db->query(
				"SELECT `id`, `user_id`, `message`, `time` FROM `chat_messages` WHERE `id` > '$last_msg_id' AND `del` = '0' ORDER BY `id` DESC LIMIT 50"
			);
		}

		while ($d = $db->assoc($q)) {
			$user_ids[] = $d['user_id'];

			$messages[] = array('id' => $d['id'], 'user_id' => $d['user_id'], 'message' => $d['message'], 'time' => $d['time']);
		}

		$messages = array_reverse($messages);

		$users_get = $db->query(
			"SELECT `uid`, `uvk_id`, `uname`, `ulast_name`, `uavatar`, `chat_ban` FROM `users` WHERE `uid` IN(" . implode(',', $user_ids) . ")"
		);

		while ($users_data = $db->assoc($users_get)) {
			$user_data_user_id = $users_data['uid'];
			$user_data_vk_id = $users_data['uvk_id'];
			$user_data_first_name = $users_data['uname'];
			$user_data_last_name = $users_data['ulast_name'];
			$user_data_avatar = $users_data['uavatar'];
			$user_data_chat_ban = $users_data['chat_ban'];

			$users[$user_data_user_id] = array(
				'user_id' => $user_data_user_id, 'vk_id' => $user_data_vk_id, 'first_name' => $user_data_first_name, 'last_name' => $user_data_last_name,
				'avatar' => $user_data_avatar, 'chat_ban' => $user_data_chat_ban
			);
		}

		for ($i = 0; $i < count($messages); $i++) {
			$msg_id = $messages[$i]['id'];
			$msg_user_id = $messages[$i]['user_id'];
			$msg_message = $messages[$i]['message'];
			$msg_time = $messages[$i]['time'];

			$msg_vk_id = $users[$msg_user_id]['vk_id'];
			$msg_first_name = $users[$msg_user_id]['first_name'];
			$msg_first_last_name = $users[$msg_user_id]['last_name'];
			$msg_avatar = $users[$msg_user_id]['avatar'];
			$user_chat_ban = $users[$msg_user_id]['chat_ban'];

			$template .= '
         <div id="chat_msg' . $msg_id . '" class="chat_msg">
          <div class="chat_msg_avatar">
           <a' . (($msg_vk_id) ? ' href="http://vk.com/id' . $msg_vk_id . '" target="_blank"' : '') . '>
            <img src="' . (($msg_avatar) ? $msg_avatar : $noavatar) . '">
           </a>
          </div>
          <div class="chat_msg_info">
           <div class="chat_msg_info_full_name"><a' . (($msg_vk_id) ? ' href="http://vk.com/id' . $msg_vk_id . '" target="_blank"' : '') . '>' . fxss(no_name($msg_first_name . ' ' . $msg_first_last_name)) . '</a></div>
           <div class="chat_msg_info_text">
            ' . $this->smilies_replace($this->bb_codes(nl2br(fxss($msg_message)))) . '
           </div>
           <div class="chat_msg_info_bar">
            <span class="chat_msg_info_bar_time">
             ' . new_time($msg_time) . '
            </span>
            ' . (($user_id == $msg_user_id) ? '' : '<span class="r">•</span> <a href="javascript://" onclick="chat.msg_answer(' . $msg_id . ')">Ответить</a>') . '
            ' . (($user_id == $msg_user_id || $ugroup == 4) ? '<span class="r">•</span> <a href="javascript://" onclick="chat.msg_del(' . $msg_id . ', \'' . $user_hash . '\')">Удалить</a>' : '') . '
           </div>
          </div>
         </div>
   ';
		}

		if ($ajax) {
			return json_encode(
				array('messages' => $template, 'del_messages' => implode(',', $del_messages), 'last_msg_id' => $last_msg_id_from_redis)
			);
		} else {
			return $template;
		}
	}

	public function send()
	{
		global $db, $redis, $dbName, $noavatar, $user_logged, $user_id, $user_hash, $user_uhash, $uvk_id, $ufirst_name, $ulast_name, $uavatar, $chat_ban;

		$message = trim($_POST['message']);
		$hash = $_POST['hash'];

		$word_not_allowed = array(
			'пидр',
			'лох',
			'пидар',
			'пидарас',
			'сука',
			'хуй',
			'хуйня',
			'пизда',
			'пиздец',
			'уебан',
			'ебать',
			'гандон',
			'бля',
			'блять',
			'сучка',
			'хуйло',
			'долбоёб',
			'дуроёб',
			'ебанатик',
			'ебанутый',
			'ебаришка',
			'ебливая', // темпераментная, гулящая, сексапильная, сексуально озабоченная.
			'ебукентий', // то же,что ебанатик, иронично-снисходительное обращение.
			'ёбарь', // (пренебр.)мужчина, сексуальный партнер.
			'ёбнутый', // то же, что ебанатик.
			'заёб', // ...
			'злоебучая', // очень темпераментная, вредная.
			'коноёбиться', // мудохаться, возиться.
			'мозгоёб',  // назойливый, болтун, придира.
			'мудоёб', // дурак, зануда.
			'разъебай', // растяпа, неумеха, мудак, дурак, ничтожество.
			'худоёбина', // худое существо.
			'взъёбка', // нагоняй, выговор.
			'доебаться', // пристать, придраться, дотошно выяснять.
			'ебаторий', // занудство, возня.
			'ебаться', // заниматься чем-то занудным, надоевшим.
			'ёбля', // утомляющее, надоевшее занятие.
			'ёбанный', // надоевший.
			'заебать', // утомить, замучить, ударить.
			'заёбанный', // уставший.
			'заебаться', // устать.
			'настоебал', // надоесть.
			'отъебись',  // оставить в покое, отстать.
			'приебался', // пристать, придраться.
			'уебать' // ударить, уговорить.
		);

		$word_not_allowed_flag = 0;

		for ($i = 0; $i < count($word_not_allowed); $i++) {
			if (preg_match('/' . $word_not_allowed[$i] . '/', mb_strtolower($message, 'UTF-8'))) {
				$word_not_allowed_flag = 1;
			}
		}

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($hash != $user_uhash) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($redis->get('chat_msg_flood' . $user_id . '' . date('i')) >= 5) {
			$error = array('error_msg' => 'Слишком много сообщений. Попробуйте позже.');
		} elseif ($word_not_allowed_flag == 1) {
			$error = array('error_msg' => 'Мат запрещён.');
		} elseif ($chat_ban == 1) {
			$error = array('error_msg' => 'Вы были заблокированы в чате, обратитесь в поддержку.');
		} elseif (!mb_strlen($message, 'UTF-8')) {
			$error = array('error_msg' => 'Необходимо ввести сообщение.');
		} else {
			if ($db->query(
				"INSERT INTO `$dbName`.`chat_messages` (`id`, `user_id`, `message`, `time`, `del`) VALUES (NULL, '$user_id', '" . $db->escape($message) . "', '" . time() . "', '0');"
			)) {
				$msg_id = $db->insert_id();

				$redis->set('chat_last_msg_id', $msg_id);

    // антифлуд
				$redis->set('chat_msg_flood' . $user_id . '' . date('i'), $redis->get('chat_msg_flood' . $user_id . '' . date('i')) + 1);
				$redis->expire('chat_msg_flood' . $user_id . '' . date('i'), 900);

				$error = array(
					'response' => 1, 'msg_id' => $msg_id, 'vk_id' => $uvk_id, 'first_name' => fxss($ufirst_name), 'last_name' => fxss($ulast_name),
					'fullname' => fxss(no_name($ufirst_name . ' ' . $ulast_name)), 'avatar' => (($uavatar) ? $uavatar : $noavatar), 'message' => $this->smilies_replace($this->bb_codes(nl2br(fxss($message)))),
					'time' => new_time(time()), 'my_hash' => $user_hash
				);
			} else {
				$error = array('error_msg' => 'Ошибка соединения с сервером. Попробуйте позже.');
			}
		}

		return json_encode($error);
	}

	public function del()
	{
		global $db, $dbName, $user_logged, $user_id, $user_uhash, $ugroup;

		$id = (int)$_GET['id'];
		$hash = $_GET['hash'];

		$q = $db->query("SELECT `user_id` FROM `chat_messages` WHERE `id` = '$id'");
		$d = $db->assoc($q);

		if (!$user_logged) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($hash != $user_uhash) {
			$error = array('error_msg' => 'Ошибка доступа.');
		} elseif ($d['user_id'] == $user_id || $ugroup == 4) {
			if ($db->query("UPDATE `$dbName`.`chat_messages` SET `del` =  '1' WHERE  `chat_messages`.`id` = '$id';")) {
				$error = array('response' => 1);
			}
		} else {
			$error = array('error_msg' => 'Ошибка доступа.');
		}

		return json_encode($error);
	}
	public function uban()
	{
		global $db, $dbName, $user_id, $udel, $session, $logs;

		$time = time();
		$uid = (int)abs($_GET['uid']);

		if ($udel) {
			$type = 'return';
			$query = "UPDATE `$dbName`.`users` SET  `chat_ban` =  '0' WHERE  `users`.`uid` = '$uid' LIMIT 1 ;";
		} else {
			$type = 'del';
			$query = "UPDATE `$dbName`.`users` SET  `chat_ban` =  '1' WHERE  `users`.`uid` = '$uid' LIMIT 1 ;";
		}

		if ($session->get('usession') != $ssid) {
			$json = array(
				'error_text' => 'Истек период сессии. Обновите страницу или попробуйте позже.'
			);
		} elseif ($db->query($query)) {
			if ($udel) {
				$logs->return_account($user_id, $user_id);
			} else {
				$logs->delete_account($user_id, $user_id);
			}
			$json = array('success' => 1, 'type' => $type);
		} else {
			$json = array('error_text' => 'Ошибка соединения с базой данных. Попробуйте позже.');
		}
		return jdecoder(json_encode($json));
	}
}

$chat = new chat;
